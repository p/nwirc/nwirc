/*
 * Copyright 2003 - 2005 Nils R. Weller
 */
#include "term_read.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "termcap.h"
#include "keydefs.h"


static struct termcap_entry	*tc;

static struct termios	oldt;
static struct termios	newt;

static void turnon_buff(void);
static void
turnoff_buff(void) {
	static int first_run	= 1;
	if (first_run) {
		atexit(turnon_buff);
		first_run = 0;
	}
	if (tcgetattr(STDIN_FILENO, &oldt) == -1) {
		perror("tcgetattr");
		exit(EXIT_FAILURE);
	}
	newt = oldt;
	newt.c_lflag &= ~(ICANON | ECHO);
	newt.c_cc[VMIN] = 1;
	newt.c_cc[VTIME] = 0;
	if (tcsetattr(STDIN_FILENO, TCSANOW, &newt) == -1) {
		perror("tcsetattr");
		exit(EXIT_FAILURE);
	}
}

static void
turnon_buff(void) {
	(void) tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
}

static int
getchar_unbuffered(void) {
	int				ch;

#if 0
	/*
	 * This is not needed in nwirc, because the terminal input driver
	 * routine sets the terminal into raw mode already
	 */
	turnoff_buff();
#endif
	ch = getchar();
	if (ch == EOF) {
		if (ferror(stdin)) {
			perror("getchar");
#if 0
			turnon_buff();
#endif
			exit(EXIT_FAILURE);
		}
	}
#if 0
	turnon_buff();
#endif
	return ch;
}

static int
compare(const char *dest, const char *src, size_t *srclen, int *substit) {
	const char *const	origsrc = src;

	/* No substitution as of yet */
	*substit = 0;

	for (;;) {
		while (*dest == *src) {
			if (*dest == 0) {
				/* They are even */
				return 0;
			}
			++dest;
			++src;
		}
		if (*dest == 91 && *src == 79) {
			/* 
			 * XXX This is a kludge to support terminal sequences for
			 * arrow keys which arrived at the form ``27 91 XX'' for me,
			 * but are specified by ``27 79 XX'' termcap entries.
			 * p.s. termcap sucks
			 */
			*substit = 1;
			++src; ++dest;
		} else {
			break;
		}
	}
	while (*src) {
		++src;
	}
	*srclen = src - origsrc;
	return 1;
}


struct termcap_entry *
search_entry(const char *name, int *tcur) {
	size_t	namelen = strlen(name);
	size_t	len;
	int		i;
	int		index;
	char	*p;
	struct termcap_entry	*ret = NULL;

	index = term_esc_len[*tcur];
	
	if (index == -1) {
		fprintf(stderr, "Termmap overflow 1!\n");
#if 0
		exit(EXIT_FAILURE);
#endif
		return NULL;
	}

	if ((p = term_esc[index][0].data) != NULL
				&& strlen(p) != namelen) {
		if (strlen(p) > namelen) {
			/*
			 * This means the database only contains entries longer than
			 * the current ``snapshot'' of the escape sequence
			 */
			return NULL;
		}
		++(*tcur);
		index = term_esc_len[*tcur];
		if (index == -1 || term_esc[index] == NULL) {
#if 0
			fprintf(stderr, "Termmap overflow 2!\n");
			printf("looking for ``%s [%d]''\n", name, *name);
			exit(EXIT_FAILURE);
#endif
			return NULL;
		}
	}

	for (i = index; term_esc[i] != NULL; ++i) {
		int	substit; 
		if (compare(name, term_esc[i][0].data, &len, &substit) == 0) {
			if (substit == 0) {
				/* 79 for 91 substitution kludge not performed */
				if (term_esc[i]->value != 0) {
					return term_esc[i];
				} else {
					continue;
				}
			} else {
				ret = term_esc[i];
			}
		} else {
			if (len > namelen) {
				/* current ``name'' is likely incomplete */
				return ret;
			}
		}
	}
	return ret;
}

static int
read_esc_seq(void) {
	char					buf[128];
	size_t					i;
	int						flags;
	int						ch;
	int						tcur = 0;
	struct termcap_entry	*tce;


	if ((flags = fcntl(STDIN_FILENO, F_GETFL)) == -1) {
		perror("fcntl");
		exit(EXIT_FAILURE);
	}

	if (fcntl(STDIN_FILENO, F_SETFL, flags | O_NONBLOCK) == -1) {
		perror("fcntl");
		exit(EXIT_FAILURE);
	}

	buf[0] = 033;
	for (i = 1; (ch = getchar()) != EOF; ++i) {
		if (i >= sizeof buf) {
			fprintf(stderr, "Terminal buffer overflow avoided\n");
			(void) fcntl(STDIN_FILENO, F_SETFL, flags);
			return EOF;
		}
		buf[i] = ch;
		buf[i + 1] = 0;
		if ((tce = search_entry(buf, &tcur)) != NULL) {
			/*printf("key is %s!\n", tce->name);*/
			(void) fcntl(STDIN_FILENO, F_SETFL, flags);
			/*return 'y';*/
			return tce->value;
		}
	}

	(void) fcntl(STDIN_FILENO, F_SETFL, flags);
	if (i == 1) {
		/* Plain ESC! */
		return KEY_ESCAPE;
	} else {
		buf[i] = 0;
#if 0
			fprintf(stderr, "Incomplete or unknown terminal sequence -"
							" %s\n", buf);
#endif
		return EOF;
	}
	/* NOTREACHED */
	return EOF;
}

int
term_read(void) {
	int			ch;
	static int	init;

	if (init == 0) {
		/* First run */
		if ((tc = get_termcap_entry()) != NULL) {
			puts("Using termcap");
		} else {
			tc = get_termcap_from_curses();
		}
		populate_term_esc();
		init = 1;
	}

	ch = getchar_unbuffered();
	if (ch == EOF) {
		/* stdin closed */
		puts("eof!");
		return -1;
	} else if (ch == 033) {
		/* Escape sequence */
		if ((ch = read_esc_seq()) == -1) {
			return 0;
		} else {
			return ch;
		}
	} else {
		/* Ordinary character */
		return ch;
	}
}


