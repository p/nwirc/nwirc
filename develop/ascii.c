/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "ascii.h"
#include <stdio.h>
#include <string.h>
#include "commands.h"
#include "color.h"
#include "defs.h"
#include "lock.h"
#include "nwirc_libc.h"

static struct {
	char *face[10],
	     *name;

} faces[] = {
	{
		{" _?_?_?_",
		 "(  o o  )",
		 "|   |   |  Huh...",
		 "( _____ )",
		NULL
		},
		"huh"
	},
	{
		{" _______",
		 "( o\\ /o )",
		 "|   |   |  Mad...",
		 "(_-----_)",
		NULL
		},
		"mad"
	},
	{
		{" _______",
		 "( O   O )",
		 "|   |   |  Surprised...",
		 "(   O   )",
		NULL
		},
     "surprised"
	},
	{
		{" _______",
		 "( o   o )",
		 "|   |   |  Apathetic...",
		 "( ----- )",
		NULL
		},
		"apathetic"
	},
	{
		{" _______",
		 "( @   @ )",
		 "|   |   |  *DISGUSTED*",
		 "( /---\\ )",
		 NULL
		 },
		 "disgusted"
	},
	{
		{" _______",
		 "( o   - )",
		 "|   |   |  *WINK*",
		 "( \\___/ )",
		 NULL
		 },
		 "wink"
	},
	{
		{" LIVE FREE OR DIE",
		 "( o   o )",
		 "|   |   |  *UNIX(R) HEAD*",
		 "( \\___/ )",
		 NULL
		 },
		 "unix"
	},
	{
		{0}, NULL
	}
};

void
sendface(char *other) {
	struct server *s;
	int            i;
	char          *p;
	p = strchr(other, ' ');
	if (p == NULL) {
		printcol("Usage: /face user <name>\n");
		return;
	}

	lock_server(sbuf);
	s = sbuf;
	*p = 0, ++p;
	for (i = 0; faces[i].name != NULL; ++i) {
   		if (n_strcasecmp(faces[i].name, p) == 0) {
			int num = i;
			for (i = 0; faces[num].face[i] != NULL; ++i) {
				privmsg_send(s, other, faces[num].face[i], 0);
			}
			break;
		}
	}
	unlock_server(sbuf);
	return;
}

