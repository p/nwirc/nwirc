/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "netinit.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <errno.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include "color.h"
#include "parse.h"
#include "commands.h"
#include "defs.h"
#include "dcc.h"
#include "fserv.h"
#include "logging.h"
#include "sendf.h"
#include "filter.h"
#include "nwirc_libc.h"
#include "mode.h"
#include "lock.h"
#include "module.h"
#include "confcol.h"


struct sockaddr_in dcc_addr;
int                size,
                   dcc_waiting = 0;
char               filename[1024];


/*
 * Sends message pointed to by ``message'' to user or channel named by
 * ``other'' - on server pointed to by ``s''. ``display'' specifies
 * whether the the message is displayed on the screen (display != QUIET)
 * or not (display == QUIET). Must be called with server locked
 */
int
privmsg_send(struct server *s, char *other, char *message, int display)
{
	char timebuf[16];

	maketimestamp(timebuf);
	if (filtering == 1) { 
		(void) checkfilter(NULL, NULL, message, OUT);
	}
	sendf(s->sock, SEND_MSG, ":%s PRIVMSG %s :%s\r\n", 
	       s->nickname, other, message);
	if (display != QUIET) {
		if (*other == '#') {
			printcol("\r%^%s %^%s: %^<%^%s%^> %^%s\n",
						colors.time->value, timebuf, 
						colors.channel->value, other,
						/*WHITE*/colors.msg_text->value,
						colors.msg_sender_me->value, s->nickname, 
						/*WHITE*/colors.msg_text->value,
						colors.msg_text->value, message); 
		} else {
			printcol("\r%^%s%^ privmsg( -> %s ): %^%s\n",
						colors.time->value, timebuf,
						colors.privmsg_sender_me->value, other,
						colors.privmsg_text->value, message);
		}
		write_log(s, other, "<%s> %s", s->nickname, message);
	}
	return 0;
}

/*
 * Send /me message ``message'' to user or channel designated by
 * ``other''
 */
int
me_send(char *other, char *message) {
	char	timebuf[16];
	char	*nickname;

	lock_server(sbuf);
	nickname = sbuf->nickname;
	sendf(sbuf->sock, SEND_ME, ":%s PRIVMSG %s :\01ACTION %s\01\r\n",
	       nickname, other, message);
	unlock_server(sbuf);
	maketimestamp(timebuf);
	if (*other == '#') {
		printcol("%^%s %^%s: %^* %^%s %^%s\n",
				colors.time->value, timebuf,
				colors.channel->value, other,
				/*WHITE*/colors.msg_text->value,
				colors.msg_sender_me->value, sbuf->nickname, 
				colors.msg_text->value, message); 
		write_log(sbuf, other, " * %s %s", nickname, message);
	} else {
		printcol("%^%s %^privmsg( -> * %s ): %^%s\n", 
				colors.time->value, timebuf,
				colors.privmsg_sender_me->value, other,
				colors.privmsg_text->value, message);
	}
	return 0;
}
  
/*
 * Send CTCP request pointed to by ``message'' to channel or user
 * ``other''
 */
int
ctcp_send(char *other, char *message) {
	printcol("%^Requesting CTCP ``%s'' from user %s\n",
		colors.ctcp->value, message, other);

	lock_server(sbuf);
	sendf(sbuf->sock, SEND_CTCP, ":%s PRIVMSG %s :\01%s\01\r\n",
			sbuf->nickname, other, message);
	unlock_server(sbuf);
	return 0;
}


int
quit_send(char *command) {
	char	*temp;
	char	buf[1024];
	int		len;
	int		logging;

	puts("\nQuitting");
	lock_server(sbuf);
	logging = sbuf->logging;
	if (logging == 1) {
		cleanupfiles(s_active_serv);
	}
	strcpy(buf, "QUIT :");
	temp = strchr(command, ' ');
	len = 6;
	if (temp) {
		++temp;
		n_snprintf(buf + len, 1024 - len, "%s\r\n", temp);
	} else {
		n_snprintf(buf + len, 1024 - len, "%s\r\n", 
					sbuf->quitmsg);
	}
	sendf(sbuf->sock, SEND_QUIT, "%s", buf);
	unlock_server(sbuf);
	return 1;
}   


int
part_send(char *command) {
	char  *temp,
	       buf[1024];
	int    i,
			logging,
	       len;
	time_t tim;

	tim = time(NULL);
	temp = strchr(command, ' ');
	memset(buf, 0, 1024);
	if (temp) {
		*temp = 0;
		n_snprintf(buf, 1024, "PART %s :", command);
		len = strlen(buf);
		n_snprintf((char *)(buf + len), 1024 - len, "%s\r\n", temp + 1); 
	} else {
		n_snprintf(buf, 1024, "PART %s\r\n", command);
	}
	printcol("%^Parting channel %s\n", RED, command);

	lock_server(sbuf);
	i = getchanindex(sbuf, command);
	logging = sbuf->logging;
	if (logging == 1) {
		if (i != -1) {
			if (temp)
				*temp = 0;
			write_log(sbuf, command, "\nSession in %s closed at %s\n", command,
				asctime(localtime(&tim)));
			close_channel_log(&sbuf->channels[i]);
		}
		if (temp)
			*temp = ' ';
	}
	clearusers(sbuf, command);
	if (i != -1) {
		free(sbuf->channels[i].name);
		sbuf->channels[i].name = NULL;
		rbuf->active_chan = -1;
	}
	sendf(sbuf->sock, SEND_PART, "%s", buf);
	unlock_server(sbuf);
	return 0;
}


int
join_send(char *command) {
	lock_server(sbuf);
	sendf(sbuf->sock, SEND_JOIN, "%s\r\n", &command[1]);
	unlock_server(sbuf);
	return 0;
}


int 
nick_send(char *command) {
	lock_server(sbuf);
	sendf(sbuf->sock, SEND_NICK, "NICK %s\r\n", &command[6]);
	unlock_server(sbuf);
	return 0;
}

int 
topic_send(char *topic) {
	char *p;
	p = strchr(topic, ' ');
	if (p) {
		*p = 0, ++p;
		lock_server(sbuf);
		sendf(sbuf->sock, SEND_TOPIC, "TOPIC %s :%s\r\n", topic, p);
		unlock_server(sbuf);
	}
	return 0;
}

int 
mode_send(char *receiver, char *mode) {
	lock_server(sbuf);
	sendf(sbuf->sock, SEND_MODE, "MODE %s %s\r\n", receiver, mode);
	unlock_server(sbuf);
	return 0;
}

int 
kick_send(char *ch, char *receiver) {
	lock_server(sbuf);
	sendf(sbuf->sock, SEND_KICK, "KICK %s %s\r\n", ch, receiver);
	unlock_server(sbuf);
	return 0;
}

static void *
do_sys_send(void *c) {
	char			buf[2048];
	char			*p;
	char			*command = c;
	FILE			*fd;
	int				i;
	struct server	*s = sbuf;

#ifdef pthread_detach /* IRIX does not have this */
	pthread_detach(pthread_self());
#endif

	p = strchr(command, ' ');
	if (p) {
		*p++ = 0;
		if ((fd = popen(p, "r")) == NULL) {
			perror("popen");
			free(command);
			return NULL;
		}
		i = 1;

		while (fgets(buf, 2048, fd)) {
			p = strchr(buf, '\n');
			if (p) *p = 0;
			if ((i % 5) == 0) {
				usleep(1000000);
			}
			lock_server(s);
			privmsg_send(s, command, buf, 0);
			unlock_server(s);
			++i;
		}
		(void) pclose(fd);
	} else {
		printcol("%^Usage: /system #channel/user <command>\n", RED);
	}
	free(command);
	return NULL;
}

int
sys_send(char *command) {
	int			rc;
	pthread_t	pt;
	char		*cmd = n_xstrdup(command);
 
#ifdef __osf__
	/* 
	 * For some obscure reason, the fork() performed by do_sys_send()
	 * will hang on Tru64 UNIX 5.1B, and possibly other versions, if
	 * we run the function as a new thread
	 */
	do_sys_send(cmd);
#else
	rc = pthread_create(&pt, NULL, do_sys_send, cmd);
	if (rc != 0) {
		printcol("pthread_create: %s\n", strerror(errno));
		free(cmd);
	}
#endif
	return 0;
}

    
int
ip_send(char *chan) {
	char buf[128]
#ifndef NO6
		, ntopbuf[INET6_ADDRSTRLEN]
#endif
	;

	lock_server(sbuf);
	if (sbuf->protocol == 4) {
		n_snprintf(buf, 128, "'s IP: %s", inet_ntoa(local.sin_addr));
	} else {
#ifndef NO6
		n_snprintf(buf, 128, "'s IP: %s", 
			inet_ntop(AF_INET6, (char *)&local6.sin6_addr.s6_addr, ntopbuf,
			sizeof(ntopbuf)));
#endif
	}
	unlock_server(sbuf);
	me_send(chan, buf);
	return 0;
} 

int
notice_send(char *user, char *message) {
	char	*p = strchr(user, ' ');

	if (p) *p = 0;
	lock_server(sbuf);
	sendf(sbuf->sock, SEND_NOTICE, "NOTICE %s :%s\r\n",
			user, message);
	unlock_server(sbuf);
	return 0;
}
  

int 
exit_client(void) {
	int i;

	for (i = 0; i < client.allservers; ++i) {
		lock_server(&servers[i]);
		if (servers[i].sock > -1) {
			if (servers[i].logging == 1) 
				cleanupfiles(i);
			sendf(servers[i].sock, SEND_QUIT, "QUIT\r\n");
			close(servers[i].sock);
			servers[i].sock = -1;
		}
		unlock_server(&servers[i]);
	}
	return 1;
}

static void *
do_deluxe(void *c) {
	int		i;
	int		n; 
	char	chan[256];
	char	message[1024];
	char	buf[512];
	char	*temp;
	char	*command = c;

#ifdef pthread_detach /* IRIX does not have this */
	pthread_detach(pthread_self());
#endif
	temp = strchr(command, ' ');
	if (temp == NULL) {
		/* print usage */
		free(command);
		return NULL;
	}
	n = (int)(temp - command);
	if (n >= 1024) {
		printcol("%^\rChannel name too long >:(\n", RED);
		free(command);
		return NULL;
	}
	strncpy(chan, command, n);
	chan[n] = 0;
	command = temp + 1;
	temp = strchr(command, ' ');
	if (temp == NULL) {
		/* print usage */
		free(command);
		return NULL;
	}
	n = (int)(temp - command); 
	if (n >= 512) { 
		printcol("%^\r``Number'' too long >:(\n", RED);
		free(command);
		return NULL;
	}
	strncpy(buf, command, n);
	buf[n] = 0;
	n = strtol(buf, NULL, 10);
	command = temp + 1;
	n_snprintf(message, 1024, "%s %s\r\n", chan, command);
	n_snprintf(buf, 512, "/join %s", chan);

	for (i = 0; i < n; i++) {
		privmsg_send(sbuf, chan, command, 0);
		usleep(1000000);
		part_send(message);
		usleep(1000000);
		join_send(buf);
		usleep(1000000);
	} 
	free(command);
	return NULL;
}
  

int 
deluxe(char *command) {
	int			rc;
	pthread_t	pt;
	char		*cmd = n_xstrdup(command);
  
	rc = pthread_create(&pt, NULL, do_deluxe, cmd);
	if (rc != 0) {
		printcol("pthread_create: %s\n", strerror(rc));
		free(cmd);
	}
	return 0;
}


static void 
ctcp_recv(char *receiver, char *other, char *request) {
	int  dcc_info[2],
		 autodcc,
	     i;
	char buf[1024],
	     buf2[1024],
	     buf3[1024],
	     timebuf[16];

	maketimestamp(timebuf);

	if (n_strcasecmp(request, "\01PING\01") == 0) {
		printcol("%^%s %^%s received CTCP PING request from %s\n",
				colors.time->value, timebuf,
				colors.ctcp->value, receiver, other);
		lock_server(rbuf);
		n_snprintf(buf, 1024,
				":%s NOTICE %s :\01PING \01\r\n",
				rbuf->nickname, other);
		buf[1023] = 0;
		send(rbuf->sock, buf, strlen(buf), 0);
		unlock_server(rbuf);
	} else if (n_strcasecmp(request, "\01VERSION\01") == 0) {
		printcol("%^%s %^%s received CTCP VERSION request from %s\n", 
				colors.time->value, timebuf,
				colors.ctcp->value, receiver, other);
		lock_server(rbuf);
		n_snprintf(buf, 1024, 
				":%s NOTICE %s :\01VERSION %s (%s) @ %s\01\r\n", 
				rbuf->nickname, other, CLI_VER, rbuf->nickname, 
				client.operating_system);
		buf[1023] = 0;
		send(rbuf->sock, buf, strlen(buf), 0);
		unlock_server(rbuf);
	} else if (n_strncasecmp(request, "\01TIME\01", 6) == 0) {
		printcol("%^%s %^%s received CTCP TIME request from %s\n", 
				colors.time->value, timebuf,
				colors.ctcp->value, receiver, other);
		lock_server(rbuf);
		n_snprintf(buf, 1024, 
				":%s NOTICE %s :\01TIME Too late for you, %s ;D\01\r\n",
				rbuf->nickname, other, other);
		buf[1023] = 0;
		send(rbuf->sock, buf, strlen(buf), 0);
		unlock_server(rbuf);
	} else if (n_strncasecmp(request, "\01DCC SEND ", 10) == 0) { 
		if (sscanf(request + 10, "%s %s %d %d\01", 
			filename, buf3, &dcc_info[0], &dcc_info[1]) == 4) {
			dcc_addr.sin_addr.s_addr = inet_addr(buf3);
			dcc_addr.sin_family = AF_INET;
			dcc_addr.sin_port = htons( dcc_info[0] );
			printf("DCC SEND request from %s (%s): %s (%d %s)\n",
					other, inet_ntoa(dcc_addr.sin_addr), filename,
					dcc_info[1] > 999 ?
					dcc_info[1] / 1024 : dcc_info[1], 
					dcc_info[1] > 999 ? "KBytes" : "Bytes");
			fflush(stdout);
			if (strchr(filename, '/') != NULL ||
				strchr(filename, '\\') != NULL) {
				printcol( 
"The name contains a directory path. I refuse to accept this transfer, for\n"
"the sake of security.\n" 
						); 
				return;
			}
			printcol("\n");
			size = dcc_info[1];
			++dcc_waiting;
			lock_server(rbuf);
			autodcc = rbuf->autodcc;
			unlock_server(rbuf);
			if (autodcc == 1)
				dcc_get();
		} else {
			printcol("Malformed CTCP request\n");
		}
	} else {
		printcol("%^%s %^%s received unknown CTCP from user %s: %s\n", 
				colors.time->value, timebuf,
				colors.ctcp->value, receiver, other, request);
		memset(buf2, 0, 1024), memset(buf, 0, 1024);
		lock_server(rbuf);
		for (i = 1; 
			request[i] != 0x1 && request[i] != 0; 
			buf[i-1]=request[i], i++);
		n_snprintf(buf2, 1024, ":%s NOTICE %s :\01%s U r y 2 lame, m8.\01\r\n",
              rbuf->nickname, other, buf);
		buf2[1023] = 0;
		printcol("%^Sending: %s\n", YELLOW, buf2);
		send(rbuf->sock, buf2, strlen(buf2), 0);
		unlock_server(rbuf);
	}
} 


void
mode_recv(char *sender, char *channel, char *mode) {
	char timebuf[16];
	maketimestamp(timebuf);
	printcol("%^%s %s:%^ %s sets mode: %s\n",
			colors.time->value, timebuf, channel, /*YELLOW*/
			colors.ctcp->value, sender, mode); 
	write_log(rbuf, channel, "%s sets mode: %s", sender, mode);
	/*
	 * Channel and user mode changes need different treatment, because the user
	 * changes are not kept in the channel user list, and currently not even
	 * implemented yet!
	 */
	if (*channel == '#') {
		parse_mode(channel, mode);
	}
}  

void
kick_recv(char *sender, char *channel, char *receiver, char *message) {
	int    i;
	char   chanbuf[128],
	       timebuf[16],
	      *p;
	time_t tim = time(NULL);

	maketimestamp(timebuf);
	lock_server(rbuf);
	i = getchanindex(rbuf, channel);
	unlock_server(rbuf);
	if ((p = strchr(receiver, ' ')) != NULL) *p = 0;
	if (*message == ':') ++message;
	if (n_strcasecmp(receiver, rbuf->nickname) != 0) {
		printcol("%^%s %^%s: %^%s has been kicked by %s (%s)\n",
				colors.time->value, timebuf,
				colors.channel->value, channel,
				YELLOW, receiver, sender, message);
		write_log(rbuf, channel, "%s has been kicked by %s (%s)", 
			receiver, sender, message);
		remuser(receiver, channel, 0, NULL);
	} else {
		printcol("%^You have been kicked by %s (%s)\n",
				YELLOW, sender, message);
		write_log(rbuf, channel, "You have been kicked by %s (%s)", sender, message);
		write_log(rbuf, channel, "Session in %s closed at %s\n", channel,
			asctime(localtime(&tim)));
		if (i != -1) {
			if (i == rbuf->active_chan) {
				rbuf->active_chan = -1;
			}
			clearusers(rbuf, channel);
			free(rbuf->channels[i].name);
			rbuf->channels[i].name = NULL;

			lock_server(sbuf);
			if (sbuf->logging != 0) {
				close_channel_log(&rbuf->channels[i]);
			}
			unlock_server(sbuf);
		}
		printcol("%^Attempting to rejoin...\n", YELLOW);
		n_snprintf(chanbuf, 128, "/join %s", channel);
		join_send(chanbuf);
	} 
}

void
ping_recv(char *sender) { /* XXX (use sendf()) */
	char sendbuf[128];
	n_snprintf(sendbuf, 128, "PONG %s\r\n", sender); 
	send(rbuf->sock, sendbuf, strlen(sendbuf), 0);
} 

void
topic_recv(char *sender, char *channel, char *topic) {
	char timebuf[16];
	maketimestamp(timebuf);
	if (*topic == ':')
		++topic;
	printcol("%^%s %s: %^%s changes topic to: %s%^\n",
			colors.time->value, timebuf, channel,
			/*YELLOW*/colors.ctcp->value, sender, topic, 
			colors.msg_text->value);
	write_log(rbuf, channel, "%s changes topic to: %s", sender, topic);
}
 

void
privmsg_recv(char *address, char *sender, char *receiver, char *message)
{
	char *temp,
	      timebuf[16];
	maketimestamp(timebuf);
	if (*message == ':') ++message;
	if (filtering == 1) {
		if (checkfilter(address, sender, message, IN) == 1) /* discard */
			return;
	}
	if (n_strcasecmp(receiver, rbuf->nickname) == 0) { /*private 
														message for you*/
		if (*message == 0x1) {
			if (n_strncasecmp(message, "\01ACTION ", 8) == 0) {
				temp = strchr(&message[1], 0x1);
				if (temp)
					*temp = 0;
				printcol("%^%s %^privmsg * %s %^%s\n",
						colors.time->value, timebuf,
						colors.privmsg_sender_other->value, sender,
						colors.privmsg_text->value, &message[8]); 
				write_log(rbuf, sender, " * %s %s", sender, &message[8]);
			} else {
				ctcp_recv(receiver, sender, message);
			}
		} else if (n_strncasecmp(message, ".fs ", 4) == 0) {
			fserv(address, sender, &message[4]);
		} else if (n_strcasecmp(message, "!fortune") == 0) {
			char	buf[128];
			n_snprintf(buf, sizeof(buf) - 1, "%s fortune", sender);
			sys_send(buf);
		} else {
			if (n_strcasecmp(message, "!beep") == 0) {
				printf("\a\a\a");
				fflush(stdout);
			}
			printcol("%^%s %^privmsg( %s ): %^%s\n",
					colors.time->value, timebuf,
					colors.privmsg_sender_other->value, sender,
					colors.privmsg_text->value, message);
			write_log(rbuf, sender, "<%s> %s", sender, message);
		}
	} else { /*channel message*/
		if (n_strncasecmp(message, "\01ACTION ", 8) == 0) {
			temp = strchr(&message[1], 0x1);
			if (temp)
				*temp = 0;
			printcol("%^%s %^%s: %^* %^%s %^%s\n",
					colors.time->value, timebuf,
					colors.channel->value, receiver,
					/*WHITE*/colors.msg_text->value,
					colors.msg_sender_other->value,
					sender, colors.msg_text->value, 
					&message[8]);
			write_log(rbuf, receiver, " * %s %s", sender, &message[8]);
		} else if (*message == 1) {
			ctcp_recv(receiver, sender, message);
		} else if (n_strcasecmp(message, "!fortune") == 0) {
			char	buf[128];
			n_snprintf(buf, sizeof(buf) - 1, "%s fortune", receiver);
			sys_send(buf);
		} else {
			printcol("%^%s %^%s: %^<%^%s%^> %^%s\n",
					colors.time->value, timebuf,
					colors.channel->value, receiver,
					/*WHITE*/colors.msg_text->value,
					colors.msg_sender_other->value, sender,
					/*WHITE*/colors.msg_text->value,
					colors.msg_text->value, message);
			write_log(rbuf, receiver, "<%s> %s", sender, message);
		}
	}
}

void part_recv
(char *name, char *address, char *sender, char *channel, char *message)
{
	char timebuf[16];

	maketimestamp(timebuf);
	if (n_strcasecmp(sender, rbuf->nickname) != 0) {
		if (*message == ':') ++message;
		printcol("%^%s %^%s [%s@%s] has left %s (%s)\n", 
				colors.time->value, timebuf,
				colors.part->value, sender,
				name, address, channel, message);
		write_log(rbuf, channel, "%s [%s@%s] has left (%s)",
			sender, name, address, message);
		remuser(sender, channel, 0, NULL);
	} else {
		strcpy(client.prompt, "nwIRC> ");
		client.focus[0] = 0;
	}
}
 

void
join_recv(char *name, char *address, char *sender, char *channel) {
	time_t          tim;
	struct chan    *temp;
	char            timebuf[16],
	                filebuf[256];
	int             cindex = -1,
	                i;

	if (filtering == 1) {
		if (checkfilter(address, sender, "", IN) == 1) /* discard */
			return;
	}

	if (*channel == ':')  ++channel;
	if (n_strcasecmp(sender, rbuf->nickname) != 0) {
		maketimestamp(timebuf);
		printcol("%^%s %^* %s [%s@%s] has joined %s\n", 
				colors.time->value, timebuf,
				colors.join->value, sender,
				name, address, channel);
		write_log(rbuf, channel, "%s [%s@%s] has joined %s", 
			sender, name, address, channel );
		adduser(sender, channel);
		return;
	}
  
	printcol("%^Now talking on %s\n", RED, channel);
	/*
	 * First attempt to find free ``slot'' in the stuff that's already allocated
	 * which is now unused because the user left the channel
	 */
	for (i = 0; i < rbuf->allchans; ++i) {
		if (rbuf->channels[i].name == NULL) {
			/* cool! it's free */
			cindex  = i;
			current = i;
		}
	}

 
	if (cindex == -1) {
		/*
		 * Got to allocate a new channel structure
		 */     
		current = rbuf->allchans;
		++rbuf->allchans;

		temp = (struct chan *)
				realloc(rbuf->channels, 
				(rbuf->allchans + 1) * sizeof(struct chan));
		if (temp == NULL) {
			printcol("%^Sorry, interface doesn\'t have enough memory to handle"
					" channel %s\n", RED, channel);
			current = -1;
			return;
		}
		rbuf->channels = temp;
		cindex = rbuf->allchans - 1;
	}

 
	rbuf->channels[cindex].name = n_strdup(channel);
	rbuf->channels[cindex].am_operator = 0;
	rbuf->channels[cindex].users = NULL;
	rbuf->channels[cindex].logday = -1;
	if (rbuf->channels[cindex].name == NULL) {
		printcol("%^Sorry, interface doesn\'t have enough memory to handle"
				" channel %s\n", RED, channel);
		return;
	}

	if (rbuf->logging == 1) { 
		if (open_channel_log(rbuf, &rbuf->channels[cindex]) != 0) {
			return;
		}
		tim = time(NULL);
		write_log(rbuf, rbuf->channels[cindex].name, 
			"Session in %s started at %s",
			rbuf->channels[cindex].name, 
			asctime(localtime(&tim)));
	}
}


void quit_recv
(char *name, char *address, char *sender, char *message, char *message2)
{
	char	timebuf[16],
			msgbuf[512];
	int		i;

	if (filtering == 1) {
		if (checkfilter(address, sender, "", IN) == 1) /* discard */
			return;
	}

	maketimestamp(timebuf);
	if (*message == ':') ++message;
	printcol("%^%s %^%s [%s@%s] has quit (%s %s)\n",
			colors.time->value, timebuf,
			colors.quit->value, sender,
			name, address, message, message2);
/*since a QUIT packet doesn't mention channels, we can't either; no logging*/
	for (i = 0; i < rbuf->allchans; i++) {
		if (rbuf->channels[i].name) { 
			n_snprintf(msgbuf, 512, "%s %s", message, message2);
			remuser(sender, rbuf->channels[i].name, 1, msgbuf);
		}
	} 
}

void
notice_recv(char *sender, char *address, char *message) {
	char	timebuf[16],
			*p;
	if (filtering == 1) {
		if (checkfilter(address, sender, message, IN) == 1) /* discard */
			return;
	}
	maketimestamp(timebuf);
	if (*message == ':') ++message;
	if ((p = strchr(message, 0x1)) != NULL) *p = ' ';
	if ((p = strchr(message, 0x1)) != NULL) *p = ' ';
	printcol("%^%s%^ [%s@%s] NOTICE: %^%s\n",
			GREY, timebuf, MAGENTA, sender, address, /*WHITE*/
			colors.msg_text->value, message);
	if (n_strncasecmp( message, "DCC Send ", 9) == 0) {
		message += 9;
		p = strchr(message, ')');
		if (p) {
			*p = 0;
			dcc_addr.sin_addr.s_addr = inet_addr( message );
		}
	} 
}

void
nick_recv(char *name, char *address, char *sender, char *nick) {
/*
 * TODO:
 * log!!!!! write generic ``for each'' function that logs channelwise
 * unspecified events,,, blah
 */
	char timebuf[16];
	maketimestamp(timebuf);
	if (*nick == ':') ++nick;
	if (n_strcasecmp(sender, rbuf->nickname) == 0) {
		free(rbuf->nickname);
		rbuf->nickname = n_strdup(nick);
		printcol("%^%s %^You are now known as %s\n",
			colors.time->value, timebuf,
			colors.nickchange->value, nick);
	} else {
		printcol("%^%s %^%s [%s@%s] is now known as %s\n",
				colors.time->value, timebuf,
				colors.nickchange->value, sender, name, address, nick);
	}
	/*
	 * Save nickchange in channel user table(s)
	 */
	change_userdata(name, nick, 0, NULL);
}  

/*void whois_recv( char *packet )
{
  char buf[128],
       buf
  maketimestamp( timebuf );
}*/
  
