/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "security.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include "commands.h"
#include "logging.h"
#include "defs.h"
#include "nwirc_libc.h"
#include "lock.h"

/* /clear #channel <minutes> */
void
clear_chan(char *data) {
	char             *p;
	int               i,
	                  minutes = 0,
	                  param = 0;
	time_t            tim;
	struct chan_user *ubuf;

	p = strchr(data, ' ');
	if (p != NULL) {
		*p = 0, ++p;
		param = 1;
		minutes = strtol(p, NULL, 10);/*only kick users that joined < mins ago*/
	}

	lock_server(sbuf);
	i = getchanindex(sbuf, data);
	if (i == -1) {
		printf("Sorry, couldn't find channel in ``database''\n");
		unlock_server(sbuf);
		return;
	}

	if (sbuf->channels[i].am_operator == 0) {
		fprintf(stderr, "Sorry, can't do this. You're not operator.\n");
		unlock_server(sbuf);
		return;
	}
	mode_send(data, "+im"); /* invite, moderated */ 
	if (param == 1) { 
		tim = time(NULL);
		for (ubuf = sbuf->channels[i].users; ubuf; ubuf = ubuf->next) {
			if (ubuf->name) {
				if (((float)(tim - ubuf->signon) / 60) <= minutes)
					kick_send(data, ubuf->name);
			}
		}
	} else {
		for (ubuf = sbuf->channels[i].users; ubuf; ubuf = ubuf->next) {
			if (ubuf->name) {
				if (n_strcasecmp(ubuf->name, sbuf->nickname) != 0)
 					kick_send(data, ubuf->name);
			}
		}
	}
	unlock_server(sbuf);
}
      
