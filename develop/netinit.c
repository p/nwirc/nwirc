/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "netinit.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <signal.h>
#include "parse.h"
#include "commands.h"
#include "unbinput.h"
#include "defs.h"
#include "users.h"
#include "filter.h"
#include "sighand.h"
#include "nwirc_libc.h"
#include "color.h"

int            filtering = 0;

struct sockaddr_in local;

#ifndef NO6
struct sockaddr_in6 local6;
#endif


void *
sendt(void *arg) {
	char	buf[2048];
	(void)arg;
	for (;;) {
		n_getstr(buf, 2048, U_ECHO);
		if (parse_send(buf) == 1)
			break;
	}
	printcol("%^", GREY);
	sleep(4);
	exit(0);
	/* NOTREACHED */
	return NULL;
}

pthread_mutex_t	sync_conn_mut;
pthread_cond_t	sync_conn_cond;
int				conn_status;

void *
setup_conn(void *data) {
	char            buf[2048],
					*temp;
	int             i;
	int				rc;
	int				on = 1;
	socklen_t	    len;
	pthread_t		sendthr;
	struct hostent *ho;
	struct sockaddr_in remote;
#ifndef NO6
	struct sockaddr_in6 remote6;
	memset(&remote6, 0, sizeof(remote6));
#endif
	memset(&remote, 0, sizeof(remote));

	if ((rc = pthread_create(&sendthr, NULL, sendt, NULL)) != 0) {
		fprintf(stderr, "pthread_create: %s\n", strerror(rc));
		exit(EXIT_FAILURE);
	}
	data = NULL;
	rc	 = -1;

	for (i = 0; i < client.allservers; ++i) {
		if (servers[i].protocol == 4) {
			/*
			 * Using IPv4
			 */
			if ((remote.sin_addr.s_addr=inet_addr(servers[i].address)) ==
				(in_addr_t)-1) {
				struct in_addr tmp;
				if ((ho = gethostbyname(servers[i].address)) == NULL) {
					herror(servers[i].address);
					servers[i].sock = -1;
					continue;
				}
				tmp = *(struct in_addr *)ho->h_addr_list[0];
				printf("Resolved server address %s\n",
						inet_ntoa(tmp));
				remote.sin_addr = *(struct in_addr *) ho->h_addr_list[0];
			}
			remote.sin_family = AF_INET;
			remote.sin_port = htons(servers[i].port);
			servers[i].sock = socket(AF_INET, SOCK_STREAM, 0);
		} else {
			/*
			 * Using IPv6
			 */
#ifndef NO6
			if (inet_pton(AF_INET6, servers[i].address, &remote6.sin6_addr)
				<= 0) {
				if ((ho = gethostbyname2(servers[i].address, AF_INET6))==NULL) {
					herror(servers[i].address);
					servers[i].sock = -1;
					continue;
				}
				remote6.sin6_addr = *(struct in6_addr *) ho->h_addr_list[0];
			}
			remote6.sin6_family = AF_INET6;
			remote6.sin6_port = htons(6667);
			servers[i].sock = socket(AF_INET6, SOCK_STREAM, 0);
#endif
		}

		if (servers[i].sock < 0) {
			perror(servers[i].address);
			continue;
		}
		(void) setsockopt(servers[i].sock, SOL_SOCKET, SO_KEEPALIVE,
			&on, sizeof on);
		servers[i].active_chan = -1;

		printf("Connecting to %s...\n", servers[i].address);
		printf("Nickname: %s\n", servers[i].nickname);
		printf("Logging %s\n", servers[i].logging == 1 ?
		       "enabled" : "not enabled");
		printf("Using IPv%c\n", servers[i].protocol == 6 ? '6' : '4');
#ifndef NO6
		if ((servers[i].protocol == 6 &&
		     (rc=connect(servers[i].sock, (struct sockaddr *) &remote6,
		     		sizeof(remote6))) == 0) ||
		   	(servers[i].protocol != 6 &&
		     (rc=connect(servers[i].sock, (struct sockaddr *) &remote,
			 sizeof(remote))) == 0))
#else
		if ((rc = connect(servers[i].sock, (struct sockaddr *) &remote,
		    sizeof(remote))) == 0)
#endif
		{
			if (servers[i].protocol != 6) {
				len = sizeof(local);
				if (getsockname(servers[i].sock, (struct sockaddr *) &local,
				    &len) != 0) {
					printcol("getsockname: %s\n", strerror(errno));
				}
				client.myaddr = local.sin_addr.s_addr;
			} else {
#ifndef NO6
				len = sizeof(local6);
				if (getsockname(servers[i].sock, (struct sockaddr *) & local6,
				    &len) != 0) {
					printcol("getsockname: %s\n", strerror(errno));
				}
				client.myaddr6 = local6.sin6_addr.s6_addr;
#endif
			}
		} else {
			fprintf(stderr, "connect: %d (%s) to %s\n", rc, strerror(errno),
					servers[i].address);
			servers[i].sock = -1;
			continue;
		}

		s_active_serv = i;
		r_active_serv = i;

		sbuf = &servers[i];
		sbuf->channels = NULL;
		rbuf = &servers[i];
		rbuf->channels = NULL;

		n_snprintf(buf, 2048, "PASS foobar\r\n");
		send(servers[i].sock, buf, strlen(buf), 0);
		n_snprintf(buf, 2048, ":~%s USER %s tolmoon tolsun :~%s\r\n",
			 servers[i].nickname, servers[i].nickname,
			 servers[i].nickname);
		send(servers[i].sock, buf, strlen(buf), 0);

		pthread_mutex_init(&sync_conn_mut, NULL);
		pthread_cond_init(&sync_conn_cond, NULL);

		/*
		 * This loop sends the nickname with as many characters ``_''
		 * appended as needed until the name is not in use. The receiver
		 * loop sets conn_status to either 0 (which means the name is
		 * good and we are connected) or 1 (which means the name is in
		 * use)
		 */
		pthread_mutex_lock(&sync_conn_mut);
		for (;;) {
			n_snprintf(buf, 2048, "NICK %s\r\n", servers[i].nickname);
			send(servers[i].sock, buf, strlen(buf), 0);
			pthread_cond_wait(&sync_conn_cond, &sync_conn_mut);
			if (conn_status == 0) {
				break;
			}

			/* Name in use - retry with one more _ */
			if ((servers[i].nickname = realloc(servers[i].nickname,
				strlen(servers[i].nickname) + 2)) == NULL) {
				perror("realloc");
				exit(EXIT_FAILURE);
			}
			strcat(servers[i].nickname, "_");
		}
		servers[i].allchans = 0;
		servers[i].channels = n_xmalloc(sizeof *servers[i].channels);

		if (servers[i].invisible) {
			mode_send(servers[i].nickname, "+i");
		}
		if (servers[i].autojoin) {
			for (temp = strchr(servers[i].autojoin, '#'),
			     strncpy(buf, "/join ", 7);
			     temp != NULL;
			     temp = strchr(temp + 1, '#'), strncpy(buf, "/join ", 7)) {
				usleep(5000000);
				if (sscanf(temp, "%2048[^,]", &buf[6]) == 1)
					join_send(buf);
			}
		}
		if (servers[i].using_filter == 1 && filtering == 0) {
			loadfilter("rules.f");
			filtering = 1;
		}
	}

	if (s_active_serv == -1) {
		/*
		 * If s_active_serv's initial value (-1) was never changed,
		 * no server connected successfully
		 */
		exit(1);
	}
	return NULL;
}


int
read_line(int sock, char *buf, size_t size) {
	size_t	i;
	int		rc;
	for (i = 0; i < size - 1; ++i) {
		if ((rc = recv(sock, &buf[i], 1, 0)) != 1) {
			return rc;
		}
		if (buf[i] == '\n') {
			++i;
			buf[i] = 0;
			return i;
		}
	}
	buf[i] = 0;
	return i;
}
			

void *
recvt(void *data) {
	char            buf[8192];
	struct timeval  tdelay;
	fd_set          fds;
	int             rc,
					n,
					i,
					j,
					bigfd;

	data = NULL;
	for (;;) {
		tdelay.tv_sec = 1;
		tdelay.tv_usec = 0;
		FD_ZERO(&fds);
		for (i = 0, bigfd = 0; i < client.allservers; ++i) {
			if (servers[i].sock > -1) {
				FD_SET(servers[i].sock, &fds);
				if (servers[i].sock > bigfd)
					bigfd = servers[i].sock;
			}
		}
		rc = select(bigfd + 1, &fds, 0, 0, &tdelay);
		if (rc == -1) {
			perror("Failure");
			break;
		} else if (rc == 0) {
			/*
			 * No descriptor ready
			 */
			continue;
		}

		for (i = 0; i < client.allservers && rc > 0; ++i, --rc) {
			if (servers[i].sock == -1
				|| !FD_ISSET(servers[i].sock, &fds)) {
				continue;
			}
			memset(buf, 0, 8192);
			n = read_line(servers[i].sock, buf, sizeof(buf));
			r_active_serv = i;
			if (n > 0) {
				if (getstripos != getstri) {
					for (j = 0; j < (getstri-getstripos); ++j) {
						putchar(' ');
					}
				}
				for (j = 0; j < (getstri + strlen(client.prompt)); ++j)
					printf("\b \b");
				fflush(stdout);
				parse_receive(buf);
				putchar('\r');
				printf("%s", client.prompt);	/* +++ */
				if (getstri != 0) {
					for (j = 0; j < getstri; ++j)
						putchar(getstrs[j]);
				}
				if (getstripos != getstri) {
					for (j = getstri - getstripos;
						j > 0;
						--j) {
						putchar('\b');
					}
				}
				fflush(stdout);
			} else {
				if (n == 0) {
					printcol("Shutting down connection with server %d (%s)\n",
					 i, servers[i].address);
					shutdown(servers[i].sock, SHUT_RDWR);
					close(servers[i].sock);
					servers[i].sock = -1;
					break;
				} else {
					printcol("Error server %d (%s): recv: %s. Closing connection\n",
						 i, servers[i].address, strerror(errno));
					close(servers[i].sock);
					servers[i].sock = -1;
				}
				if (client.allservers == 1) {
					/*
					 * There's no reason
					 * to continue
					 * running - We just
					 * lost the last
					 * connection
					 */
					/*
					 * todo: Later check
					 * for running dcc
					 * transfers - We
					 * don't want to
					 * interrupt them
					 */
					exit(1);
				}
			}
		}
	}
	return NULL;
}


static void
nonfatal_handler(int s) {
	(void) s;
	restore_term();
	_exit(0);
}

int
ircconnect(void) {
	struct utsname  me;
	pthread_t		pt;
	pthread_t		pt2;

	if (uname(&me) != -1) {
		strncpy(client.operating_system, me.sysname, 64);
		client.operating_system[63] = 0;
	} else {
		printcol("OS not recognized\n");
		strncpy(client.operating_system, "Unknown OS", 11);
	}

	(void) signal(SIGINT, nonfatal_handler);
	(void) signal(SIGTERM, nonfatal_handler);
	(void) signal(SIGHUP, nonfatal_handler);
	(void) signal(SIGPIPE, SIG_IGN);

	strncpy(client.prompt, "nwIRC> ", 8);
	if (pthread_create(&pt, NULL, setup_conn, NULL) == 0 &&
	    pthread_create(&pt2, NULL, recvt, NULL) == 0) {
		/*
		 * XXX At least Debian Testing has a buggy pthread_exit()
		 * that acts like exit(). Perhaps a ``configure'' test
		 * for a working pthread_exit() would be in order. Thanks
		 * to Andre van Ruth for spotting this
		 */
		/* pthread_exit(NULL); */
		(void) pthread_join(pt, NULL);
		(void) pthread_join(pt2, NULL);
	} else {
		printcol("An error occured: %s\n", strerror(errno));
		return 1;
	}
	return 0;
}

