/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FILTER_H
#define FILTER_H

#define ADDRESS 0
#define CHAR    1
#define WORD    2
#define NICK    3
#define REPLACE 4
#define REMOVE  5
#define DISCARD 6
#define IN      7
#define OUT     8

struct rule {
	int          type,
	             action;
	char        *data,
	            *altern;
	struct rule *next,
	            *prev; 
};
       
   

int loadfilter   ( char *filename                                          );
void unloadfilter( void                                                    );
int checkfilter  ( char *address, char *nick, char *message, int direction ); 


#endif
