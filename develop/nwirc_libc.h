/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef NWIRC_LIBC_H
#define NWIRC_LIBC_H

#ifndef STDDEF_H
#define STDDEF_H
#include <stddef.h>
#endif

extern char *n_optarg;
extern int   n_optind,
             n_opterr,
             n_optopt,
             n_optreset;

void *n_xmalloc	(size_t nbytes);
void *n_xrealloc(void *dest, size_t nbytes);
char *n_xstrdup(const char *txt);
int n_strcasecmp (char *dest, char *src               );
int n_strncasecmp(char *dest, char *src, size_t n     );
char *n_strdup   (char *str                           );
int n_getopt     (int argc, char *argv[], char *optstr);
int n_isblank    (int ch                              );
char *get_term_name(void);

#if !defined(__osf__)
#define n_vsnprintf vsnprintf
#define n_snprintf snprintf
#define HAVE_SNPRINTF
#endif

#endif /* #ifndef NWIRC_LIBC_H */
