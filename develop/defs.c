/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "defs.h"
#include <stdio.h>
#include <pthread.h>

int           s_active_serv = -1,
              r_active_serv = -1;

struct server servers[MAXSERVERS];
struct server *sbuf = NULL;
struct server *rbuf = NULL;
struct client_ client = {
	NULL, NULL, "nwIRC> ", { 0 }, { 0 }, { 0 }, 0, 0, 0, 0, 0, 0, NULL
};

void
init_defs(void) {
	int	i;
	for (i = 0; i < MAXSERVERS; ++i) {
		pthread_mutex_init(&servers[i].mutex, NULL);
		servers[i].nickname		= NULL;
		servers[i].name[0]		= 0;
		servers[i].handle[0]	= 0;
		servers[i].autojoin		= NULL;
		servers[i].quitmsg		= NULL;
		servers[i].address		= NULL;
		servers[i].sock			= -1;
		servers[i].protocol		= 4;
		servers[i].port			= 6667;
		servers[i].logging		= 0;
		servers[i].autodcc		= 0;
		servers[i].dcctimeout	= 0;
		servers[i].invisible	= 1;
		servers[i].serving		= 0;
		servers[i].using_config	= 0;
		servers[i].using_filter = 0;
		servers[i].allchans		= 0;
		servers[i].active_chan	= -1;
		servers[i].channels		= NULL;
	}
}

