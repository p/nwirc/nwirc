/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "color.h"
#include <stdio.h>
#include <stdarg.h>
#include <ctype.h>
#include "defs.h"

const struct color foreground[] = {
	{ "grey", GREY },
	{ "black", BLACK },
	{ "red", RED },
	{ "green", GREEN },
	{ "yellow", YELLOW },
	{ "blue", BLUE },
	{ "magenta", MAGENTA },
	{ "cyan", CYAN },
	{ "white", WHITE },
	{ NULL, 0 }
};

const struct color background[] = {
	{ "black", B_BLACK },
	{ "blue", B_BLUE },
	{ "red", B_RED },
	{ "magenta", B_MAGENTA },
	{ "green", B_GREEN },
	{ "cyan", B_CYAN },
	{ "yellow", B_YELLOW },
	{ "white", B_WHITE },
	{ NULL, 0 }
};


static int 
do_text_to_value(const struct color *table, const char *name) {
	for (; table->name != NULL; ++table) {
		if (strcmp(table->name, name) == 0) {
			return table->value;
		}
	}
	return -1;
}

static char * 
do_value_to_text(const struct color *table, int value) {
	for (; table->name != NULL; ++table) {
		if (table->value == value) {
			return table->name;
		}
	}
	return NULL;
}

int
col_text_to_value(const char *name) {
	return do_text_to_value(foreground, name);
}

int
bgcol_text_to_value(const char *name) {
	return do_text_to_value(background, name);
}

char *
col_value_to_text(int value) {
	return do_value_to_text(foreground, value);
}

char *
bgcol_value_to_text(int value) {
	return do_value_to_text(background, value);
}

/*example for printcol:
printcol( "%^hello %^%s", BLUE, CYAN, "world" );
outputs text ``hello world'', where ``hello'' is blue coloured, ``world'' cyan*/

static int	do_print_prompt = 1;

void
printcol_prompt(int setting) {
	do_print_prompt = setting;
}


static int		saved_foreground = GREY;
static int		saved_background = 0;


void
set_bg_color(int col) {
	printf("\033[1;%d;%dm", saved_foreground, col);
	saved_background = col;
	fflush(stdout);
}

void
printcol(char *fmt, ...) {
	va_list ap;
	int     i;
	char   *s;

	va_start(ap, fmt);
	putchar('\r');
	while (*fmt) {
		switch (*fmt) {
		case '%':
			++fmt;
			switch (*fmt) {
			case '^':
				i = va_arg(ap, int);
				if (saved_background) {
					printf("\033[1;%d;%dm", i, saved_background);
				} else {
					printf("\033[1;%dm", i); /* XXX */
				}
				saved_foreground = i;
				/* printf("\033[1;%d;%dm", i, B_WHITE); */
				break;  
			case 's':
				s = va_arg(ap, char*);
				if (s == NULL) {
					printf("(null)");
					break;
				}
				for (i = 0; s[i]; i++) {
					if ((isprint((unsigned char)s[i]) == 0) &&
						(s[i] == 0x7 || s[i] == 0x8 || s[i] == 0xb || 
						s[i] == 0xc)) {
							printf("\\%x", s[i]);
					} else {
						putchar(s[i]);
					}
				}
				/*printf( "%s", s ); */
				break;
			case 'c':
				i = va_arg(ap, int);
				printf("%c", i);
				break;
			case 'd':
				i = va_arg(ap, int);
				printf("%d", i);
				break;
			default:
				if ((isprint((unsigned char)*fmt) == 0) &&
					(*fmt == 0x7 || *fmt == 0x8 || *fmt == 0xb || *fmt == 0xc))
					printf("\\x%x", *fmt);
				else
					putchar(*fmt);
				break;
			}
			break;
		default:
			putchar(*fmt);
			break;
		}
		++fmt;
	}
	va_end(ap);
  /*if( strchr( fmt, '\n' ) )*/
	if (do_print_prompt) {
		printf("\033[1;%dm", GREY);
		printf("%s", client.prompt);
		fflush(stdout); 
	}
}

