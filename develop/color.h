/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef COLOR_H
#define COLOR_H

/* foreground colors */

#define GREY    0
#define BLACK   30
#define RED     31
#define GREEN   32
#define YELLOW  33
#define BLUE    34
#define MAGENTA 35
#define CYAN    36
#define WHITE   37

/* background colors */

#define B_BLACK   40
#define B_BLUE    44
#define B_RED     41
#define B_MAGENTA 45
#define B_GREEN   42
#define B_CYAN    46
#define B_YELLOW  43
#define B_WHITE   47

struct color {
	char	*name;
	int		value;
}; 

extern const struct color background[];
extern const struct color foreground[];

void	printcol_prompt(int on);
void	set_bg_color(int col);
void	printcol(char *fmt, ...);
int		col_text_to_value(const char *);
char	*col_value_to_text(int);
int		bgcol_text_to_value(const char *);
char	*bgcol_value_to_text(int);

#endif

