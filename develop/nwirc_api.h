/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef NWIRC_API_H
#define NWIRC_API_H

#define API_SEND_MSG    (1)
#define API_SEND_ME     (2)
#define API_SEND_CTCP   (3)
#define API_SEND_QUIT   (4)
#define API_SEND_PART   (5)
#define API_SEND_JOIN   (6)
#define API_SEND_NICK   (7)
#define API_SEND_TOPIC  (8)
#define API_SEND_MODE   (9) 
#define API_SEND_KICK   (10)
#define API_SEND_SYS    (11)
#define API_SEND_IP     (12)
#define API_SEND_NOTICE (13)

struct server;

typedef int (*f_privmsg_send_t)  (struct server *, char *, char *, int);
typedef int (*f_me_send_t)       (char *, char *);
typedef int (*f_ctcp_send_t)     (char *, char *);
typedef int (*f_quit_send_t)     (char *);
typedef int (*f_part_send_t)     (char *);
typedef int (*f_join_send_t)     (char *);
typedef int (*f_nick_send_t)     (char *);
typedef int (*f_topic_send_t)    (char *);
typedef int (*f_mode_send_t)     (char *, char *);
typedef int (*f_kick_send_t)     (char *, char *);
typedef int (*f_sys_send_t)      (char *);
typedef int (*f_ip_send_t)       (char *);
typedef int (*f_notice_send_t)   (char *, char *);

#include "module.h"

extern generic_fptr_t	api_table[];

#endif

