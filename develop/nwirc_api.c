/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "module.h"
#include <stdio.h>
#include "commands.h"

generic_fptr_t	api_table[] = {
	(generic_fptr_t) privmsg_send, 
	(generic_fptr_t) me_send, 
	(generic_fptr_t) ctcp_send, 
	(generic_fptr_t) quit_send, 
	(generic_fptr_t) part_send, 
	(generic_fptr_t) join_send, 
	(generic_fptr_t) nick_send, 
	(generic_fptr_t) topic_send, 
	(generic_fptr_t) mode_send, 
	(generic_fptr_t) kick_send, 
	(generic_fptr_t) sys_send, 
	(generic_fptr_t) ip_send, 
	(generic_fptr_t) notice_send, 
	NULL
};
  
