/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>

#ifdef sun
/* for thr_setconcurrency( ) */
#include <pthread.h>
#endif

#include "defs.h"
#include "netinit.h"
#include "unbinput.h"
#include "creatconf.h"
#include "nwirc_libc.h"
#include "parse.h"
#include "color.h"
#include "confcol.h"


int
create_nwirc_dir(void) {
	struct stat		sbuf;
	char			buf[4096];
	struct passwd	*pw;

	if ((pw = getpwuid(getuid())) == NULL) {
		(void) fprintf(stderr, "Cannot obtain your user identity. Sorry.\n");
		return -1;
	}
	if (strlen(pw->pw_dir) + sizeof "/.nwirc" > sizeof buf) {
		(void) fprintf(stderr,
			"Your home directory name is way too long!\n");
		return -1;
	}
	sprintf(buf, "%s/.nwirc", pw->pw_dir);
	if (stat(buf, &sbuf) == -1) {
		if (errno != ENOENT) {
			perror(buf);
			return -1;
		} else {
			if (mkdir(buf, S_IRWXU) == -1) {
				perror(buf);
				return -1;
			}
		}
	}
	if (chdir(buf) == -1) {
		perror(buf);
		return -1;
	}
	return 0;
}


/*
 * This is a horrible mess and should be rewritten...
 */
int
main(int argc, char *argv[]) {
	char buf[3];
	char buf2[256];
	int  ch;

	if (create_nwirc_dir() == -1) {
		return 1;
	}
#ifdef sun
	/* set thread concurrency level for Solaris */
	thr_setconcurrency(6);
#endif

	init_defs();
	if (argc < 2) { /* no command line options */
		switch (readconfig()) {

		/* if config file available, connect using its data */
		case 0:
			servers[0].using_config = 1; 
			if (colors.background->value != -1) {
				set_bg_color(colors.background->value);
			}
			return ircconnect();

		/* no config file, use some defaults */
		case -1:
		case 1:
			servers[0].logging      = 1;
			servers[0].splitlogs    = 0;
			servers[0].using_config = 0;
			servers[0].serving      = 0;
			servers[0].protocol     = 4;
			servers[0].port         = 6667;
			servers[0].quitmsg      = "leaving";
		}

		/* Prompt user to create new config file */
		printf("Create new config file? [y\\n] ");
		n_getline(buf, 3); 
		switch (toupper((unsigned char)buf[0])) {
		case 'Y': return makeconfig();
		default: ;
		} 
		servers[0].autodcc = 0;

		/*
		 * Prompt user to enter server and nickname (no defaults used 
		 * here)
		 */
		printf("Enter server address: ");
		n_getline(buf2, 256);
		servers[0].address = n_strdup(buf2);
		printf("Enter your nickname: ");
		n_getline(buf2, 256);
		servers[0].nickname = n_strdup(buf2);
	} else {
    
		/* parse command line options */
#ifndef NO6
		while ((ch = n_getopt(argc, argv, "s:u:p:lFfdhn6S")) != -1) {
#else
		while ((ch = n_getopt(argc, argv, "s:u:p:lFfdhnS")) != -1) {
#endif
			switch (ch) {
#ifndef NO6
			case '6': servers[0].protocol = 6;
#endif
			case 'l': servers[0].logging = 1; break;
			case 'f': servers[0].serving = 1; break;
			case 'F': servers[0].using_filter = 1; break;
			case 'd': servers[0].autodcc = 1; break;
			case 'p': servers[0].port = strtol(n_optarg, NULL, 10); break;
			case 'n': client.noterm = 1; break;
			case 's':
				if (n_optarg)
					servers[0].address = n_strdup(n_optarg);
				break;
			case 'S': /* XXX ugly */
				servers[0].splitlogs = 1;
				break;
			case 'u':
				if (n_optarg)
					servers[0].nickname = n_strdup(n_optarg);
				break;
			case 'h':
				fprintf(stderr, "d = autodcc\nf = fserv\nh = help\nl = "
				"logging\np = packetfilter\n");
				/* FALLTHRU */
			default:
				fprintf(stderr, 
				"Usage: ./nwirc [-dfhlp] [-u nickname]  [-s server]\n");
				return 1;
			}
		}
	}

	/* validate if server/nickname was entered & successfully copied */
	if (servers[0].address == NULL || servers[0].nickname == NULL) {
		fprintf(stderr, "Need server/nickname\n");
		return 1;
	}
	client.allservers = 1;
	/* connect (no config file) */
	if (servers[0].protocol != 6) {
		servers[0].protocol = 4;
	}
	servers[0].quitmsg = "leaving";
	return ircconnect();
}

