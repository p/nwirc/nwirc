#include "module.h"
#include <dlfcn.h>
#include <stdio.h>
#include <string.h>
#include "nwirc_libc.h"
#include "sendf.h"

#ifdef NO_MODULE

/* Empty module stubs */
void	load_mod(const char *name) { return; }

#else /* NO_MODULE not defined */

struct module			*all_modules = NULL;
static struct module	*all_mod_tail = NULL;

static void
add_module(struct module *m) {
	m->next = NULL;
	if (all_modules == NULL) {
		all_modules = all_mod_tail = m;
	} else {
		all_mod_tail->next = m;
		all_mod_tail = all_mod_tail->next;
	}
}
		
void
load_mod(const char *name) {
	char			buf[1024];
	void			*handle;
	struct module	*m;

	if (strlen(name) + sizeof "modules/" > sizeof buf) {
		printcol("Error: module name (way) too long\n");
		return;
	}
	sprintf(buf, "modules/%s.mod", name);
	if ((handle = dlopen(buf, RTLD_NOW)) == NULL) {
		printcol("%s: %s\n", buf, dlerror());
		return;
	}
	m = n_xmalloc(sizeof *m);
	m->name = name;
	m->path = n_xstrdup(buf);
	m->handle = handle;
	add_module(m);
	send_register(m);
	printcol("Loaded module %s\n", buf);
}

#endif /* NO_MODULE not defined */

