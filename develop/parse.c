/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "parse.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include "defs.h"
#include "netinit.h"
#include "commands.h"
#include "extra.h"
#include "dcc.h"
#include "creatconf.h"
#include "color.h"
#include "users.h"
#include "security.h"
#include "logging.h"
#include "ascii.h"
#include "nwirc_libc.h"
#include "module.h"
#include "confcol.h"


char	address[128],
		name[128];

void
n_getline(char *buf, int len) {
	char *temp;
	if (fgets(buf, len, stdin)) {
		if ((temp = strchr(buf, '\n')) != NULL)
			*temp = 0;
		else
			while (getchar() != '\n');
	} else
		exit(0);
}


int
scanpacket(char *packet,
	   char *arg1, char *arg2, char *arg3, char *arg4, char *arg5) {
	char	*ptr,
			*ptr2;
	int		i,
			n;

	memset(arg1, 0, 1024);
	memset(arg2, 0, 1024);
	memset(arg3, 0, 1024);
	memset(arg4, 0, 1024);
	memset(arg5, 0, 1024);
	memset(address, 0, 128);
	memset(name, 0, 128);
	if ((ptr = strchr(packet, 0xd)) != NULL)
		*ptr = 0;
	i = strcspn(&packet[1], "! ");
	if (packet[i + 1] == 0) {
		return -1;
	} else if (packet[i + 1] == ' ' && packet[0] != ':') {
		return -1;
	}
	ptr = strchr(&packet[i + 1], '@');
	if (ptr) {
		n = (int)(ptr - &packet[i + 2]);
		if (n >= 128) {
			strncpy(name, &packet[i + 2], 128);
			name[127] = 0;
		} else {
			strncpy(name, &packet[i + 2], n);
			name[n] = 0;
		}
		++ptr;
		ptr2 = strchr(ptr, ' ');
		if (ptr2) {
			n = (int)(ptr2 - ptr);
			if (n >= 128) {
				strncpy(address, ptr, 128);
				address[127] = 0;
			} else {
				strncpy(address, ptr, n);
				address[n] = 0;
			}
		}
	}
	if (i >= 1024) {
		strncpy(arg1, &packet[1], 1024);
		arg1[1023] = 0;
	} else {
		strncpy(arg1, &packet[1], i);	/* sender of packet */
		arg1[i] = 0;
	}
	ptr = strchr(packet, ' ');
	if (ptr == NULL || *++ptr == 0) {
		printf("Couldn't parse packet: %s\n", packet);
		return -1;
	}
	/*
	 * i did sscanf( ptr, "...%1024[^\x0]" .. ) first, but that didn't
	 * work with glibc, as opposed to bsd's libc
	 */

	i = sscanf(ptr, "%1024s %1024s %1024s", arg2, arg3, arg4);
	if (i == 3) {
		if ((ptr2 = strchr(ptr, ' ')) != NULL) {
			if ((ptr2 = strchr(ptr2 + 1, ' ')) != NULL) {
				if ((ptr2 = strchr(ptr2 + 1, ' ')) != NULL) {
					strncpy(arg5, ++ptr2, 1024);
					arg5[1023] = 0;
					++i;
				}
			}
		}
	}

	return i;
}


int
parse_send(char *command) {
	char *p;

	sbuf = &servers[s_active_serv];

	if (command[0] == '/') {
		switch (tolower((unsigned char)command[1])) {
		case 'c':
			if (n_strncasecmp(&command[1], "ctcp ", 5) == 0) {
				if ((p = strchr(&command[6], ' '))) {
					*p = 0, ++p;
					return ctcp_send(&command[6], p);
				}
			} else if (n_strncasecmp(&command[1], "clear ", 6) == 0) {
				clear_chan(&command[7]);
				return 0;
			}
			break;
		case 'd':
			if (n_strcasecmp(&command[1], "dcc get") == 0)
				return dcc_get();
			else if (n_strncasecmp(&command[1], "dcc send ", 9) == 0)
				dcc_send(&command[10]);
			else if (n_strncasecmp(&command[1], "deluxe", 6) == 0)
				deluxe(&command[8]);
			return 0;
			break;
		case 'e':
			if (n_strcasecmp(&command[1], "exit") == 0)
				return exit_client();
			break;
		case 'f':
			if (n_strncasecmp(&command[1], "face ", 5) == 0) {
				sendface(&command[6]);
				return 0;
			} else if (n_strncasecmp(&command[1], "focus ", 6) == 0) {
				int	i;
				if ((i = getchanindex(sbuf, &command[7])) != -1) {
					int	len = strlen(client.prompt);
					if (client.allservers > 1) {
						n_snprintf(client.prompt, 64, "%s:%s> ",
							sbuf->address, sbuf->channels[i].name);
					} else {
						n_snprintf(client.prompt, 64, "%s> ",
							sbuf->channels[i].name);
						strncpy(client.focus, sbuf->channels[i].name,
							64);
						client.focus[63] = 0;
						sbuf->active_chan = i;
						redraw_cmdline(len);
					}
				}
				return 0;
			}
					
			break;
		case 'h':
			if (n_strncasecmp(&command[1], "help", 4) == 0)
				return help(command[5] != 0 ? &command[6] : &command[5]);
			break;
		case 'i':
			if (n_strncasecmp(&command[1], "ip ", 3) == 0)
				return ip_send(&command[4]);
			break;
		case 'j':
			if (n_strncasecmp(&command[1], "join ", 5) == 0)
				return join_send(command);
			break;
		case 'k':
			if (n_strncasecmp(&command[1], "kick ", 5) == 0) {
				if ((p = strchr(&command[1], ' '))) {
					*p = 0, ++p;
					return kick_send(&command[6], p);
				}
			}
			break;
		case 'l':
			if (n_strncasecmp(&command[1], "loadm ", 6) == 0) {
				if ((p = strchr(&command[1], ' '))) {
					*p = 0, ++p;
					load_mod(p);
					return 0;
				}
			}
			break;
		case 'm':
			if (n_strncasecmp(&command[1], "msg ", 4) == 0) {
				if ((p = strchr(&command[5], ' '))) {
					*p = 0, ++p;
					return privmsg_send(sbuf, &command[5], p, 0);
				}
			} else if (n_strncasecmp(&command[1], "mode ", 5) == 0) {
				if ((p = strchr(&command[6], ' '))) {
					*p = 0, ++p;
					return mode_send(&command[6], p);
				}
			} else if (n_strncasecmp(&command[1], "me ", 3) == 0) {
				char	*receiver = NULL;
				char	*message = NULL;
				p = &command[4];
				while (isspace((unsigned char)*p))
					++p;

				if (*p == '#') {
					/* channel specified */
					receiver = p;
					if ((message = strchr(p, ' ')) == NULL)
						return 0;
					*message++ = 0;
				} else if (*p == '/') {
					/* user specified */
					receiver = p + 1;
					if ((message = strchr(p, ' ')) == NULL)
						return 0;
					*message++ = 0;
				} else if (client.focus[0] != 0) {
					/* send to current channel */
					receiver = client.focus;
					message = &command[4];
				} else {
					/* assume this is a user despite lack of / */
					receiver = p;
					if ((message = strchr(p, ' ')) == NULL)
						return 0;
					*message++ = 0;
				}
				return me_send(receiver, message);
			}
			break;
		case 'n':
			if (n_strncasecmp(&command[1], "nick ", 5) == 0)
				return nick_send(command);
			else if (n_strncasecmp(&command[1], "names ", 6) == 0) {
				printusers(&command[7]);
				return 0;
			} else if (n_strncasecmp(&command[1], "notice ", 7) == 0) {
				char *p;
				if ((p = strchr(&command[8], ' ')) != NULL) {
					return notice_send(&command[8], ++p);
				}
			}
			break;
		case 'p':
			if (n_strncasecmp(&command[1], "part ", 5) == 0)
				return part_send(&command[6]);
			break;
		case 'q':
			if (n_strncasecmp(&command[1], "quit", 4) == 0)
				return quit_send(command);
			break;
		case 's':
			if (n_strncasecmp(&command[1], "system ", 7) == 0)
				return sys_send(&command[8]);
			break;
		case 't':
			if (n_strncasecmp(&command[1], "topic ", 6) == 0)
				return topic_send(&command[7]);
			break;
		case 'u':
			if (n_strncasecmp(&command[1], "url ", 4) == 0)
				return urlparse(&command[5]);
			break;
		case 'w':
			if (n_strncasecmp(&command[1], "whois ", 6) == 0) {
				(void) send(sbuf->sock, command+1, strlen(command+1),0);
				(void) send(sbuf->sock, "\r\n", 2, 0);
				return 0;
			}
			break;
		default:
			printcol("Unknown command.\n");
			return 0;
		}
	} else {
		if (client.focus[0] != 0) {
			privmsg_send(sbuf, client.focus, command, 0);
			return 0;
		}
		strncat(command, "\r\n", 3);
		send(sbuf->sock, command, strlen(command), 0);
		return 0;
	}
	printcol("Unknown command.\n");
	return 0;
}



void
parse_receive(char *packet) {
	int		i,
			val;
	char	buf[1024],
			buf2[1024],
			buf3[1024],
			buf4[1024],
			buf5[1024],
			timebuf[16],
			*p;

	rbuf = &servers[r_active_serv];
	p = packet;
	(void)strtok(p, "\r\n");
	i = scanpacket(p, buf, buf2, buf3, buf4, buf5);
	if (i == 4) {
		val = strlen(buf4);
		strncat(buf4, " ", 1);
		strncat(buf4, buf5, 1023 - val);
	} else if (i < 0) {
		if (n_strncasecmp(p, "PING ", 5) == 0) {
			ping_recv(&p[5]);
		} else {
			printcol("%^%s\n", RED, p);
		}
		return;
	}
	val = strtol(buf2, NULL, 10);
	if (val != 0) {
		p = strchr(buf4, ':');
		if (p != NULL) {
			*p = 0, ++p;
			maketimestamp(timebuf);
			printcol("%^%s %^%s: %^%s\n",
				 colors.time->value, timebuf, colors.msg_text->value, buf4, RED, p);
		} else {
			for (i = 0; buf4[i]; putchar(buf4[i]), i++);
			putchar('\n');
		}
		switch (val) {
		case 353:	/* channel names :server 353
				 * <yournick> = #channel :name name */
			initusers(p);
			break;
		case 311:	/* whois :server 313 yournick nick
				 * realname host * :ircname */
			break;
		case 421:	/* unknown command */
			break;
		case 1:
			pthread_mutex_lock(&sync_conn_mut);
			conn_status = 0; /* connected */
			pthread_cond_signal(&sync_conn_cond);
			pthread_mutex_unlock(&sync_conn_mut);
			break;
		case 433:
			pthread_mutex_lock(&sync_conn_mut);
			conn_status = 1; /* nick in use */
			pthread_cond_signal(&sync_conn_cond);
			pthread_mutex_unlock(&sync_conn_mut);
			break;
		}
	} else {
		switch (toupper((unsigned char)buf2[0])) {
		case 'P':
			if (n_strcasecmp(buf2, "PRIVMSG") == 0)
				privmsg_recv(address, buf, buf3, buf4);
			else if (n_strcasecmp(buf2, "PART") == 0)
				part_recv(name, address, buf, buf3, buf4);
			break;
		case 'J':
			if (n_strcasecmp(buf2, "JOIN") == 0)
				join_recv(name, address, buf, buf3);
			break;
		case 'T':
			if (n_strcasecmp(buf2, "TOPIC") == 0)
				topic_recv(buf, buf3, buf4);
			break;
		case 'Q':
			if (n_strcasecmp(buf2, "QUIT") == 0)
				quit_recv(name, address, buf, buf3, buf4);
			break;
		case 'M':
			if (n_strcasecmp(buf2, "MODE") == 0)
				mode_recv(buf, buf3, buf4);
			break;
		case 'K':
			if (n_strcasecmp(buf2, "KICK") == 0)
				kick_recv(buf, buf3, buf4, buf5);
			break;
		case 'N':
			if (n_strcasecmp(buf2, "NOTICE") == 0)
				notice_recv(buf, address, buf4);
			else if (n_strcasecmp(buf2, "NICK") == 0) {
				nick_recv(name, address, buf, buf3);
			}
			break;

		default:
			for (i = 0; p[i]; putchar(p[i]), i++);
			putchar('\n');
		}
	}
}

