/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "filter.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "netinit.h"
#include "defs.h"
#include "nwirc_libc.h"

#ifdef NO_FILTER

/* Empty filter stubs */

int 
addfilter(struct rule **r, char *type, char *data, char *action, char *altern) {
	return 0;
}

int
loadfilter(char *filename) {
	return 0;
}

void
unloadfilter(void) {
	return;
}

int
checkfilter(char *address, char *nick, char *message, int direction) {
	return 0;
}

#else /* NO_FILTER not defined */

static struct rule *rules_in  = NULL,
            *rules_out = NULL;

int 
addfilter(struct rule **r, char *type, char *data, char *action, char *altern)
{
	int i,
	    a = -1,
	    t = -1;
	struct {
		char *ty;
		int   num;
	} test[2][4] = { 
	{
		{ "word", WORD },
		{ "char", CHAR },
		{ "nick", NICK }, 
		{ "address", ADDRESS }
	}, 
	{
		{ "replace", REPLACE },
		{ "remove", REMOVE },
		{ "discard", DISCARD },
		{ NULL, 0 }
	}
	};
	struct rule *prev = NULL;

	for(i = 0; i < 4; i++) {
		if (n_strcasecmp(test[0][i].ty, type) == 0) {
			t = test[0][i].num;
			break;
		}
	}
	for (i = 0; i < 3; i++) {
		if (n_strcasecmp(test[1][i].ty, action) == 0) {
			a = test[1][i].num;
			break;
		} 
	}
	if (t == -1 || a == -1) {
		fprintf(stderr, "Unknown identifier, skipping line\n");
		return 0;
	}

    
	while ((*r) && (*r)->next) {
		prev = (*r);
		r = &(*r)->next;
	}
	if (*r == NULL) {
		*r = malloc(sizeof(struct rule));
	} else {
		(*r)->next = malloc(sizeof(struct rule));
		r = &(*r)->next;
	}
	if (*r == NULL) {
		fprintf(stderr, "Not enough memory to load rules\n");
		return 1;
	}

	(*r)->next = NULL;
	(*r)->type = t;
	(*r)->action = a;
	(*r)->data = n_strdup(data);
	if (a == REPLACE) {
		(*r)->altern = n_strdup(altern);
	}
	(*r)->prev = prev;
	return 0;
}
    
  

int
loadfilter(char *filename) {
	FILE        *fd;
	char         buf[256],
	             direction[8],  /* ``in'', ``out'' */
	             type[8],       /* ``char'', ``word'', ``nick'' */
	             data[116],
	             action[8],     /* ``replace'', ``remove'', ``discard'' */
	             altern[116];
	int          i = 0,
	             rc;

	fd = fopen(filename, "r");
	if (fd == NULL) {
		perror("Couldn\'t load file");
		return 1;
	}
	while (fgets(buf, 256, fd)) {
		strtok(buf, "\n");
		++i;

		if (buf[0] == '#') { /* is comment */
			continue;
		}

		direction[0] = 0,type[0] = 0, data[0] = 0, action[0] = 0, altern[0] = 0;
		rc = sscanf(buf, 
					"%8s %8s %116s %8s %116s", 
					direction, type, data, action, altern);
		if (rc > 3) {
			if (n_strcasecmp(direction, "in:") == 0) {
				if (addfilter(&rules_in, type, data, action, altern) == 1)
					break; 
			} else if (n_strcasecmp(direction, "out:") == 0) {
				if(addfilter(&rules_out, type, data, action, altern) == 1)
					break;
			} else
				fprintf(stderr, "Syntax error in line %d - skipping\n", i);
		} else if (rc > 2) {
			if (n_strcasecmp(type, "nick") == 0) {
				if (n_strcasecmp(direction, "in:") == 0) {
					if (addfilter(&rules_in, type, data, action, altern) == 1)
						break;
				} else if (n_strcasecmp(direction, "out:") == 0) {
					if (addfilter(&rules_out, type, data, action, altern) == 1)
						break;
				}
			} else
				fprintf(stderr, "Syntax error in line %d - skipping\n", i);
		}
	}
	fclose(fd);
	return 0;
}

void
unloadfilter(void) {
	struct rule *rulbuf[2];
	int          i;
  
	rulbuf[0] = rules_in;
	rulbuf[1] = rules_out;
	filtering = 0;
	for (i = 0; i < 2; i++) {
		while (rulbuf[i]) {
			if (rulbuf[i]->data)
				free(rulbuf[i]->data);
			if (rulbuf[i]->altern)
				free(rulbuf[i]->altern);
			if (rulbuf[i]->prev != rulbuf[i] && rulbuf[i]->prev)
				free(rulbuf[i]->prev);
			rulbuf[i] = rulbuf[i]->next;
		}
	}
	rules_in = NULL;
	rules_out = NULL;
}
      


int
checkfilter(char *address, char *nick, char *message, int direction) {
	struct rule *rulbuf;
	char        *t,
	            *t2,
	             msgbuf[2048];
	int          found = 0;

	if (direction == IN)
		rulbuf = rules_in;
	else
		rulbuf = rules_out;
	while (rulbuf) {
		if (rulbuf->type == CHAR) {
			if(rulbuf->data == NULL) {
				rulbuf = rulbuf->next;
				continue;
			}
			t = message;
			if (rulbuf->action == REPLACE) {
				if (rulbuf->altern == NULL) {
					rulbuf = rulbuf->next;
					continue;
				}
				while ((t2 = strchr(t, rulbuf->data[0])) != NULL) {
					*t2 = rulbuf->altern[0];
					t = t2 + 1;
				}
			} else { /* remove! */
				while ((t = strchr(message, rulbuf->data[0])) != NULL) {
					for (t2 = t; *t2; *t2 = t2[1], ++t2)
						;
				}
			}
		} else if (rulbuf->type == WORD) {
			if(rulbuf->data == NULL) {
				rulbuf = rulbuf->next;
 				continue;
			}
			t = message;
			msgbuf[0] = 0;
			if (rulbuf->action == REMOVE) {
				while ((t2 = strstr(t, rulbuf->data)) != NULL) {
					found = 1;
					strncat(msgbuf, t, t2 - t);
					msgbuf[t2 - t] = 0;
 					t = t2 + strlen(rulbuf->data);   
				}
				if (found) {
					strncpy(message, msgbuf, 2047);
					msgbuf[2047] = 0;
				}
			} else if (rulbuf->action == REPLACE) {
				while ((t2 = strstr(t, rulbuf->data)) != NULL) {
 					found = 1;
					strncat(msgbuf, t, t2 - t);
					if ((strlen(msgbuf) + strlen(rulbuf->altern)) > 2047)
						break;
					strncat(msgbuf, rulbuf->altern, strlen(rulbuf->altern));
					t = t2 + strlen(rulbuf->data);
				}
				if (found) {
					strncat(msgbuf, t, 2047 - strlen(msgbuf));
					msgbuf[2047] = 0;
					strncpy(message, msgbuf, 2048);
					message[2047] = 0;
				}
			} else {
				/* discard message - this only works with incoming packets */
				if ((t = strstr(message, rulbuf->data)) != NULL) {
					int	ch = t[strlen(rulbuf->data)];
					if (ch == 0 || ch == ' ' || ch == '\t' || ch == '\n') {
						if (t == message
							|| (t[-1] == ' ' || t[-1] == '\t')) {
							return 1;
						}
					}
				}
			}
		} else if (rulbuf->type == ADDRESS) { 
			if (fnmatch(rulbuf->data, address, 0) == 0)
				return 1; 
		} else { /* nickname */
			if (fnmatch(rulbuf->data, nick, 0) == 0) {
				return 1; 
			}
		}
		rulbuf = rulbuf->next;
	}
	return 0;
}

#endif /* NO_FILTER not defined */

