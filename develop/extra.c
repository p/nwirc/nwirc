/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "extra.h"
#include <stdio.h>
#include <string.h>
#include "defs.h"
#include "color.h"
#include "nwirc_libc.h"

int
urlparse(char *chan) {
	FILE *fd,
	     *fd2;
	char  buf[1024],
	      buf2[1024],
	     *temp;
	int   i;

	if (*chan == 0) {
		printcol("Usage: urlparse <log>\n");
		return 1;
	}
	fd = fopen(chan, "r");
	if (fd == NULL) {
		printcol("No such file/directory\n");
		return 1;
	}
	if (sscanf(chan, "%1019[^.]", buf) == 1) {
		strncat(buf, ".url", 4);
	} else {
		printcol("Illegal log name :>\n");
	}
	fd2 = fopen(buf, "w+");
	if (fd2 == NULL) {
		printcol("Cannot open %s?!\n", buf);
		return 1;
	}
	while (fgets(buf, 1024, fd)) {
		temp = strchr(buf, '\n');
		if (temp)
			*temp = 0;
		temp = buf;
		while ((temp = strstr( temp, "http://")) != NULL) {
			if (sscanf(temp, "%1024s", buf2) == 1) {
  				fprintf(fd2, "%s\n", buf2);
				temp += strlen(buf2);
			} else 
				break;
		}
	}
	fclose(fd);
	fclose(fd2);
	printcol("URLs from log saved to ");
	for (i = 0; putchar(chan[i]) != '.'; i++);
	printcol("url\n"); 
	return 0;
}


int
help(char *p) {
	int i;
	static struct {
		char	*command,
				*parameters,
				*description;
	} commands[] = {
{ "quit", "<msg>", "Disconnect\n" }, 
{ "exit", "", "Disconnect\n" }, 
{ "join", "#channel", "Join channel ``#channel\'\'\n" },
{ "part", "#channel <msg>", "Part channel ``#channel\'\'\n" },
{ "msg", "#channel/nick <text>", "Send <text> to channel/user\n" },
{ "ctcp", "<user> <request>", "Send ctcp request <request> to <user>\n" },
{ "mode", "#channel <mode>", "Set modes on ``#channel\'\'\n" },
{ "nick", "<name>", "Change your nick to <name>\n" },
{ "url", "#channel.log", "Grabs URL\'s from logfile #channel.log, writes "
                               "them to #channel.url\n" },
{ "dcc", "send <user> <file>/get", "Sends file <file> to user <user>/Accepts latest dcc send request\n" },
{ "deluxe", "<chan> <times> <msg>", "Equivalent to <times> /msg #chan <msg>\n"
      "/part #chan <msg>, join #chan" },
{ NULL, NULL, NULL }
  };
	if (*p == 0) {
		printf("=====\n");
		printcol("%^%s - written by Nils Weller (nrw)\n\nCommands:\n", 
				GREEN, CLI_VER ); 
		printcol("%^THIS HELP IS OUTDATED - REFER TO THE README INSTEAD\n",
				RED);
		putchar('\r');
		for (i = 0; commands[i].command; i++) {
			switch (i % 3) {
			case 0: printf("%s", commands[i].command); break;
			case 1: printf("%32s", commands[i].command); break;
			case 2: printf("%32s\n", commands[i].command); break; 
			}
		}
		printcol("\n\nNote: If you don\'t prefix your command with a ``/\'\',"
				" the line will be directly sent to the server\n"); 
		printcol("\nFor specific help, type /help <command>\n");
		printcol("=====\n");
	} else {
		for (i = 0; commands[i].command; i++) {
			if (n_strcasecmp(commands[i].command, p) == 0) {
				printcol("/%s %28s - %s\n", 
						commands[i].command, commands[i].parameters,
						commands[i].description);
				return 0;
			}
		}
		printcol("%s: No such command.\n", p);
	}
	return 0;
}

