/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/*
 * This file contains general purpose string handling functions that are
 * naturally available on most Unix-libraries, but not defined by ANSI.
 */

#include "nwirc_libc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <stddef.h>


void *
n_xmalloc(size_t nbytes) {
	void	*ret;

	if ((ret = malloc(nbytes)) == NULL) {
		perror("malloc");
		exit(EXIT_FAILURE);
	}
	return ret;
}

void *
n_xrealloc(void *dest, size_t nbytes) {
	void	*ret;

	if ((ret = realloc(dest, nbytes)) == NULL) {
		perror("realloc");
		exit(EXIT_FAILURE);
	}
	return ret;
}

char *
n_xstrdup(const char *txt) {
	char	*ret;
	size_t	len = strlen(txt) + 1;
	ret = n_xmalloc(len);
	memcpy(ret, txt, len);
	return ret;
}
int
n_strcasecmp(char *dest, char *src) {
	while (tolower((unsigned char)*dest)
		== tolower((unsigned char)*src)) {
		if (*dest == 0) {
			return 0;
		}
		++dest, ++src;
	}
	return tolower((unsigned char)*dest) - tolower((unsigned char)*src);
}


int
n_strncasecmp(char *dest, char *src, size_t n) {
	do {
		if (tolower((unsigned char)*dest)
			!= tolower((unsigned char)*src)) {
			return tolower((unsigned char)*dest)
				- tolower((unsigned char)*src);
		}
		if (*src == 0) {
			break;
		}
		++dest, ++src;
	} while (--n);
	return 0;
}


char *
n_strdup(char *str) {
	int		len = strlen(str) + 1;
	char	*ret = malloc(len);
	if (ret) {
		memcpy(ret, str, len);
	}
	return ret;
}


char	*n_optarg = NULL;
int		n_optind = 0,
		n_opterr = 1,
		n_optopt = 0,
		n_optreset = 0;



int
n_getopt(int argc, char *argv[], char *optstr) {
	static char *pos = "";
	static int  skipargs = 1;   /* number of arguments to options
                                 * that have been processed and have
                                 * to be skipped */
	char		*p;
	int			i,
				argindex;

	if (n_optreset == 1) {	/* new run, reset all static data to default */
		pos = "";
		skipargs = 1;
		n_optind = 0;
		n_optreset = 0;
	}
	if (*pos == 0 || *++pos == 0) {	/* processing new argument */
		n_optind += skipargs;
		skipargs = 1;
		pos = argv[n_optind];
		if (argv[n_optind] == NULL) {
			return -1;
		}
		if (strcmp(argv[n_optind], "--") == 0) {
			++n_optind;
			pos = "";
			return -1;
		} else if (strcmp(argv[n_optind], "-") == 0 || argv[n_optind][0]!='-') {
			pos = "";
			return -1;
		}
		++pos;
	}
	p = strchr(optstr, *pos);
	if (p == NULL) {	/* unknown option */
		n_optopt = *pos;
		if (n_opterr != 0) {
			fprintf(stderr, "Unknown option -- %c\n", n_optopt);
		}
		return '?';
	}
	if (p[1] != ':') {	/* argument required? */
		return *pos;
	}
	/*
	 * Try and see if argument follows directly after option, as in;
	 * -lpthread -> rc = l, optarg = pthread
	 */
	if (pos[1]) {
		n_optarg = pos + 1;
		n_optopt = *pos;
		pos = "";
		return n_optopt;
	}
	/*
	 * Calculate destination index. Example: -foo bar foo bar ^-- optind
	 * = 0. argindex = 0 + 3 = 3, IF first two options take args
	 */
	argindex = n_optind;
	for (i = (pos - argv[n_optind]); argv[n_optind][i] != '-'; --i) {
		if ((p = strchr(optstr, argv[n_optind][i])) && p[1] == ':') {
			++argindex;
		}
	}
	/*
	 * Validate argument is given
	 */
	if (argindex >= argc || argv[argindex] == NULL) {
		/* it isn't */
		n_optopt = *pos;
		if (n_opterr != 0) {
			fprintf(stderr, "Argument required for option -- %c\n", n_optopt);
		}
		return *optstr == ':' ? ':' : '?';
	}
	n_optarg = argv[argindex];
	++skipargs;
	return *pos;
}

int
n_isblank(int ch) {
	if (ch == ' ' || ch == '\t') {
		return ch;
	} else {
		return 0;
	}
}

#if !defined(HAVE_SNPRINTF) 

int
n_vsnprintf(char *buf, int maxlen, char *format, va_list v) {
	static char	*p = NULL;
	int			rc;
	if (p == NULL) {
		p = malloc(16384);
		if (p == NULL) {
			perror("malloc");
			*buf = 0;
			return 0;
		}
	}
	rc = vsprintf(p, format, v);
	strncpy(buf, p, maxlen);
	buf[maxlen - 1] = 0;
	return rc;
}

int
n_snprintf(char *buf, int maxlen, char *format, ...) {
	int		rc;
	va_list	v;
	va_start(v, format);
	rc = n_vsnprintf(buf, maxlen, format, v);
	va_end(v);
	return rc;
}

#endif

char *
get_term_name(void) {
	char	*p;
	
	if ((p = getenv("TERM")) == NULL) {
		fprintf(stderr, "Error: TERM environment variable not set!\n");
		return NULL;
	}
	return p;
}

