/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef SENDF_H
#define SENDF_H

#define SEND_MSG    (1)
#define SEND_ME     (2)
#define SEND_CTCP   (3)
#define SEND_QUIT   (4)
#define SEND_PART   (5)
#define SEND_JOIN   (6)
#define SEND_NICK   (7)
#define SEND_TOPIC  (8)
#define SEND_MODE   (9) 
#define SEND_KICK   (10)
#define SEND_SYS    (11)
#define SEND_IP     (12)
#define SEND_NOTICE (13)
#define SEND_EXIT   (14)
#define SEND_MAX	SEND_EXIT

struct module;

void sendf(int sock, int type, char *format, ...);
void send_register(struct module *m);

#endif
