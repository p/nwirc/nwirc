 /*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "unbinput.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include "color.h"
#include "netinit.h"
#include "nwirc_libc.h"
#include "defs.h"
#include "lock.h"
#include "term_read.h"
#include "termcap.h"
#include "keydefs.h"

int   getstri = 0;
int		getstripos;
char *getstrs;

void
redraw_cmdline(int promptlen) {
	int i;
	for (i = 0 /* 2 */; i < getstri; ++i) {
		printf("\b \b");
	}
	for (i = promptlen; i > 0; --i) {
		printf("\b \b");
	}
	printf("%s", client.prompt);
	for (i = 0; i < getstri; ++i) {
		putchar(getstrs[i]);
	}
}


static char char_map[256];
static int	cm_is_init = 0;

static void
cm_init(void) {
	static const struct {
		char	ch;
		char	val;
	} assoc_chars[] = {
		{ 'q', 10 },
		{ 'w', 11 },
		{ 'e', 12 },
		{ 'r', 13 },
		{ 't', 14 },
		{ 'y', 15 },
		{ 'u', 16 },
		{ 'i', 17 },
		{ 'o', 18 },
		{ 'p', 19 },
		{ 0, 0    }
	};
	int	i;
	for (i = 0; assoc_chars[i].ch != 0; ++i) {
		if ((unsigned char)assoc_chars[i].ch < sizeof char_map) {
			char_map[(unsigned char)assoc_chars[i].ch] = assoc_chars[i].val;
		}
	}
}

#define LOOKUP_CHAR_MAP(ch) \
	((unsigned char)(ch) < sizeof char_map ? char_map[(ch)] : 0)

void
handle_fx(int ch) {
	int	lentmp;

	if (ch >= '0' && ch <= '9') {
		ch -= '0';
	} else {
		ch = LOOKUP_CHAR_MAP(ch);
	}
	lock_server(sbuf);
	if (ch <= sbuf->allchans && ch > 0) {
		int i;
		int	j;
		/*
		 * We have to take possibly unused ``slots''
		 * in the channel array into account, e.g.
		 * because the user left a channel. This
		 * makes it appear as though the channels
		 * from N to end are moved to N - 1 when
		 * N - 1 is left.
		 */
		for (i = 0, j = 0;
			 j < ch && i <= sbuf->allchans;
			 ++i) {
			if (sbuf->channels[i].name != NULL) {
				++j;
			}
		}
               
		if (i > sbuf->allchans) {
			/*
			 * Invalid channel index: Too high. 
			 * Pretend the ``slot'' isn't allocated
			 * by redrawing the default nwIRC prompt
			 */
			goto defaultprompt;
		}

		/*
		 * Ok, i is the real index + 1 now (because
		 * the user starts inputing at 1, while
		 * array indexing starts at 0
		 */
		ch = i - 1;
                 
		if (sbuf->channels[ch].name) {
			lentmp = strlen(client.prompt); /* save old length */
			if (client.allservers > 1) {
				n_snprintf(client.prompt, 64,
				"%s:%s> ", sbuf->address,
				sbuf->channels[ch].name);
			} else {
				n_snprintf(client.prompt, 64,"%s> ",
				sbuf->channels[ch].name);
			}
			strncpy(client.focus,
			sbuf->channels[ch].name, 64);
			client.focus[63] = 0;
			sbuf->active_chan = ch;
			redraw_cmdline(lentmp);
		} else {
			goto defaultprompt;
		}
	} else {

defaultprompt:
		client.focus[0] = 0;
		lentmp = strlen(client.prompt); /* save old length */
		if (client.allservers > 1)
			n_snprintf(client.prompt, 64,
			"nwIRC:%s> ", sbuf->address);
		else {
			strncpy(client.prompt, "nwIRC> ", 7);
			client.prompt[7] = 0;
		}
		redraw_cmdline(lentmp);
		sbuf->active_chan = -1;
	}
	unlock_server(sbuf);
} 

static struct termios	old_io;

/*
 * Exit handler to restore terminal setting when nwIRC exits
 */
void
restore_term(void) {
	(void) tcsetattr(STDIN_FILENO, TCSANOW, &old_io);
	printf("\033[0;0;0m");
}

static struct histent {
	char			*message;
	struct histent	*next;
	struct histent	*prev;
}	*hlist = NULL,
	*hlist_seek = NULL,
	*hlist_tail = NULL;

static void
history_append(char *msg) {
	struct histent	*hptr;
	static int		nmsg;

	if (hlist_tail == NULL) {
		hptr = hlist = n_xmalloc(sizeof(struct histent));
		hptr->prev = NULL;
		hlist_tail = hlist;
	} else {
		hptr = hlist_tail->next = n_xmalloc(sizeof(struct histent));
		hptr->prev = hlist_tail;
		hlist_tail = hlist_tail->next;
	}
	hptr->next = NULL;
	hptr->message = n_xstrdup(msg);
	if (nmsg == 100) {
		struct histent	*tmp = hlist;
		free(hlist->message);
		hlist = hlist->next;
		free(tmp);
		hlist->prev = NULL;
	} else {
		++nmsg;
	}
}

static void
do_draw_line(const char *line) {
	int	i;

	if (getstripos != getstri) {
		for (i = getstri - getstripos; i < getstri; ++i) {
			putchar(' ');
		}
	}
	for (i = 0; i < getstri; ++i) {
		printf("\b \b");
	}
	printf("%s", line);

	if (getstripos != getstri) {
		for (i = getstri - getstripos; i > 0; --i) {
			putchar('\b');
		}
	}
	fflush(stdout);
}
		

static char * 
history_search_backward(void) {
	char	*msg = NULL;
	if (hlist_seek == NULL) {
		if ((hlist_seek = hlist_tail) == NULL) {
			return NULL;
		}
		msg = hlist_seek->message;
	} else {
		if ((hlist_seek = hlist_seek->prev) == NULL) {
			return NULL;
		}
		msg = hlist_seek->message;
	}
	do_draw_line(msg);
	return msg;
}

static char *
history_search_forward(void) {
	char	*msg;
	if (hlist_seek == NULL) {
		return NULL;
	}
	if ((hlist_seek = hlist_seek->next) == NULL) {
		do_draw_line("");
		return "";
	/*	return NULL;*/
	}
	msg = hlist_seek->message;
	do_draw_line(msg);
	return msg;
}


/*
 * getstr() is a huge monolithic piece of crap - arguably some of the
 * messiest code I've ever written in my life. It currently stands there
 * ``as is'' because it actually works. This stuff is in dire need of
 * being rewritten with read_key(), a function I've already written but
 * not yet integrated (first attempt at that failed badly), so the
 * terminal sequences do not have to be parsed in the main loop
 * UPDATE: uses read_key() now... seems to work..
 */

#define DO_COPY(hptr, getstrs, getstri) \
	if (hptr != NULL) {			\
		int	len = strlen(hptr);	\
		memcpy(getstrs, hptr, len + 1); \
		getstripos = getstri = len; \
		hptr = NULL; \
	}


void
n_getstr(char *buf, int len, int mode) {
	static struct termios	new_io;
	char					*hptr = NULL;
	int						input;
	int						fd;
	int						ch;
	int						lentmp;
	static int				using_term_read = 0;
	int						i;

	if (cm_is_init == 0) {
		/* Initialize character map */
		cm_init();
		cm_is_init = 1;

		/* Initialize terminal handling stuff */ 
		if (get_termcap_entry() != NULL) {
			/* Using termcap */
			using_term_read = 1;
		} else if (get_termcap_from_curses() != NULL) {
			/* Using termcap as provided by curses */
			using_term_read = 1;
		} else {
			printcol(
"Couldn't find termcap database or terminal entry in termcap\n");
			printcol("Using dumb terminal settings\n");
		}
	}
	getstri = 0;
	getstrs = buf;
	fd = STDIN_FILENO;
	if (tcgetattr(fd, &old_io) == -1) {
		perror("tcgetattr");
		exit(1);
	}
	new_io = old_io;
  
	/* set terminal into non-canonical mode */
	new_io.c_lflag &= ~(ICANON | ECHO);

  /* VMIN specifies the minimum number of bytes read at time. I explicitly set
     it to 1 so that every char will be processed immediately. On *BSD, it's no
     problem to leave VMIN untouched, but on Linux/IRIX/... it is */
 
	new_io.c_cc[VMIN] = 1;
	new_io.c_cc[VTIME] = 0;
	new_io.c_cc[VINTR] = CINTR;
 	if (tcsetattr(fd, TCSANOW, &new_io) == -1) {
		perror("tcsetattr");
		exit(1);
	}
	--len;

	getstripos = 0;
	while (getstri < len) {
		if (using_term_read) {
			input = term_read();
			if (input == 0) {
				/*
				 * Indicates press of unsupported (by termcap entry)
				 * key - ignore
				 */
				continue;
			}
		} else {
			input = getchar();
			if (input == 033) {
				/*
				 * Escape sequences cannot be handled due to lack of
				 * termcap
				 */
				int	flags;
				flags = fcntl(STDIN_FILENO, F_GETFL);
				(void)fcntl(STDIN_FILENO, F_SETFL, flags | O_NONBLOCK);
				while ((ch = getchar()) != EOF)
					;
				(void)fcntl(STDIN_FILENO, F_SETFL, flags);
				continue;
			}
		}
		if (input == KEY_EOF) {
			if (client.noterm == 0) {
				exit(0);
			} else {
				sleep(INT_MAX);
			}
		}
		switch (input) {
		case '\n':
			hlist_seek = NULL;
			buf[getstri] = 0;
			len = getstri;
			break;  
		case '\b':
		case 127:
			hlist_seek = NULL;
			if (getstripos > 0) {
				if (getstripos != getstri) {
					memmove(&buf[getstripos - 1], &buf[getstripos],
						getstri - getstripos + 1);
					buf[getstri - 1] = 0;
					printf("\b \b%s \b", &buf[getstripos - 1]);
					for (i = 0; i < (getstri - getstripos); ++i) {
						putchar('\b');
					}
				} else {
					printf("\b \b");
				}
				--getstripos;
				--getstri;
			}
			break;
		case KEY_ESCAPE:
			ch = term_read();
			if (isalnum((unsigned char)ch)) {
				handle_fx(ch);
			}
			break;
		case KEY_F1:
		case KEY_F2:
		case KEY_F3:
		case KEY_F4:
			ch = input - KEY_F1;
			if (ch >= 0 &&
				ch < client.allservers &&
				servers[ch].sock > -1) {
				s_active_serv = ch;
				sbuf = &servers[s_active_serv];
				lentmp = strlen(client.prompt); /*save old len*/
				lock_server(sbuf);
				if (sbuf->active_chan > -1
					&& sbuf->active_chan < sbuf->allchans
					&& sbuf->channels[sbuf->active_chan].name != NULL) {
					n_snprintf(client.prompt, 64, "%s:%s> ",
						sbuf->address,
						sbuf->channels[sbuf->active_chan].name);
					strncpy(client.focus,
						sbuf->channels[sbuf->active_chan].name, 64);
					client.focus[63] = 0;
				} else {
					n_snprintf(client.prompt, 64, "nwIRC:%s> ",
						sbuf->address);
				}
				unlock_server(sbuf);
				redraw_cmdline(lentmp);
			} else {
				printcol("No such server\n");
			}
			break;
		case KEY_LEFT:
			if (getstripos > 0) {
				printf("\b");
				--getstripos;
			}
			break;
		case KEY_RIGHT:
			if (getstripos < getstri) {
				putchar(buf[getstripos]);
				++getstripos;
			}
			break;
		case KEY_UP:
			hptr = history_search_backward();
			DO_COPY(hptr, getstrs, getstri);
			break;
		case KEY_DOWN:
			hptr = history_search_forward();
			DO_COPY(hptr, getstrs, getstri);
			break;
		case KEY_HOME:
			while (getstripos > 0) {
				printf("\b");
				--getstripos;
			}
			break;
		case KEY_EOL:
			while (getstripos < getstri) {
				putchar(buf[getstripos]);
				++getstripos;
			}
			break;
		default:  
			hlist_seek = NULL;
			if (getstripos != getstri) {
				memmove(&buf[getstripos + 1], &buf[getstripos],
					getstri - getstripos + 1);
				buf[getstripos] = input;
				buf[getstri + 1] = 0;
				printf("%*s", getstri - getstripos + 1,
					&buf[getstripos]);
				for (i = 0; i < (getstri - getstripos); ++i) {
					putchar('\b');
				}
				++getstripos;
				++getstri;
			} else {
				buf[getstri++] = input;
				if (mode == U_NOECHO)
					putchar('*');
				else 
					putchar(input);
				++getstripos;
			}
			fflush(stdout);
		}
		fflush(stdout);
	}
	/*if (getstripos) {*/
		for (input = getstripos; input < (getstri/* - 1*/); ++input)
			putchar(' ');
	/*}*/
	for (input = getstri; input >= 1 /* was > */; input--)
		printf("\b \b");
	fflush(stdout);

	tcsetattr(fd, TCSANOW, &old_io);
	buf[getstri] = 0;
	if (getstri > 0) {
		history_append(buf);
	}
	getstri = 0;
}

