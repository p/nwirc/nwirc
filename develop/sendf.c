/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "sendf.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <dlfcn.h>
#include "nwirc_libc.h"
#include "color.h"
#include "module.h"
#include "defs.h"
#include "nwirc_api.h"


#ifndef NO_MODULE
/*
 * Table of lists containing registered modules. The type passed to
 * sendf minus one is used to index this table (it is guaranteed that
 * a send type is always above 0 and equal to or below SEND_MAX)
 */
static struct mod_entry {
	modfunc_t			func;
	struct module		*mod;
	struct mod_entry	*next;
} *mod_lists[SEND_MAX] = {
	NULL, NULL
};

void
send_register(struct module *m) {
	int					i;
	int					*sreg;
	char				buf[1024];
	struct mod_entry	*me;

	n_snprintf(buf, sizeof buf, "%s_sreg", m->name);
	if ((sreg = dlsym(m->handle, buf)) == NULL) {
		/* Does not wish to register for sending */
		return;
	}

	for (i = 0; sreg[i] != -1; ++i) {
		if (i < 0 || i > SEND_MAX) {
			printcol("Warning: Module %s attempted to register for"
					" invalid send value! (%d)\n", m->name, sreg[i]);
			continue;
		}
		me = n_xmalloc(sizeof *me);
		n_snprintf(buf, sizeof buf, "%s_send", m->name);
		if ((me->func = (modfunc_t)dlsym(m->handle, buf)) == NULL) {
			printcol("Could not resolve send function for module %s\n",
				m->name);
			free(me);
			return;
		}
		me->mod = m;
		me->next = mod_lists[sreg[i] - 1];
		mod_lists[sreg[i] - 1] = me;
	}
}


static int			level = 0; /* To avoid recursion with modules */

#endif /* #ifndef NO_MODULE */

void
sendf(int sock, int type, char *format, ...) { 
	char    			buf[8192];
	char				*p;
	char				*p2;
	char				*fmtptr[128];
	int     			i;
	va_list 			list;
	struct mod_entry	*mod;
	struct mod_context	context;

	p = buf;
	va_start(list, format);
	memset(buf, 0, 8192 * sizeof(char));
	n_vsnprintf(buf, 8191, format, list);
	va_end(list);

#ifndef NO_MODULE
	if (type != 0 && level == 0) {
		/* 
		 * type = 0 means we are dealing with an unspecified message.
		 * This is useful for modules wanting to call sendf()
		 */
		va_start(list, format);
		for (p2 = format, i = 0; *p2 != 0; ++p2) {
			if (*p2 == '%') {
				char	*tmp = va_arg(list, char *);
				if (i == 127) {
					printcol("Too many arguments for sendf()\n");
					exit(EXIT_FAILURE);
				}
				fmtptr[i] = tmp;
				++i;
			}
		}
		fmtptr[i] = NULL;
		va_end(list);
		context.then_sbuf = sbuf;
		context.then_rbuf = rbuf;
		context.msgtype = type;
		context.msg = &p;
		context.fmtptr = fmtptr;
		context.api_table = api_table;
		for (mod = mod_lists[type - 1]; mod != NULL; mod = mod->next) {
			/*
			 * call func with something like
			 * if (((int(*)(bla))mod->func) == 1) {
			 *     ... drop packet ...
			 * }
			 * We should probably store func as a function pointer, as
			 * opposed to a void pointer
			 */
			++level;
			if (mod->func(&context) != 0) {
				/* Module wishes to drop packet */
				--level;
				return;
			}
			--level;
		}
	}
#endif

	send(sock, p, strlen(p), 0);
}

