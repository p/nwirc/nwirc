/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef NETINIT_H
#define NETINIT_H

#ifndef DEFS_H
#include "defs.h"
#endif
#include <pthread.h>

extern int      filtering; 

extern unsigned long myaddr;

extern struct sockaddr_in  local;

extern pthread_mutex_t	sync_conn_mut;
extern pthread_cond_t	sync_conn_cond;
extern int				conn_status;

#ifndef NO6
extern struct sockaddr_in6 local6;
#endif

void *recvt       (void *dummy);
void *sendt       (void *dummy);
void *setup_conn  (void *dummy);
int ircconnect    (void       );

#endif
