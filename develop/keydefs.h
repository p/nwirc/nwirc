#ifndef KEYDEFS_H
#define KEYDEFS_H

#define KEY_ESCAPE	256
#define KEY_LEFT	260
#define KEY_RIGHT	261
#define KEY_UP		262
#define KEY_DOWN	263
#define KEY_F1		264
#define KEY_F2		265
#define KEY_F3		266
#define KEY_F4		267
#define KEY_F5		268
#define KEY_F6		269
#define KEY_F7		270
#define KEY_F8		271
#define KEY_F9		272
#define KEY_F10		273
#define KEY_F11		274
#define KEY_F12		275
#define KEY_F13		276
#define KEY_EOL		277
#define KEY_HOME	288

#include <termios.h>
#define KEY_EOF		TCION

#endif
