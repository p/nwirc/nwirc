/*
 * This file serves two purposes:
 * 1) As an interactive program to configure nwirc color settings -
 * compile with ``gcc confcol.c defs.c color.c nwirc_libc.c -o confcol
 * -lpthread -DUSE_MAIN'' to do this.
 * 2) As container for the current color settings and the helper
 * functions to load the settings from the config file, which can also
 * be used from within the client. Just needs to be compiled WITHOUT
 * -DUSE_MAIN and then linked with the rest of the client for this to
 * work
 */
#include "confcol.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <limits.h>
#include "color.h"
#include "nwirc_libc.h"

static struct setting /* {
	const char	*informal;
	const char	*name;
	int			value;
	int			changed;
} */ set_tab[] = {
	{ "channel message text",
		"msg_text", /*WHITE*/GREY, 0 },
	{ "channel message sender name if not you",
		"msg_sender_other", BLUE, 0 },
	{ "channel message sender name if you",
		"msg_sender_me", CYAN, 0 },
	{ "channel name in events and messages",
		"channel", GREY, 0 },
	{ "private message text",
		"privmsg_text", /*WHITE*/GREY, 0 },
	{ "private message sender name if not you",
		"privmsg_sender_other", RED, 0 },
	{ "private message sender name if you",
		"privmsg_sender_me", RED, 0 },
	{ "timestamp in events and messages",
		"time", GREY, 0 },
	{ "notice text",
		"notice_text", /*WHITE*/GREY, 0 },
	{ "notice sender",
		"notice_sender", MAGENTA, 0 },
	{ "ctcp, mode and topic messages",
		"ctcp", /*YELLOW*/GREY, 0 },
	{ "join messages",
		"join", GREEN, 0 },
	{ "nick change messages",
		"nickchange", /*YELLOW*/GREY, 0 },
	{ "part messages",
		"part", GREY, 0 },
	{ "quit messages",
		"quit", GREY, 0 },
	{ "background color",
		"background", -1, 0 },
	{ NULL,
		NULL, 0, 0 }
};

#define TABSIZE (sizeof set_tab / sizeof set_tab[0] - 1) 

/* More verbose handles for convenience. Careful - hardcoded */

struct colors /* {
	struct setting		*msg_text;
	struct setting		*msg_sender_other;
	struct setting		*msg_sender_me;
	struct setting		*channel;
	struct setting		*privmsg_text;
	struct setting		*privmsg_sender_other;
	struct setting		*privmsg_sender_me;
	struct setting		*time;
	struct setting		*notice_text;
	struct setting		*notice_sender;
	struct setting		*ctcp;
	struct setting		*join;
	struct setting		*nickchange;
	struct setting		*part;
	struct setting		*quit;
	struct setting		*background;
} */ colors = {
	&set_tab[0],
	&set_tab[1],
	&set_tab[2],
	&set_tab[3],
	&set_tab[4],
	&set_tab[5],
	&set_tab[6],
	&set_tab[7],
	&set_tab[8],
	&set_tab[9],
	&set_tab[10],
	&set_tab[11],
	&set_tab[12],
	&set_tab[13],
	&set_tab[14],
	&set_tab[15]
};
	

static struct setting *
lookup_setting(const struct setting *table, char **namep) {
	char	*p = *namep;
	size_t	namelen = 0;

	while (isalnum((unsigned char)*p) || *p == '_') {
		++namelen;
		++p;
	}
	if (*p != '=') {
		printcol("%^Invalid name or missing assignment for color setting ``%s''\n",
				GREY, *namep);
		exit(1);
	}

	for (; table->name != NULL; ++table) {
		if (strncmp(table->name, *namep, namelen) == 0) {
			/* Found! */
			*namep = p + 1;
			return (struct setting *)table;
		}
	}

	printcol("%^Unknown color setting ``%s''\n", GREY, *namep);
	exit(1);
	/* NOTREACHED */
	return NULL;
}


/*
 * Load existent custom color settings, if any
 */
void
load_color_settings(FILE *fd) {
	char	buf[128];

	while (fgets(buf, sizeof buf, fd) != NULL) {
		(void) strtok(buf, "\n");
		if (strncmp(buf, "color_", 6) == 0) {
			char			*p = buf + 6;
			int				newval;
			struct setting	*sp;

			sp = lookup_setting(set_tab, &p);
			if (*p == 0) {
				/* Assume default - don't change */
				continue;
			}

			if (sp == (set_tab + TABSIZE - 1)) {
				newval = bgcol_text_to_value(p);
			} else {
				newval = col_text_to_value(p);
			}
			if (newval == -1) {
				printcol(
				"%^Invalid color specified in ``%s'' - ignoring\n",
					GREY, buf); 
				continue;
			}
			sp->value = newval; 
		}
	}
	rewind(fd);
}


static int
get_selection(const char *text) {
	char	buf[16];
	char	*p;
	long	rc;

	if (text) {
		printcol("%^%s: ", GREY, text);
		fflush(stdout);
	}

	if (fgets(buf, sizeof buf, stdin) == NULL) {
		putchar('\n');
		if (ferror(stdin)) {
			perror("stdin");
			exit(1);
		} else {
			/* EOF */
			return 0;
		}
	}
	
	(void) strtok(buf, "\n");

	errno = 0;
	rc = strtol(buf, &p, 10);

	/* The == LONG_MIN check because caller needs to substract one */
	if (errno || rc == LONG_MIN || rc > INT_MAX || rc < INT_MIN) {
		return -1;
	}
	if (*p || p == buf) {
		return -1;
	}
	return (int)rc;
}

static int 
print_current(void) {
	int		i = 0;
	char	*timebuf = "00:00:00";
	char	*chan = "#channel";
	char	*text = "hello world";

	for (i = 0; i < TABSIZE; ++i) {
		if (set_tab[i].value != -1) {
			printcol("%^[%d] %^%s %s\n", GREY, i+1,
				set_tab[i].value, set_tab[i].informal,
				set_tab[i].changed? "(just changed)": "");
		} else {
			printcol("%^[%d] %s (not set - default)\n",
				GREY, i+1, set_tab[i].informal);
		}
	}

	printcol("%^Memorize the number of the setting you want to alter, then\n"
		"hit enter to see a few message examples with the current colors",
			GREY);
	while ((i = getchar()) != EOF && i != '\n')
		; /* nothing */

	if (i == EOF) {
		return 1;
	}

	printcol("%^%s%^ %s: %^<%^%s%^> %^%s\n",
		colors.time->value, timebuf,
		colors.channel->value, chan,
		colors.msg_text->value,
		colors.msg_sender_other->value, "bizfish",
		colors.msg_text->value,
		colors.msg_text->value, text);

	printcol("%^%s %^privmsg( -> bizfish ): %^hello\n",
		colors.time->value, timebuf,
		colors.privmsg_sender_other->value, colors.privmsg_text->value);

	printcol("%^%s %^[bizfish@3ffe:bc0:8000:0:8000:0:50e4:380e]"
		"NOTICE: %^foo\n",
		colors.time->value, timebuf,
		colors.notice_sender->value,
		colors.notice_text->value);
	printcol("%^%s%^ #sgi received CTCP TIME request from bizfish\n",
		colors.time->value, timebuf, colors.ctcp->value);

	printcol("%^%s %^* biztruder "
		"[~root@3ffe:bc0:8000:0:8000:0:50e4:380e] has joined #sgi\n",
		colors.time->value, timebuf,
		colors.join->value);

	printcol("%^%s %^* biztruder "
		"[~root@3ffe:bc0:8000:0:8000:0:50e4:380e] is now known as bt\n",
		colors.time->value, timebuf,
		colors.nickchange->value);

	printcol("%^%s %^* biztruder "
		"[~root@3ffe:bc0:8000:0:8000:0:50e4:380e] has left #sgi\n",
		colors.time->value, timebuf,
		colors.part->value);

	printcol("%^%s %^* biztruder "
		"[~root@3ffe:bc0:8000:0:8000:0:50e4:380e] has quit (\"Lost terminal\")\n",
		colors.time->value, timebuf,
		colors.quit->value);
	return 0;
}

static FILE *
get_tmp_file(char *buf, size_t bufsiz) {
	int				fd;
	unsigned long	i = 0;

	do {
		n_snprintf(buf, bufsiz, "/tmp/config-tmp-%lu", i++);
		if ((fd = open(buf, O_CREAT | O_EXCL | O_WRONLY, 0700)) == -1) {
			if (errno != EEXIST) {
				perror(buf);
				exit(1);
			}
		}
	} while (fd == -1);

	/* Got file! */
	return fdopen(fd, "w");
}


static int 
move_file(const char *in, const char *out) {
	FILE	*infd;
	FILE	*outfd;
	size_t	rc;
	char	buf[2048];

	if ((infd = fopen(in, "r")) == NULL) {
		perror(in);
		return -1;
	}
	if ((outfd = fopen(out, "w+")) == NULL) {
		perror(out);
		return -1;
	}

	while (fgets(buf, sizeof buf, infd) != NULL) {
		fputs(buf, outfd);
	}

	(void) fclose(infd);
	(void) fclose(outfd);
	remove(in);
}


#ifdef USE_MAIN

int
main(int argc, char *argv[]) {
	FILE				*fd;
	FILE				*fdnew;
	char				buf[1024];
	char				buf2[1024];
	int					n_fore = 0;
	int					n_back = 0;
	int					i;
	const struct color	*ctmp;
	struct setting		*sp;

	puts("Hello.");
	puts("This program will write to the file ``colors''. If you started");
	puts("it through the nwirc config generator, it will automatically");
	puts("enter this file into the master config file (``config''),");
	puts("otherwise you have to do it manually.\n");

	puts("You can always exit this program by hitting the EOF key");
	puts("(typically Ctrl-D.)\n");
	puts("Press enter to start");

	while ((i = getchar()) != EOF && i != '\n')
		; /* nothing */
	if (i == EOF) {
		return 0;
	}


#if 0
	if (argc != 2) {
		(void) fprintf(stderr, "Usage: %s configfile\n",
			argv[0]? argv[0]: "confcol");
		return 1;
	}
#endif

	printcol_prompt(0); /* Don't have printcol() redraw nwIRC prompt */

	for (ctmp = foreground, n_fore = 0; ctmp->name != NULL; ++ctmp) {
		++n_fore;
	}

	for (ctmp = background, n_back = 0; ctmp->name != NULL; ++ctmp) {
		++n_back;
	}

	if ((fd = fopen(/*argv[1]*/"colors", "r+")) == NULL) {
		if ((fd = fopen(/*argv[1]*/"colors", "w+")) == NULL) {
			perror(/*argv[1]*/"colors");
			return 1;
		}
	} else {
		load_color_settings(fd);
	}

	for (;;) {
		char	*p;
		int		is_background;
		if (print_current() != 0) {
			/* EOF */
			break;
		}
		while ((i = get_selection("Enter number of setting to alter")) == -1
			|| i > TABSIZE
			|| i < 0) {
			(void) printcol("%^Bad choice - try again\n", GREY);
		}

		if (i == 0) {
			/* EOF */
			break;
		}

		sp = &set_tab[--i];
		printcol("%^The current setting for this option (%s - %s) is %^%s%^\n",
			GREY, sp->name, sp->informal, sp->value,
			(p = col_value_to_text(sp->value))? p: "<none>", GREY);
		puts("Enter color to replace it:");

		if (sp == (set_tab + TABSIZE - 1)) {
			is_background = 1;
			for (i = 0; background[i].name != NULL; ++i) {
				printcol("[%d] %^%s\n", i+1,
					background[i].value, background[i].name);
			}
		} else {
			is_background = 0;
			for (i = 0; foreground[i].name != NULL; ++i) {
				printcol("[%d] %^%s\n", i+1,
					foreground[i].value, foreground[i].name);
			}
		}

		printcol("%^", GREY);
		while ((i = get_selection(NULL)) == -1
			|| (is_background && i > n_back)
			|| (is_background == 0 && i > n_fore)
			|| i < 0) {
			(void) printcol("%^Bad choice - try again\n", GREY);
		}

		if (i == 0) {
			break;
		}
		if (is_background) {
			sp->value = background[--i].value;
			set_bg_color(sp->value);
		} else {
			sp->value = foreground[--i].value;
		}
		sp->changed = 1;
	}

	/* Write out all changes */
	fdnew = get_tmp_file(buf, sizeof buf);

	while (fgets(buf2, sizeof buf2, fd) != NULL) {
		(void) strtok(buf2, "\n");
		if (strncmp(buf2, "color_", 6) == 0) {
			char	*p = buf2 + 6;

			sp = lookup_setting(set_tab, &p);
			if (sp->changed) {
				/* This will be renewed later on - drop line */
				continue;
			}
		}
		(void) fprintf(fdnew, "%s\n", buf2);
	}

	for (i = 0; i < TABSIZE; ++i) {
		if (set_tab[i].changed) {
			printcol("%^Writing new setting for %s\n", GREY, set_tab[i].name);
			if (i == TABSIZE - 1) {
				(void) fprintf(fdnew, "color_%s=%s\n",
					set_tab[i].name, bgcol_value_to_text(set_tab[i].value));
			} else {
				(void) fprintf(fdnew, "color_%s=%s\n",
					set_tab[i].name, col_value_to_text(set_tab[i].value));
			}
		}
	}

	if (fclose(fd) == EOF
		|| fclose(fdnew) == EOF) {
		perror("fclose");
	}

	set_bg_color(0);
	/* Replace old file with new one */
	printcol(
"%^Done. If you invoked confcol manually (not through the nwirc config\n"
"generator), don't forget to add the file to your nwirc master config\n",
	GREY);
	return move_file(buf, /*argv[1]*/"colors");
}

#endif /* #ifdef USE_MAIN */

