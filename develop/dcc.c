/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/* name NOTICE: DCC send filename (ip.add.re.ss) */


#ifdef NO_DCC

int	dcc_get(void) {  return 0; }
int	dcc_send(char *data) { return 0; }

/* Empty DCC stubs */
#else /* NO_DCC not defined */
#include "dcc.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include "netinit.h"
#include "logging.h"
#include "color.h"
#include "defs.h"
#include "lock.h"
#include "nwirc_libc.h"
#include "confcol.h"

/*
 * TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! (...):
 *
 * - Currently only one send/get at time supported :-(, due to the fact that we
 *   only have a single sockaddr_in to store them in commands.c. Make a list for
 *   it!
 * - IPv6 support
 * - verify that we really invited the client that's now connection, based on
 *   their address. That should be easy, once we support parallel transfers
 */
static void *
do_get(void *dummy) {
	int    sock,
	       all,
	       readbytes,
	       minutes,
	       seconds,
		   rc,
	       i;
	time_t start, 
	       end;
	char   buf[4096];
	FILE  *fd;

#ifdef pthread_detach /* IRIX doesn't have this */
	pthread_detach(pthread_self());
#endif

	dummy = NULL;
	if (dcc_waiting == 0) {
		printcol("%^No transfer waiting", colors.msg_text->value);
		return NULL;
	}

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		printcol("%s\n", strerror(errno));
		return NULL;
	}
	fd = fopen(filename, "r");

	if (fd) {
		printcol("File ``%s\'\' already exists.\n", filename);
		i = strlen(filename);
		while (i < 1023) {
			filename[i] = '_';
			++i;
			filename[i] = 0;
			fd = fopen(filename, "r");
			if (fd == NULL)
				break;
		}
		if (i == 1023) {
			printcol("What the heck...\n");
			return NULL;
		} 
		printcol("Saving as ``%s\'\' instead.\n", filename);
	}
	fd = fopen(filename, "w+");

	if (fd == NULL) {
		printcol("%s: %s\n", filename, strerror(errno));
		return NULL;
	}
	all = 0;
	if (connect(sock,(struct sockaddr *)&dcc_addr, sizeof(struct sockaddr))==0){
		fd_set			fds;
		struct timeval	t;
		--dcc_waiting;
		start = time(NULL); 
		printcol("Receiving...\n");
		do {
			FD_ZERO(&fds);
			FD_SET(sock, &fds);
			t.tv_sec	= 30;
			t.tv_usec	= 0;
			rc = select(sock + 1, &fds, NULL, NULL, &t);
			if (rc == -1) {
				printcol("select: %s\n", strerror(errno));
			} else if (rc == 0) {
				printcol("Transfer timed out: 30 seconds inactivity.\n");
				printcol("%d bytes transferred\n", all);
				fclose(fd);
				close(sock);
				return NULL;
			}
			readbytes = recv(sock, buf, 4096, 0);
			if (readbytes < 0) {
				printcol("An error occured: %s. Bailing out\n",strerror(errno));
				printcol("%d bytes transferred\n", all);
				fclose(fd);
				close(sock);
				return NULL;
			} else if (readbytes != 0) { 
				if (fwrite(buf, sizeof(char), readbytes, fd) !=
					(size_t)readbytes) {
					printcol("Cannot write buffer to disk: %s. Bailing out\n",
							strerror(errno));
					fclose(fd);
					close(sock);
					return NULL;
				}
				all += readbytes;
			}
		} while (readbytes != 0 && all < size);
		end = time(NULL);
		write(sock, "\01DCC QUIT\01", sizeof "\01DCC QUIT\01");
		sleep(2);
		shutdown(sock, SHUT_RDWR);
		close(sock);
		fclose(fd);
		seconds = end - start;
		minutes = seconds / 60;
		seconds %= 60;
		sprintf(buf, "%02d:%02d", minutes, seconds);
		printcol("Transfer of %s completed (%d bytes in %s minutes).\n", 
				filename, size, buf); 
		if (all != size) {
			printcol("Warning: File size mismatch. Is %d, should be %d\n",
					all, size);
		}
		return NULL;
	} else {
		printcol("%s\n", strerror(errno));
		return NULL;
	}
	/* NOTREACHED */
}

int 
dcc_get(void) {
	int			rc;
	pthread_t	pt;

	rc = pthread_create(&pt, NULL, do_get, NULL); 
	if (rc != 0) {
		printcol("thread: %s\n", strerror(rc));
	}
	return 0;
}

struct send_arg {
	struct server	*server;
	char			*data;
	int				timeout;
};

static void
free_arg(struct send_arg *s) {
	free(s->data);
	free(s);
}

static void *
do_send(void *arg) {
	int                 sock = -1,
	                    sock2,
	                    rc,
						proto,
	                    n,
	                    sent,
	                    portnum = 1300,
	                    sendtimeout, 
	                    seconds,
	                    minutes;
	char                packet[1024],
	                    receiver[1024],
	                    filename[1024],
 	                    *data;
	FILE               *fd;
	time_t              start,
	                    end;
	fd_set              f;
	struct timeval      tim;
	struct stat         statbuf;
	struct sockaddr_in  local;
#ifndef NO6
	struct sockaddr_in6 local6;
#endif
	struct send_arg		*sa = arg;
	struct server		*server;
#ifdef HAVE_SOCKLEN_T
	socklen_t             localsiz;
#else
	int						localsiz;
#endif

#ifdef pthread_detach /* IRIX doesn't have this */
	pthread_detach(pthread_self());
#endif

	data = sa->data;
	sendtimeout = sa->timeout;
	server = sa->server;

	if (sscanf(data, "%s %s", receiver, filename) != 2) {
		printf("Usage: /dcc send <name> <file>\n");
		free_arg(sa);
		return NULL;
	}

	
	lock_server(server);
	proto = server->protocol;
	unlock_server(server);
	if (proto == 4) {
		sock = socket(AF_INET, SOCK_STREAM, 0);
	} else {
#ifndef NO6
		sock = socket(AF_INET6, SOCK_STREAM, 0);
#endif
	}

	if (sock < 0) {
		printcol("%s\n", strerror(errno));
		free_arg(sa);
		return NULL;
	}

	if (proto == 4) {
		localsiz = sizeof(struct sockaddr_in);
		local.sin_family = AF_INET;
		local.sin_addr.s_addr = INADDR_ANY;
	} else {
#ifndef NO6
		localsiz = sizeof(struct sockaddr_in6);
		local6.sin6_family = AF_INET6;
		local6.sin6_addr = in6addr_any;
#endif
	}

	for (;;) {
		if (proto == 4) {
			local.sin_port = htons(portnum);
			if (bind(sock, (struct sockaddr *)&local, (unsigned)localsiz) < 0) {
				printcol("%s\n", strerror(errno));
				++portnum;
				if (portnum == 1350) { /* giving up */ 
					free_arg(sa);
					return NULL;
				}
			} else {
				break;
			}
		} else {
#ifndef NO6
			local6.sin6_port = htons( portnum );
			if (bind(sock, (struct sockaddr *)&local6, localsiz) < 0) {
				printcol("%s\n", strerror(errno));
				++portnum;
				if (portnum == 1350) { /* giving up */
					free_arg(sa);
					return NULL;
				}
			} else {
				break;
			}
#endif
		}
	}
      
	if (stat(filename, &statbuf) == -1) {
		printcol("%s: %s\n", filename, strerror(errno));
		free_arg(sa);
		return NULL;
	}
	lock_server(server);
	n_snprintf(packet, 1024, ":%s PRIVMSG %s :\01DCC SEND %s %ld %d %ld\01\r\n",
            server->nickname, receiver, filename, ntohl(client.myaddr), portnum, 
            (long)statbuf.st_size);
	unlock_server(server);
	packet[1023] = 0;

	fd = fopen(filename, "r");
	if (fd == NULL) {
		printcol("%s: %s\n", filename, strerror(errno));
		free_arg(sa);
		return NULL;
	}

	tim.tv_sec  = 10;
	tim.tv_usec = 0;
	if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tim, sizeof(tim)) < 0) {
		printcol("%s\n", strerror(errno));
		free_arg(sa);
		return NULL;
	}

	lock_server(server);
	if (send(server->sock, packet, strlen(packet), 0) <= 0) {
		printcol("send: %s\n", strerror(errno));
		free_arg(sa);
		unlock_server(server);
		return NULL;
	}
	unlock_server(server);

	if (listen(sock, 1) < 0) {
		printcol("%s\n", strerror(errno));
		free_arg(sa);
		return NULL;
	}

	FD_ZERO(&f);
	FD_SET(sock, &f);
	if (sendtimeout != 0) {
		/* Client connection timeout specified */ 
		printcol("Waiting for %d seconds\n", sendtimeout);
		tim.tv_sec  = sendtimeout;
		tim.tv_usec = 0;
		rc = select(sock + 1, &f, NULL, NULL, &tim); 
	} else {
		/* No timeout, wait forever for client to connect */
		printcol("Waiting forever\n");
		rc = select(sock + 1, &f, NULL, NULL, NULL);
	}
	if (rc == -1 || rc == 0) {
		if (rc == -1) {
			printcol("select: %s. Bailing out\n", strerror(errno));
		} else {
			printcol("DCC send timed out after %d seconds\n", sendtimeout);
		}
		close(sock);
		free_arg(sa);
		return NULL;
	}

	/*
	 * At this point, there /must/ be a connection waiting for us
	 */
	sock2 = accept(sock, (struct sockaddr *)&local, &localsiz);

	if (sock2 < 0) {
		printcol("%s\n", strerror(errno));
		close(sock);
		free_arg(sa);
		return NULL;
	}

	sent = 0;
	printcol( "Sending...\n" );
	start = time( NULL );
	do {
		n = fread(packet, 1, 1024, fd);
		if (send(sock2, packet, n, 0) < 0) {
			printcol("An error occured: %s. Bailing out\n", strerror(errno));
			close(sock2);
			close(sock);
			free_arg(sa);
			return NULL;
		}
		sent += n;
	} while (sent < statbuf.st_size); 
	end = time(NULL);
	seconds = end - start;
	minutes = seconds / 60;
	seconds %= 60;

	sprintf(packet, "%02d:%02d", minutes, seconds);
	printcol("Transfer of %s completed (%d bytes in %s minutes).\n", 
				filename, (int)statbuf.st_size, packet); 
	usleep(10000000);
	shutdown(sock2, SHUT_RDWR);
	shutdown(sock, SHUT_RDWR);
	close(sock);
	close(sock2);
	free_arg(sa);
	return NULL;
}


int 
dcc_send(char *data) {
	int				rc;
	pthread_t		pt;
	struct send_arg	*sa;

	sa = n_xmalloc(sizeof *sa);
	sa->server = sbuf;
	lock_server(sa->server);
	sa->timeout = sa->server->dcctimeout;
	unlock_server(sa->server);
	sa->data = n_xstrdup(data);
  
	rc = pthread_create(&pt, NULL, do_send, sa);
	if (rc != 0) {
		printcol("pthread_create: %s\n", strerror(rc));
	}
	return 0;
}

#endif /* NO_DCC not defined */

