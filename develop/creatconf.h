/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef CREATCONF_H

#define CREATCONF_H

#define YES_NO          1
#define TEXT            2
#define TEXT_LIST       3 

#define HIDDEN     1 
#define NOT_HIDDEN 0
#define ALNUM      (1 << 1)
#define NUMERIC    (1 << 2)


struct confvar {
	char *key[3],
	     *text[3], /* text[0] alone for non-text-lists */ 
	     *introtext; /* for text lists only */
	int  method,
	     levels, /* again, for text lists only */
	     flag[3];
};

#ifndef STDIO_H
#define STDIO_H
#include <stdio.h> /* for FILE */
#endif

char *text    (FILE *fd, struct confvar data, char *islist, int element); 
int createconf(struct confvar *data, char *filename);
char *putvar  (struct confvar data);
char *readconf(char *filename);
char *scanvar (char *buf, char *key);
int makeconfig(void);
int readconfig(void);

#endif
