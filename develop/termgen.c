#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>

static size_t 
n_strlcat(char *dest, const char *src, size_t bufsiz) {
	const char	*start = dest;
	size_t		destlen = strlen(dest);

	dest += destlen;
	bufsiz -= destlen;
	while (--bufsiz > 0) {
		if ((*dest++ = *src++) == 0) {
			return dest - start;
		}
	}

	/* buffer size exceeded */
	*dest = 0;
	for (bufsiz = 0; *src != 0; ++bufsiz)
		;
	return dest - start + bufsiz;
}


/*
 * fprintf() replacement which exists if I/O error encountered
 */
static void
x_fprintf(FILE *out, const char *msg, ...) {
	va_list	va;
	int		rc;
	va_start(va, msg);
	rc = vfprintf(out, msg, va);
	if (rc == EOF) {
		perror("vfprintf");
		exit(EXIT_FAILURE);
	}
	if (fflush(out) == EOF) {
		perror("fflush");
		exit(EXIT_FAILURE);
	}
	va_end(va);
}

static char *
transform(int ch) {
	static char	buf[256];

	if (ch == 033) {
		strcpy(buf, "\\E");
	} else if (isprint((unsigned char)ch)) {
		buf[0] = (unsigned char)ch;
		buf[1] = 0;
	} else {
		sprintf(buf, "\\0%o", (unsigned)ch);
	}
	return buf;
}

static int
get_sequence(FILE *out, const char *name, const char *message) {
	struct termios	t;
	struct termios	old;
	int				flags;
	int				ch;
	char			buf[2048];

	if (tcgetattr(0, &t) == -1) {
		perror("tcgetattr");
		return -1;
	}
	old = t;
	t.c_lflag &= ~(ICANON | ECHO);
	t.c_cc[VMIN] = 1;
	t.c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSANOW, &t) == -1) {
		perror("tcsetattr");
		return -1;
	}

	printf("Please hit the %s key\n", message);

	ch = getchar();
	strcpy(buf, transform(ch));
	if ((flags = fcntl(0, F_GETFL)) == -1
		|| fcntl(0, F_SETFL, flags | O_NONBLOCK) == -1) {
		perror("fcntl");
		(void)tcsetattr(0, TCSANOW, &old);
		return -1;
	}
	while ((ch = getchar()) != EOF) {
		n_strlcat(buf, transform(ch), sizeof buf);
	}

	do {
		x_fprintf(out, "%.2s=%s:", name, buf);
	} while (*(name += 2) == '!' && *++name != 0);

	(void)fcntl(0, F_SETFL, flags);
	(void)tcsetattr(0, TCSANOW, &old);
	return 0;
}


/*
 * Prints prompt, reads string from keyboard and returns a pointer to
 * that. prompt is not printed if null pointer
 */
static char * 
get_string(const char *prompt) {
	static char	buf[256];

	if (prompt) {
		printf("%s", prompt);
		fflush(stdout);
	}
	if (fgets(buf, sizeof buf, stdin) == NULL) {
		if (ferror(stdin)) {
			perror("fgets");
			exit(EXIT_FAILURE);
		} else {
			exit(0);
		}
	}
	if (strtok(buf, "\n") == NULL) {
		int	ch;
		do {
			ch = getchar();
		} while (ch != EOF && ch != '\n');
	}
	return buf;
}



int
main(int argc, char *argv[]) {
	char			*p;
	FILE			*out;
	int				fd;
	int				ch;
	int				i;
	int				cmd = -1;
	struct winsize	w;
	static struct {
		const char	*tc_name;
		const char	*ti_name; /* not implemented */
		const char	*msg;
	}				keys[] = {
		{ "kd", 0, "down-arrow" },
		{ "kl", 0, "left-arrow" },
		{ "kr", 0, "right-arrow" },
		{ "ku!cm!up", 0, "up-arrow" },
		{ "k1", 0, "F1" },
		{ "k2", 0, "F2" },
		{ "k3", 0, "F3" },
		{ "k4", 0, "F4" },
		{ "k5", 0, "F5" },
		{ "k6", 0, "F6" },
		{ "k7", 0, "F7" },
		{ "k8", 0, "F8" },
		{ "k9", 0, "F9" },
		{ "k;", 0, "F10" },
		{ "F1", 0, "F11" },
		{ "F2", 0, "F12" },
		{ "@7!kH", 0, "end-of-line" },
		{ "kN", 0, "page down/next page" },
		{ "kP", 0, "page up/prev page" },
		{ "kh", 0, "home/pos 1" },
		{ "al", 0, "insert line" },
		{ "kD", 0, "delete character" },
		{ NULL, 0, NULL }
	};

	/*
	 * Obscure hardcoded values that probably can't be determined
	 * empirically
	 */
	static struct {
		const char	*name;
		const char	*value;
	}				hardcoded[] = {
		{ "te", "\\E[?1049l" },	/* strings to end programs using cup (huh~) */
		{ "ce", "\\E[K" },		/* clear to end of line */
		{ "cd", "\\E[J" },		/* clear to end of screen */
		{ "cl", "\\E[H\\E[2J" },	/* clear screen and home cursor */
		{ "ti", "\\E[?1049h" },
		{ "vb", "\\E[?5h\\E[?5l" }, /* visible bell */
		{ "kI", "\\E[2~" }, /* insert at cursor */
		{ "so", "\\E[7m" }, /* standout begin */
		{ "se", "\\E[27m" }, /* standout end */
		{ NULL, NULL }
	};


	if (argc != 2) {
		x_fprintf(stderr, "Usage: genterm [output]\n");
		return EXIT_FAILURE;
	}

	fd = open(argv[1], O_CREAT | O_EXCL | O_WRONLY, S_IRWXU);
	if (fd == -1) {
		if (errno == EEXIST) {
			/* File already exists */
			x_fprintf(stderr, "File already exists. Overwrite? [y/n] ");
			ch = toupper(get_string(NULL)[0]);
			if (ch == 'N') {
				return 0;
			} else if (ch != 'Y') {
				x_fprintf(stderr, "Bad response\n");
				return EXIT_FAILURE;
			}

			/* Overwrite old file */
			fd = open(argv[1], O_CREAT | O_TRUNC | O_WRONLY, S_IRWXU);
			if (fd == -1) {
				perror(argv[1]);
				return EXIT_FAILURE;
			}
		} else {
			perror(argv[1]);
			return EXIT_FAILURE;
		}
	}
	if ((out = fdopen(fd, "w")) == NULL) {
		perror("fdopen");
		return EXIT_FAILURE;
	}
	p = get_string("Enter the name of your terminal: ");
	x_fprintf(out, "%s|", p);
	p = get_string("Enter a description: ");
	x_fprintf(out, "%s:\\\n", p);

	/* Get row and column count */
#ifdef TIOCGWINSZ
	cmd = TIOCGWINSZ;
#elif defined(TIOCGSIZE)
	cmd = TIOCGSIZE;
#endif
	if (cmd == -1 || ioctl(0, cmd, &w) == -1) {
		/* Default = 80x25 */
		x_fprintf(stderr, "Assuming 80x25 terminal\n");
		w.ws_row = 25;
		w.ws_col = 80;
	}
	x_fprintf(out, "\t:li#%d:co#%d:", w.ws_row, w.ws_col);
			

	for (i = 0; keys[i].tc_name != NULL; ++i) {
		get_sequence(out, keys[i].tc_name, keys[i].msg);
		if ((i % 5) == 0) {
			if (keys[i + 1].tc_name != NULL) {
				x_fprintf(out, "\\\n");
				x_fprintf(out, "\t:");
			}
		}
	}
	for (i = 0; hardcoded[i].name != NULL; ++i) {
		x_fprintf(out, "%s=%s:", hardcoded[i].name, hardcoded[i].value); 
		if ((i % 5) == 0) {
			if (keys[i + 1].tc_name != NULL) {
				x_fprintf(out, "\\\n");
				x_fprintf(out, "\t:");
			}
		}
	}
	p = get_string("Do you wish to continue another terminal? [y/n] ");
	if (toupper(p[0]) == 'Y') {
		p = get_string("Enter the name of the terminal to continue: ");
		x_fprintf(out, "tc=%s:", p);
	}
	x_fprintf(out, "\n");
	puts("Done.");
	return 0;
}

