/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "creatconf.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include "parse.h"
#include "defs.h"
#include "nwirc_libc.h"
#include "unbinput.h"  /* for n_getstr() */
#include "confcol.h"


/* 
 * Asks YES/NO question. Writes variable_name=1 for YES, variable_name=0 for NO
 */

void
yes_no(FILE *fd, struct confvar data) {
	char buf[4];
	printf("%s [y\\n] ", data.text[0]);
	n_getline(buf, 4);
	if (toupper((unsigned char)buf[0]) == 'Y') {
		fprintf(fd, "%s=1\n", data.key[0]);
	} else {
		fprintf(fd, "%s=0\n", data.key[0]);
	}
}


/* 
 * Reads a text string from the user. It allows options for alphanumeric or
 * numeric only text input and the possibility to disguise text by displaying 
 * asterisks (*) instead of the real input 
 */

char *
text(FILE *fd, struct confvar data, char *islist, int element) {
	static char buf[1024];
	int         i,
	            n,
	            len,
	            runagain;
	if (islist == NULL) {
		printf("%s ", data.text[0]);
	}
	do {
		runagain = 0;
		if (data.flag[element] & HIDDEN) {
			/* print asterisks instead of the input on the screen */
			n_getstr(buf, 1023, U_NOECHO);
			putchar('\n');
		} else {
			/* normal input */
			n_getline(buf, 1023);
		}

		if (data.flag[element] & ALNUM) {
			/* read alphanumeric text /only/ */
			for (i = 0; buf[i]; i++) {
				if (isalnum((unsigned char)buf[i]) == 0) {
					if (islist && buf[i] == '.') break;
					else {
						/* bad input, read again */
						puts("Only characters 0-9, a-z and A-Z allowed."
							" Try again.");
						runagain = 1;
						break;
					}
				}
			}
		} else if (data.flag[element] & NUMERIC) {
			/* read numeric text /only/ */
			for (i = 0; buf[i]; i++) {
				if (isdigit((unsigned char)buf[i]) == 0) {
					if (islist && buf[i] == '.') break;
					else {
						/* bad input, read again */
						puts("Only digits 0-9 allowed. Try again.");
						runagain = 1;
						break; 
					}
				}
			}
		}
	} while (runagain == 1 || (strchr(buf, '=') != NULL &&
         puts("The ``='' character is a reserved token. Don't use it!")));

	if (islist == NULL)
		/* function was called for a stand-alone string */
		fprintf(fd, "%s=%s\n", data.key[0], buf);
	else { 
		/* function was invoked by textlist( ), input is appended */
		strncat(buf, ",", 1);
		len = strlen(islist);
		n = 2048 - len;
		strncat(islist, buf, n);
	}
	return buf;
}


void
textlist(FILE *fd, struct confvar data) {
	char buf[3][2048] = { { 0 }, { 0 }, { 0 } },
	    *p;
	int  done = 0,
	     i,
	     j    = 0;

	/*too many nested variables (relatively arbitrary value, could be increased)
	*/
	if (data.levels > 3) return;
	printf("%s\n", data.introtext);
	while (done == 0) {
		++j;
		for (i = 0; i < data.levels; i++) {
			/* for every variable in the list */

			printf("%s (#%d) ", data.text[i], j);
			if (i == 0) printf("(Press ``.'' + Enter to stop)  ");

			/* get input */
			text(fd, data, buf[i], i);
			if (i == 0) {
				p = strrchr(buf[0], '.');
				if (p != NULL && p[1] == ',' && p[2] == 0) {
					*p = 0;
					if (p != buf[0]) { 
						/* user /did/ enter some data before ``.'' */
						for (i = 0; i < data.levels; i++) 
							fprintf(fd, "%s=%s\n", data.key[i], buf[i]);
					}
					done = 1;
					break;
				}
			}
		}
	} 
}

int
createconf(struct confvar *data, char *filename) {
	FILE *fd;
	int   i = 0;
	fd = fopen(filename, "w+");
	if (fd == NULL) {
		perror(filename);
		return 1;
	}
	while (data[i].key[0]) {
		switch (data[i].method) {
		case YES_NO:    yes_no(fd, data[i]);     break;
		case TEXT:      text(fd, data[i], NULL, 0); break;
		case TEXT_LIST: textlist(fd, data[i]);   break; 
		}
		++i;
	}
	fclose(fd);
	return 0;
}

    
      
char *
readconf(char *filename) {
	char       *ret;
	struct stat statbuf;
	FILE       *fd;

	if (stat(filename, &statbuf) != -1) {
		fd = fopen(filename, "r");
		if (fd == NULL) {
			perror("Can't load config file");
			return NULL;
		}
		ret = malloc((statbuf.st_size + 1) * sizeof(char));
		if (ret == NULL) {
			perror("Can't load config file");
			/* using_config = 0; */
			fclose(fd);
			return NULL;
		}
		if (fread(ret, sizeof(char), statbuf.st_size, fd) != 
			(size_t)statbuf.st_size) {
			fprintf(stderr, "Config file (nwirc.conf) seems corrupted. Create a "
						"new "
                       "one, or fix the existing one (if you delete the file "
                       "and run nwirc again, you will be prompted to generate a "
                       "new one).\n");
			/* using_config = 0; */
			fclose(fd);
			return NULL;
		}
		ret[statbuf.st_size] = 0;
		fclose(fd);
		return ret;
	} else {
		perror("Cannot stat config");
		return NULL;
	} 
	/* NOTREACHED */
	return NULL;
}
      
    

char *
scanvar(char *buf, char *key) {
	char   *tmp,
	       *p,
	       *ret;
	size_t  len,
			buf_len;
	ssize_t n;
	len = strlen(key);
	buf_len = strlen(buf);
	while ((tmp = strstr(buf, key)) != NULL) {
		if (tmp < (buf + buf_len - len)) {
			tmp += len;
			if (*tmp == '=') {
				++tmp;
				p = strchr(tmp, '\n');
				if (p)
					n = (int)(p - tmp) + 1;
				else 
					n = (int)((buf + buf_len) - tmp) + 1;
				ret = malloc(n * sizeof(char));
				if (ret == NULL)
					return NULL;
				strncpy(ret, tmp, n);   
				ret[n - 1] = 0;
				return ret;
			}
		} else /* end of buffer reached */
			return NULL;
		buf = tmp + len;
	}
	return NULL;
}

int
makeconfig(void) {
	char            buf[128];
	int             i;
	FILE           *fd;
	struct confvar  foo[] = {
		{
			{"nickname", NULL, NULL},
			{"Enter your nickname ", NULL, NULL},
			NULL,
			TEXT, 0, {0, 0, 0}
		},

		{
			{"server", NULL, NULL},
			{"Enter server ", NULL, NULL},
			NULL,
			TEXT, 0, {0, 0, 0}
		},
#ifndef NO6
		{
			{"proto6", NULL, NULL},
			{"Use IPv6 protocol? (If you don't know what you're doing, choose ``n'') ",
			NULL, NULL},
			NULL,
			YES_NO, 0, {0, 0, 0}
		},
#endif /* #ifndef NO6 */
		{
			{"autojoin", NULL, NULL},
			{"Channel ", NULL, NULL},
"Enter auto-joined channels. If you don't want to auto-join any, press ``.'' + enter",
			TEXT_LIST, 1, {0, 0, 0}
		},
		{
			{"logging", NULL, NULL},
			{"Enable logging? ", NULL, NULL},
			NULL,
			YES_NO, 0, {0, 0, 0}
		},
		{
			{"splitlogs", NULL, NULL},
			{"Split channel log files (by creating one log file per day rather than keeping a single log file)? ", NULL, NULL},
			NULL,
			YES_NO, 0, {0, 0, 0}
		},
#ifndef NO_DCC
		{
			{"autodcc", NULL, NULL},
			{"Auto-accept dcc-send requests? ", NULL, NULL},
			NULL,
			YES_NO, 0, {0, 0, 0}
		},
		{
			{"dcctimeout", NULL, NULL},
			{
"Enter a dcc send timeout (i.e. how long I shall wait for a client to accept\n"
"the send request until I stop waiting and close the socket) in seconds. Enter\n"
			"0 if I shall wait /forever/ ", NULL, NULL},
			NULL,
			TEXT, 0, {NUMERIC, 0, 0}
		},
#endif /* #ifndef NO_DCC */
		{
			{"invisible", NULL, NULL},
			{"Switch to invisible mode (+i) on connect? ", NULL, NULL},
			NULL,
			YES_NO, 0, {0, 0, 0}
		},
#ifndef NO_FILTER
		{
			{"using_filter", NULL, NULL},
			{"Use filter? ", NULL, NULL},
			NULL,
			YES_NO, 0, {0, 0, 0}
		},
#endif /* #ifndef NO_FILTER */
#ifndef NO_FSERV
		{
			{"user", "password", NULL},
			{"Enter username ", "Enter password ", NULL},
"You can now create accounts for nwIRC's internal file server, called ``fserv''. If you don't want to use this feature, press ``.'' + enter",
			TEXT_LIST, 2, {ALNUM, ALNUM | HIDDEN, 0}
		},
#endif /* #ifndef NO_FSERV */
		{
			{"quit", NULL, NULL},
			{"Enter quit message (only enter for default) ", NULL, NULL},
			NULL,
			TEXT, 0, {0, 0, 0}
		},
		{{NULL, NULL, NULL}, {NULL, NULL, NULL}, NULL, 0, 0, {0, 0, 0}}
	};

	/*
	 * text split because ANSI only guarantees 509 chars in string
	 * literal
	 */
	printf("\nGreetings!\n"
"You will now be prompted to enter the server(s) you will be connecting to, your\n"
"nickname, auto-joined channels and some other stuff. The reason I'm harrassing\n"
"you with this stuff already is that it saves you reading the manual, yet you\n"
"don't miss anything important (e.g. logging). My own laziness was one of\n"
"the reasons I wrote nwIRC - I didn't want to read the manual for other "
"clients!\n\n"
"(The configuration files (and possibly logs later on) will be stored in\n"
"~/.nwirc)\n\n"
		);

	fd = fopen("config", "w+");
	if (fd == NULL) {
		perror("Can't create config file");
		return 1;
	}
	i = 0;
	do {
		n_snprintf(buf, 64, "nwirc%d.conf", i);
		if (createconf(foo, buf) == 0) {
			fprintf(fd, "%s\n", buf);
			printf(
			       "Do you wish to automatically connect to a further server on startup, parallely?\n"
			       "[y\\n] ");
			n_getline(buf, 4);
			if (toupper((unsigned char)buf[0]) != 'Y')
				break;
		}
		++i;
	} while (i < MAXSERVERS);
	printf("Do you want to configure the client colors? (be warned, the"
		" configuration program to do so looks awful and is unpleasant"
		" to use) [y/n] ");
	fflush(stdout);
	n_getline(buf, 2);
	if (toupper((unsigned char)buf[0]) == 'Y') {
		pid_t	child;
		if ((child = fork()) == (pid_t)-1) {
			perror("fork");
			return 1;
		} else if (child == 0) {
			execl("./confcol", "confcol", "colors", (char *)0);
			perror("./confcol");
			_exit(1);
		} else {
			int	status;
			waitpid(child, &status, 0);
			if (WIFEXITED(status)) {
				if ((status = WEXITSTATUS(status)) == 0) {
					(void) fprintf(fd, "%s\n", "colors");
				}
				fclose(fd);
				return WEXITSTATUS(status);
			} else {
				fclose(fd);
				return 1;
			}
		}
	}

	fclose(fd);
	puts("All done. Start nwIRC again.");
	return 0;
}


int
readconfig(void) {
	char           *data = NULL,
					*p,
					buf[128];
	int             i;
	struct confvar  confdat;
	FILE           *fd,
					*fd2;

	fd = fopen("config", "r");
	if (fd == NULL) {
		perror("Can't open config file");
		return 1;
	}

	i = 0;
	client.allservers = 0;
	while (fgets(buf, 128, fd) != NULL) {
		(void) strtok(buf, "\n");
		fd2 = fopen(buf, "a+");
		if (fd2 == NULL) {
			fprintf(stderr, "Can't open %s. Skipping.\n", buf);
			continue;
		}

		if (strcmp(buf, "colors") == 0) {
			FILE	*newfd = fopen(buf, "r");
			if (newfd == NULL) {
				perror(buf);
			} else {
				load_color_settings(newfd);
			}
			fclose(fd2);
			fclose(newfd);
			continue;
		}

		data = readconf(buf);
		if (data == NULL)
			return 1;

		if ((servers[i].nickname = scanvar(data, "nickname")) == NULL) {
			confdat.key[0] = "nickname", confdat.text[0] = "Enter your nickname ",
				confdat.flag[0] = 0;
			if ((servers[i].nickname = n_strdup(text(fd, confdat, NULL, 0))) ==
			    NULL) {
				free(data);
				return 1;
			}
		}
		if ((servers[i].address = scanvar(data, "server")) == NULL) {
			confdat.key[0] = "server", confdat.text[0] = "Enter server ",
				confdat.flag[0] = 0;
			if ((servers[i].address = n_strdup(text(fd, confdat, NULL, 0))) ==
			    NULL) {
				free(data), free(servers[i].nickname);
				return 1;
			}
		}
#ifndef NO6
		if ((p = scanvar(data, "proto6")) != NULL) {
			servers[i].protocol = strtol(p, NULL, 10) == 1 ? 6 : 4;
			free(p);
		} else
			servers[i].protocol = 4;
#else
		servers[i].protocol = 4;
#endif

		if ((p = scanvar(data, "port")) != NULL) {
			servers[i].port = strtol(p, NULL, 10);
			free(p);
		} else
			servers[i].port = 6667;

		servers[i].autojoin = scanvar(data, "autojoin");

		if ((p = scanvar(data, "logging")) != NULL) {
			servers[i].logging = strtol(p, NULL, 10);
			free(p);
		} else
			servers[i].logging = 1;

		if ((p = scanvar(data, "splitlogs")) != NULL) {
			servers[i].splitlogs = strtol(p, NULL, 10);
			free(p);
		} else
			servers[i].splitlogs = 0;

#ifndef NO_DCC
		if ((p = scanvar(data, "autodcc")) != NULL) {
			servers[i].autodcc = strtol(p, NULL, 10);
			free(p);
		} else
			servers[i].autodcc = 0;
#else
		servers[i].autodcc = 1; /* immediately call empty stub */
#endif

#ifndef NO_DCC
		if ((p = scanvar(data, "dcctimeout")) != NULL) {
			servers[i].dcctimeout = strtol(p, NULL, 10);
			free(p);
		} else
			servers[i].dcctimeout = 60;
#else
		servers[i].dcctimeout = 1;
#endif

		if ((p = scanvar(data, "invisible")) != NULL) {
			servers[i].invisible = strtol(p, NULL, 10);
			free(p);
		} else
			servers[i].invisible = 1;

#ifndef NO_FILTER
		if ((p = scanvar(data, "using_filter")) != NULL) {
			servers[i].using_filter = strtol(p, NULL, 10);
			free(p);
		} else
			servers[i].using_filter = 0;
#else
		servers[i].using_filter = 0;
#endif

		if ((servers[i].quitmsg = scanvar(data, "quit"))) {
			if ((p = strchr(servers[i].quitmsg, '\n')))
				*p = 0;
		}
		if (servers[i].quitmsg == NULL || servers[i].quitmsg[0] == 0) {
			servers[i].quitmsg = "leaving";
		}

#ifndef NO_FSERV
		if ((client.user = scanvar(data, "user")) != NULL &&
		    (client.password = scanvar(data, "password")) != NULL) {
			servers[i].serving = 1;
		} else {
			servers[i].serving = 0;
			if (client.user)
				free(client.user);
			if (client.password)
				free(client.password);
		}
#else
		servers[i].serving = 0;
#endif

		++client.allservers;
		++i;
		fclose(fd2);
	}
	if (data) {
		free(data);
	}
	fclose(fd);
	return 0;
}

