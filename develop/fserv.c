/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifdef NO_FSERV

/* Empty fserv stubs */
void	fserv(char *address, char *usr, char *command) { return; }

#else /* NO_FSERV not defined */

#include "fserv.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <dirent.h>
#include <unistd.h>
#include <pthread.h>
#include "dcc.h"
#include "defs.h"
#include "commands.h"
#include "color.h"
#include "nwirc_libc.h"

/* static const char       *rootdir = "fserv"; */
static struct server		*s;
static int					logged_in = 0;
static struct user_list		*USER_LIST = NULL;
static struct user_list		*START_LIST = NULL;
/*
 * Mutex to protect flist routine (It's (not yet) thread-safe)
 */
static pthread_mutex_t	flist_mut = PTHREAD_MUTEX_INITIALIZER;


static int
fseeklist(const char *address) {
	struct user_list	*buf = START_LIST;
	time_t				timbuf;
	int					i;

	i = 0;

	while (buf != NULL) {
		if (buf->address) {
			if (strcmp(buf->address, address) == 0) {
				timbuf = time(NULL);
				if ((timbuf - buf->last_action) > 120) { 
					return -2; /*timeout*/
				} else {
					buf->last_action = timbuf;
					return i; 
				}
			}
		} 
		++i;
		buf = buf->next;
	}
	return -1;
}
     

static void
flogin(char *address, char *usr, char *data) {
	char	*temp;
	char	*p;
	char	*p2;
	char	namebuf[1024];
	int		i;  

	
	pthread_mutex_lock(&flist_mut);
	i = fseeklist(address);
	if (i > -1) {
		pthread_mutex_unlock(&flist_mut);
		privmsg_send(s, usr, "You\'re already logged in.\n", QUIET);
		return;
	}
	p = strchr(data, ':');
	if (p == NULL) {
		pthread_mutex_unlock(&flist_mut);
		privmsg_send(s, usr, "Bad user data.", QUIET);
		return;
	}
	for (p2 = data; p2 < p; ++p2) {
		if (isalnum((unsigned char)*p2) == 0) {
			pthread_mutex_unlock(&flist_mut);
			privmsg_send(s, usr, "Bad user data.", QUIET);
			return;
		}
	}
	for (++p2; *p2; ++p2) {
		if (isalnum((unsigned char)*p2) == 0) {
			pthread_mutex_unlock(&flist_mut);
			privmsg_send(s, usr, "Bad user data.", QUIET);
			return;
		}
	}
	p2 = p + 1;
	*p = 0, ++p;
	n_snprintf(namebuf, 1024, "%s,", data);
	if ((temp = strstr(client.user, namebuf)) != NULL) {
		if (temp != client.user && temp[-1] != ',') {
			pthread_mutex_unlock(&flist_mut);
			privmsg_send(s, usr, "Bad user data.", QUIET);
			return;
		}
		for (p2 = client.user, i = 0;
			(p2 = strchr(p2, ',')) != NULL && p2 < temp;
			p2++, i++)
			;
		n_snprintf(namebuf, 1024, "%s,", p);
		if ((temp = strstr(client.password, namebuf)) != NULL) {
			if (temp != client.password && temp[-1] != ',') {
				pthread_mutex_unlock(&flist_mut);
				privmsg_send(s, usr, "Bad user data.", QUIET);
				return; 
			}
			for (p2 = client.password;
				(p2 = strchr(p2, ',')) != NULL && p2 < temp;
				p2++, i--)
				;
			if (i != 0) {
				pthread_mutex_unlock(&flist_mut);
				privmsg_send(s, usr, "Bad user data.", QUIET);
				return;
			}
			USER_LIST->next = malloc(sizeof(struct user_list)); 
			if (USER_LIST->next == NULL) {
				pthread_mutex_unlock(&flist_mut);
				return;
			}
			USER_LIST = USER_LIST->next;
			strncpy(USER_LIST->name, data, 100);
			USER_LIST->name[99] = 0;
			USER_LIST->address = n_strdup(address);
			if (USER_LIST->address == NULL) {
				pthread_mutex_unlock(&flist_mut);
				return;
			}
			USER_LIST->last_action = time(NULL);
			strncpy(USER_LIST->path, "fserv", 6);
			privmsg_send(s, usr, "Login successful. Have fun :)", QUIET);
			++logged_in;
			/*login successful*/
		} else
			privmsg_send(s, usr, "Bad user data.", QUIET);
	} else {
		/*user doesn't exist*/
		privmsg_send(s, usr, "Bad user data.", QUIET);
	}
	pthread_mutex_unlock(&flist_mut);
}

static void
flogout(char *address, char *usr, int status) { 
	int					i;
	int					j;
	struct user_list	*buf = START_LIST, *buf2;

	pthread_mutex_lock(&flist_mut);
	i = fseeklist(address);
	if (i != -1) {
		--i;
		for (j = 0; j < i; j++) {
			buf = buf->next;
		}
		buf2 = buf->next;
		if (buf->address) {
			free(buf->address);
			buf->address = 0;
			free(buf);
		}
		--logged_in;
		if (status != FORCED) {
			privmsg_send(s, usr, "Have a nice day.", QUIET);
		}
	} else {
		privmsg_send(s, usr, "You\'re not logged in.\n", QUIET);
	}
	pthread_mutex_unlock(&flist_mut);
}

struct flist_arg {
	char	*address;
	char	*usr;
};



static void *
do_flist(void *arg) {
	struct flist_arg	*f = arg;
	char				*address;
	char				*usr;
	DIR					*dirbuf;
	struct dirent		*direntbuf;
	struct stat			statbuf;
	struct user_list	*usrbuf = START_LIST;
	char				buf[1024];
	int					i;
	int					j;
	int					rc;
	int					size;

#ifdef pthread_detach /* IRIX doesn't have this */
	pthread_detach(pthread_self());
#endif

	rc = pthread_mutex_lock(&flist_mut);
	if (rc != 0) {
		printcol("thread_mutex_lock: %s\n", strerror(rc));
		free(arg);
		return NULL;
	}
	address = f->address;
	usr     = f->usr;  

	i = fseeklist(address);
	if (i == -1) {
		privmsg_send(s, usr, "You\'re not logged in.\n", QUIET);
		pthread_mutex_unlock(&flist_mut);
		free(arg);
		return NULL;
	} else if (i == -2) {
		privmsg_send(s, usr, "Timeout (120s). Log in again.", QUIET);
		pthread_mutex_unlock(&flist_mut); /* flogout locks this */
		flogout(address, usr, FORCED);
		free(arg);
		return NULL;
	}
	for (j = 0; j < i; j++) {
		usrbuf = usrbuf->next;
	}
	dirbuf = opendir("fserv");
	if (dirbuf == NULL) {
		privmsg_send(s, usr, "Directory doesn't exist yet.", QUIET);
		(void) mkdir("fserv", S_IRWXU);
		pthread_mutex_unlock(&flist_mut);
		free(arg);
		return NULL;
	}
	i = 0;
	size = 0;
	(void) getcwd(client.cwd, 1024);
	(void) chdir(usrbuf->path);
	while ((direntbuf = readdir(dirbuf)) != NULL) {
		if ((i % 5) == 0) {
			usleep(1000000); /*so we don't get kicked for flooding*/
		}
		if (stat(direntbuf->d_name, &statbuf) != -1) {
			if (S_ISDIR(statbuf.st_mode)) {
				n_snprintf(buf, 1024, "%s - DIR", direntbuf->d_name);
			} else {
				n_snprintf(buf, 1024, "%s - %ld bytes",
						direntbuf->d_name, (long int)statbuf.st_size);
				size += statbuf.st_size;
			}
		} else {
			n_snprintf(buf, 1024, "%s - ???", direntbuf->d_name);
		}
		privmsg_send(s, usr, buf, QUIET);
		++i;
	}
	(void) chdir(client.cwd);
	(void) closedir(dirbuf);
	n_snprintf(buf, 1024, "%d file(s), %d byte(s)", i, size);
	buf[1023] = 0;
	usleep(1000000);
	privmsg_send(s, usr, "---", QUIET);
	privmsg_send(s, usr, buf, QUIET);
	pthread_mutex_unlock(&flist_mut);
	free(arg);
	return NULL;
}

static void
flist(char *address, char *usr) { 
	struct flist_arg	*f;
	pthread_t			pt;
	int					rc;

	f = n_xmalloc(sizeof *f);

	f->address = address;
	f->usr     = usr;
	rc = pthread_create(&pt, NULL, do_flist, f);
	if (rc != 0) {
		printcol("thr_create: %s\n", strerror(rc));
	}
}

static void
fgetfil(char *address, char *usr, char *fil) {
	char				buf[1024];
	char				file[1024];
	struct stat			statbuf;
	struct user_list	*listbuf = START_LIST;
	int					i;
	int					j;

	pthread_mutex_lock(&flist_mut);
	i = fseeklist(address);
	if (i == -1) {
		pthread_mutex_unlock(&flist_mut);
		privmsg_send(s, usr, "You\'re not logged in.", QUIET);
		return;
	} else if (i == -2) {
		privmsg_send(s, usr, "Timeout (120s). Log in again.", QUIET);
		pthread_mutex_unlock(&flist_mut); /* flogout needs to lock */
		flogout(address, usr, FORCED);
		return;
	} else {
		for (j = 0; j < i; j++)
			listbuf = listbuf->next;
	}
	if (strcmp(file, ".") == 0 ||
		strcmp(file, "..") == 0 ||
		strchr(file, '/') != NULL) {
		pthread_mutex_unlock(&flist_mut);
		privmsg_send(s, usr, "Illegal file name.", QUIET);
		return;
	}
	getcwd(client.cwd, 1024);
	if (n_strcasecmp(client.cwd, listbuf->path) != 0) {
		if (chdir(listbuf->path) == -1) {
			pthread_mutex_unlock(&flist_mut);
			privmsg_send(s, usr, "You\'re not in a valid directory.", QUIET);
			return;
		}
	} 
	strncpy(file, fil, 1024);
	file[1023] = 0;
	if (stat(file, &statbuf) == -1) {
		pthread_mutex_unlock(&flist_mut);
		printf("%s: %s\n", file, strerror(errno));
		privmsg_send(s, usr, "No such file.", QUIET);
		return;
	} else {
		n_snprintf(buf, 1024, "%s %s", usr, file);
		pthread_mutex_unlock(&flist_mut);
		dcc_send(buf);
	}
	chdir(client.cwd);
}

/*void fcd( char *user, char *directory )
{*/
  

static void
fhelp(char *usr) {
  int   i = 0;
  char *text[] = {
"                            Welcome to FileServ 1.0!",
" ,--------------------------~~~~~~~~~~~~~~~~~~~~~~~~",
" |        What's it?         |",
" |---------------------------'----------------------,",
" | Fserv is a simple, optional IRC file server that |",
" | comes with nwIRC v0.7b+.                         |",
" `--------------------------------------------------'",
" ,---------------------------,",
" |        How to use?        |",
" |---------------------------'----------------------,",
" | Easy. If the server is ``running'', you're able  |",
" | to log in (considering you have an account) and  |",
" | browse/download served files. To apply a command,|",
" | send it as private message, prefixed with a      |",
" | ``.fs'', to the server. E.g.:                    |",
" | /msg serving_client .fs login myname:mypass      |",
" `--------------------------------------------------'",
" ,---------------------------,",
" |          Commands         |",
" |---------------------------'----------------------,",
" |   login user:password     |        logout        |",
" |      cd <directory>       |     get <filename>   |",
" |          list             |         help         |",
" `---------------------------'----------------------'" 
};      
	for (i = 0; i < 24; i++) {
		if ((i % 5) == 0) {
			usleep(1000000);
		}
		privmsg_send(s, usr, text[i], QUIET);
	}
} 

void
fserv(char *address, char *usr, char *command) {
	s = rbuf;
	printf("Fserv command from user %s (%s): %s\n",
			address, usr, command);
	if (s->serving != 1) {
		privmsg_send(s, usr, "Service currently not available.", QUIET);
		return;
	}
	if (logged_in == 0) {
		USER_LIST = malloc(sizeof(struct user_list));
		if (USER_LIST == NULL) {
			return;
		}
		USER_LIST->next = NULL;
		USER_LIST->address = NULL;
		START_LIST = USER_LIST;
	}
	switch (toupper((unsigned char)*command)) {
	case 'L':
		if (n_strncasecmp(command, "login ", 6) == 0) {
			flogin(address, usr, (char *)command + 6);
		} else if (n_strcasecmp(command, "logout") == 0) {
			flogout(address, usr, NOT_FORCED);
		} else if (n_strcasecmp(command, "list") == 0) {
			flist(address, usr);
		} else { 
			privmsg_send(s, usr, "Unknown command.", QUIET);
		}
		break;
	case 'H':
		if (n_strcasecmp(command, "help") == 0) {
			fhelp(usr);
		} else {
			privmsg_send(s, usr, "Unknown command.", QUIET);
		}
		break;
	case 'G':
		if (n_strncasecmp(command, "get ", 4) == 0) { 
			fgetfil(address, usr, (char *)command + 4);
		} else {
			privmsg_send(s, usr, "Unknown command.", QUIET);
		}
		break;
	default:
		privmsg_send(s, usr, "Unknown command.", QUIET);
	}
}
  
#endif /* NO_FSERV not defined */

