/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "logging.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "defs.h"
#include "color.h"
#include "nwirc_libc.h"


int
getchanindex(struct server *s, char *channame) {
	int i;

	for (i = 0; i < s->allchans; i++) {
		if (s->channels[i].name != NULL &&
			n_strcasecmp(s->channels[i].name, channame) == 0) {
			/*&&
			(s->logging == 0 || s->channels[i].fd != NULL)
			) */
			return i; 
		}
				
	}
	return -1;
}


void
maketimestamp(char *buf) {
	struct tm *timestamp;
	time_t     now;

	now = time(NULL);
	timestamp = localtime(&now);
	sprintf(buf, "%02d:", timestamp->tm_hour);
	sprintf(buf + 3, "%02d:", timestamp->tm_min);
	sprintf(buf + 6, "%02d", timestamp->tm_sec);
}


void
cleanupfiles(int srv) {
	int    i;
	time_t tim;

	for (i = 0; i < servers[srv].allchans; i++) {
		if (servers[srv].channels[i].fd) {
			tim = time(NULL);
			fprintf(servers[srv].channels[i].fd,
					"\nSession in %s closed at %s\n",  
					servers[srv].channels[i].name, asctime(localtime(&tim)));
			fflush(servers[srv].channels[i].fd);
			fclose(servers[srv].channels[i].fd);
			servers[srv].channels[i].fd = NULL;
		}
	}
}


int
open_channel_log(struct server *s, struct chan *c) {
	char	filebuf[256];
	FILE	*fd;
	time_t	tim = time(NULL);

	if (s->splitlogs) {
		n_snprintf(filebuf, 256, "logs/%s-%02d%02d%02d",
			c->name,
			1900+localtime(&tim)->tm_year, /* XXX is this portable?!??!?! */
			localtime(&tim)->tm_mon+1,
			localtime(&tim)->tm_mday);
	} else {
		n_snprintf(filebuf, 256, "logs/%s.log", c->name);
	}
	fd = fopen(filebuf, "a+");
	if (fd == NULL) {
		if (mkdir("logs", 0700) != 0) {
			printcol("mkdir: %s\n", strerror(errno));
			return -1;
		}
		fd = fopen(filebuf, "w+");
		if (fd == NULL) {
			printcol("fopen: %s\n", strerror(errno));
			return -1;
		}
	}
	c->fd = fd;

	if (s->splitlogs) {
		/* Remember current day of month to switch logs if neeed */
		c->logday = localtime(&tim)->tm_mday;
	}

	return 0; 
}

void
close_channel_log(struct chan *c) {
	if (c->fd != NULL) {
		(void) fclose(c->fd);
		c->fd = NULL;
	}
}

void
write_log(struct server *s, char *c, char *form, ...) {
	int     i;
	char    timebuf[16],
	        buf[128],
	       *p;
	FILE   *fd;
	va_list list;

	va_start(list, form);
	if (s->logging != 1) return;
	i = getchanindex(s, c);
	if (i == -1) {  /* seems to be a regular user */
		for (p = c; *p; ++p) {
			*p = tolower((unsigned char)*p);
			if (isprint((unsigned char)*p) == 0)
				*p = '_';
		}
		n_snprintf(buf, 128, "logs/%s.log", c);
		fd = fopen(buf, "a+");
		if (fd == NULL) {
			mkdir("logs", 0700);
			fd = fopen(buf, "w+");
			if (fd == NULL) {
				printcol("fopen: %s\n", strerror(errno));
				return;
			}
		}
	} else {
#if 0
		memcpy(&fd, &s->channels[i].fd, sizeof(fd));
#endif
		time_t	tim;

		if (s->splitlogs
			&& (tim = time(NULL)) != (time_t)-1
			&& localtime(&tim)->tm_mday != s->channels[i].logday) {
			/* A new day - need to switch logs */
			(void) fprintf(s->channels[i].fd, "New day - closing log\n");
			close_channel_log(&s->channels[i]);
			(void) open_channel_log(s, &s->channels[i]);

			fd = s->channels[i].fd;
			if (fd != NULL) {
				(void) fprintf(s->channels[i].fd, "New day.\n");
			}
		} else {
			fd = s->channels[i].fd;
		}
	}

	/*
	 * 20141016: Quickfix hack to avoid crashes caused by write_log() calls during
	 * shutdown (not clear why this never caused problems before)
	 */
	if (fd == NULL) {
		return;
	}

	maketimestamp(timebuf);
	fprintf(fd, "%s ", timebuf);
	while (*form) {
		switch (*form) {
		case '%':
			++form;
			switch (*form) {
			case 's':
				p = va_arg(list, char*);
				fprintf(fd, "%s", p);
				break;
			default:
				fprintf(fd, "%%%c", *form);
			}
			break;
		default:
			fputc(*form, fd);
		}
		++form;
	}
	va_end(list);
	fputc('\n', fd);
	fflush(fd);
	if (i == -1)
		fclose(fd);
}

