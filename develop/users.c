/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "users.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "color.h"
#include "logging.h"
#include "lock.h"
#include "nwirc_libc.h"

struct chan_user *chan_users = NULL;

int             current = -1;

void
initusers(char *data) {
	char				*p,
						*p2;
	time_t				tim;
	int					found_me;
	struct chan_user	*ubuf,
						*ubufstart;

	if (current < 0 || current > rbuf->allchans)
		return;
	if (data == NULL)
		return;
	tim = time(NULL);
	p = data;
	ubuf = malloc(sizeof(struct chan_user));
	if (ubuf == NULL)
		return;
	ubufstart = ubuf;
	found_me = 0;
	while ((p2 = strchr(p, ' ')) != NULL) {
		*p2 = 0;
		if (*p == '@') {
			if (found_me == 0 && n_strcasecmp(&p[1], rbuf->nickname) == 0) {
				rbuf->channels[current].am_operator = 1;
				found_me = 1;
			}
			ubuf->is_operator = 1;
			ubuf->name = n_strdup(&p[1]);
		} else {
			ubuf->is_operator = 0;
			ubuf->name = n_strdup(p);
		}
		ubuf->signon = tim;
		p = p2 + 1;
		ubuf->next = malloc(sizeof(struct chan_user));
		if (ubuf->next == NULL)
			break;
		ubuf = ubuf->next;
	}
	ubuf->next = NULL;
	ubuf->name = NULL;
	if (rbuf->channels[current].users == NULL) {
		rbuf->channels[current].users = ubufstart;
	} else {
		while (rbuf->channels[current].users->next != NULL)
			rbuf->channels[current].users =
				rbuf->channels[current].users->next;	/* XXX */
		rbuf->channels[current].users->next = ubufstart;
	}
}

void
clearusers(void *ptr, char *name) {
	int					i;
	struct chan_user	*ubuf,
						*ubuf2;
	struct server		*s = ptr;
	i = getchanindex(s, name);
	if (i == -1)
		return;
	if (s->channels[i].users == NULL)
		return;
	ubuf = s->channels[i].users;
	s->channels[i].users = NULL;
	while (ubuf->next) {
		ubuf2 = ubuf;
		ubuf = ubuf->next;
		if (ubuf2->name)
			free(ubuf2->name);
		free(ubuf2);
	}
	if (ubuf->name)
		free(ubuf->name);
	free(ubuf);
}


void
adduser(char *name, char *ch) {
	int					i;
	struct chan_user	*ubuf;

	i = getchanindex(rbuf, ch);
	if (i == -1) {
		printf("channel %s doesn't exist\n", ch);
		return;
	}
	if (rbuf->channels[i].users == NULL) {
		rbuf->channels[i].users = malloc(sizeof(struct chan_user));
		if (rbuf->channels[i].users) {
			rbuf->channels[i].users->name = n_strdup(name);
			rbuf->channels[i].users->signon = time(NULL);
			rbuf->channels[i].users->next = NULL;
		}
		return;
	}
	for (ubuf = rbuf->channels[i].users;
	     ubuf->next != NULL;
	     ubuf = ubuf->next);
	if (ubuf->name == NULL) {
		ubuf->name = n_strdup(name);
		ubuf->signon = time(NULL);
		return;
	}
	ubuf->next = malloc(sizeof(struct chan_user));
	if (ubuf->next) {
		ubuf->next->name = n_strdup(name);
		ubuf->next->signon = time(NULL);
		ubuf->next->next = NULL;
	}
}

void
remuser(char *name, char *ch, int is_quit, char *quitmsg) {
	struct chan_user	**ubuf,
						*ubuf2;
	int					i,
						j;

	if (is_quit == 0) {	/* parting or kicked from /one/ channel */
		i = getchanindex(rbuf, ch);
		if (i == -1)
			return;
		j = i;
		++i;
	} else			/* quit -> parting /all/ channels */
		j = 0, i = rbuf->allchans;

	for (; j < i; j++) {	/* only looped once if parting/kicked */
		if (rbuf->channels[j].users == NULL)
			continue;
		ubuf = &rbuf->channels[j].users;
		if ((*ubuf)->name &&
		    n_strcasecmp((*ubuf)->name, name) == 0) {
			if (rbuf->channels[j].name && quitmsg != NULL && is_quit != 0) {
				write_log(rbuf, rbuf->channels[j].name,
					"%s has quit (%s)", name, quitmsg);
			}
			free((*ubuf)->name);
			ubuf2 = (*ubuf);
			rbuf->channels[j].users = rbuf->channels[j].users->next;
			free(ubuf2);
			continue;
		}
		while ((*ubuf) && (*ubuf)->next) {
			if ((*ubuf)->next->name &&
			    n_strcasecmp((*ubuf)->next->name, name) == 0) {
				if (rbuf->channels[j].name && is_quit != 0)
					write_log(rbuf, rbuf->channels[j].name,
						"%s has quit (%s)", name, quitmsg);
				ubuf2 = (*ubuf)->next;
				(*ubuf)->next = (*ubuf)->next->next;
				free(ubuf2->name);
				free(ubuf2);
				break;
			}
			ubuf = &(*ubuf)->next;
		}
	}
}

void
change_userdata(char *name, char *newname, int operator, char *channel)
{
	struct chan_user	*ubuf;
	int					i;

	if (newname != NULL) {	/* caller wants to change nickname */
		for (i = 0; i < rbuf->allchans; i++) {
			ubuf = rbuf->channels[i].users;
			while (ubuf) {
				if (ubuf->name && n_strcasecmp(ubuf->name, name) == 0) {
					free(ubuf->name);
					ubuf->name = n_strdup(newname);
					break;
				}
				ubuf = ubuf->next;
			}
		}
	} else {		/* caller wants to change operator status */
		i = getchanindex(rbuf, channel);
		if (i == -1)
			return;
		ubuf = rbuf->channels[i].users;
		while (ubuf) {
			if (ubuf->name && n_strcasecmp(ubuf->name, name) == 0) {
				ubuf->is_operator = operator;
				break;
			}
			ubuf = ubuf->next;
		}
	}
}



void
printusers(char *ch) {
	int					i;
	struct chan_user	*c;

	if (ch == NULL)
		return;
	lock_server(sbuf);
	i = getchanindex(sbuf, ch);
	if (i != -1 && sbuf->channels[i].users != NULL) {
		unlock_server(sbuf);
		c = sbuf->channels[i].users;
		while (c->next) {
			if (c->name) {
				printf("%s", c->name);
				if (c->is_operator == 1)
					printf(" (operator)");
				printf("\n");
			}
			c = c->next;
		}
		if (c->name) {
			printf("%s", c->name);
			if (c->is_operator == 1)
				printf(" (operator)");
		}
		printcol("\n");	/* so the prompt is redrawn */
	} else {
		unlock_server(sbuf);
	}
}

