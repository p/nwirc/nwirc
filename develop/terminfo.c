/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "terminfo.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "nwirc_libc.h"

static char *terminfo_paths[] = {
	"/usr/share/terminfo",
	"/usr/share/lib/terminfo",
	NULL
};

static struct terminfo_entry *
do_get_entry(const char *file, const char *name) {
	FILE		*fd;
	char		buf[FILENAME_MAX + 1];
	struct terminfo_entry	*ret;

	sprintf(buf, "%s/%c/%s", file, name[0], name);
	
	if ((fd = fopen(buf, "r")) == NULL) {
		if (errno != ENOENT) {
			/* File exists but couldn't be opened */
			perror(buf);
		}
		return NULL;
	}

	if ((ret = malloc(sizeof *ret)) == NULL) {
		perror("malloc");
		return NULL;
	}

	if ((ret->nbytes =
	   	fread(ret->buf, sizeof(char), sizeof ret->buf, fd)) == sizeof ret->buf) {
		fprintf(stderr, "Terminal entry for %s too large\n", name);
		free(ret);
		return NULL;
	} else if (ret->nbytes == 0) {
		fprintf(stderr, "Terminal entry for %s is empty\n", name);
		free(ret);
		return NULL;
	}
	return ret;
}

static struct terminfo_entry *
do_get_terminfo_entry(const char *name) {
	int						i;
	struct terminfo_entry	*p = NULL;
	
	for (i = 0; terminfo_paths[i] != NULL; ++i) {
		if ((p = do_get_entry(terminfo_paths[i], name)) != NULL) {
			break;
		}
	}
	if (p == NULL) {
		fprintf(stderr, "Error: Couldn't find terminfo database!\n");
		return NULL;
	}
	return NULL;
	return p;
}

struct terminfo_entry *
get_terminfo_entry(void) {
	char	*name;

	if ((name = get_term_name()) == NULL) {
		return NULL;
	}
	return do_get_terminfo_entry(name);
}

