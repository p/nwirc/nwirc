/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef DEFS_H
#define DEFS_H

#ifndef USERS_H
#include "users.h" 
#endif

#define CLI_VER "nwIRC v0.7.8"

#ifndef MAXSERVERS
#define MAXSERVERS 4 /* arbitrary decision */
#endif

extern int           s_active_serv,
                     r_active_serv,
                     protocol;
extern unsigned long myaddr;

void init_defs(void);

#ifndef STDIO_H
#define STDIO_H
#include <stdio.h> /* for FILE */
#endif

#ifndef PTHREAD_H
#define PTHREAD_H
#include <pthread.h>
#endif

struct chan {
	char             *name;
	FILE             *fd;
	int		  logday;
	int               am_operator;
	struct chan_user *users;
};

struct server {
	pthread_mutex_t	mutex;
	char			*nickname;
	char			name[256];
	char			handle[16];
	char			*autojoin;
	char			*quitmsg;
	char			*address;
	int				sock;
	int				protocol;
	int				port;
	int				logging;
	int				splitlogs;
	int				autodcc;
	int				dcctimeout;
	int				invisible;
	int				serving;
	int				using_config;
	int				using_filter;
	int				allchans;
	int				active_chan;
	struct chan *channels;
} /* servers[MAXSERVERS] */;

struct client_ {
	char                *user,
	                    *password,
	                     prompt[64],
	                     focus[64],
	                     cwd[1024],
	                     operating_system[64];
	int                  s_active_serv,
	                     r_active_serv,
	                     allservers,
	                     running_lcs;
	int                  noterm;
	unsigned long        myaddr;
	unsigned char       *myaddr6;
};

extern struct server servers[MAXSERVERS];
extern struct server *sbuf; 
extern struct server *rbuf;
extern struct client_ client;

#endif
