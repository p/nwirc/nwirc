#ifndef CONFCOL_H
#define CONFCOL_H

#include <stdio.h>


struct setting {
	const char	*informal;
	const char	*name;
	int			value;
	int			changed;
};

struct colors {
	struct setting		*msg_text;
	struct setting		*msg_sender_other;
	struct setting		*msg_sender_me;
	struct setting		*channel;
	struct setting		*privmsg_text;
	struct setting		*privmsg_sender_other;
	struct setting		*privmsg_sender_me;
	struct setting		*time;
	struct setting		*notice_text;
	struct setting		*notice_sender;
	struct setting		*ctcp;
	struct setting		*join;
	struct setting		*nickchange;
	struct setting		*part;
	struct setting		*quit;
	struct setting		*background;
};

extern struct colors	colors;
	
/*
 * Load existent custom color settings, if any
 */
void	load_color_settings(FILE *fd);

#endif /* #ifndef CONFCOL_H */
