#ifndef MODULE_H
#define MODULE_H

struct mod_context;

typedef	int	(*modfunc_t)(struct mod_context *m);
typedef void (*generic_fptr_t)(void);

struct module {
	const char		*name;
	const char		*path;
	void			*handle;
	struct module	*next;
};


struct mod_context {
	struct server	*then_sbuf;
	struct server	*then_rbuf;
	int				msgtype;
	char			**msg;
	char			**fmtptr;
	generic_fptr_t	*api_table;
};


void	load_mod(const char *name);

#endif
