/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "termcap.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include "keydefs.h"
#include "nwirc_libc.h"

/*
 * This file used to be used as a ``replacement'' for the functionality
 * offered by the widespread and standard curses library, written for
 * fun and unusable for systems without plaintext termcap file (such as
 * IRIX, HP-UX and Darwin) anyway. Now, it is used for custom termcap
 * entries. If the user wishes to use a custom termcap file, this can
 * be achieved by placing it (under the name termcap) in the nwirc 
 * directory. The routines offered here will then read and parse it ...
 * In the unlikely event that a system does not have the curses library
 * functions used here (they are marked deprected on HP-UX, for example),
 * we'll try to replace it 
 */
#ifndef NO_CURSES /* don't need system termcap file */
static char *termcap_paths[] = {
	"./termcap",
	NULL
};
#else /* let's attempt to replace curses */
static char *termcap_paths[] = {
	"/etc/termcap", /* BSD, Linux, Solaris */
	"/usr/share/misc/termcap", /* Linux */
	NULL
};
#endif

static struct termcap_entry *
do_get_termcap_entry(const char *name);

/* Pointers to string escape sequences in termmap */
struct termcap_entry **term_esc = NULL;
int					term_esc_len[32];

/* Table to read termcap database into */
struct termcap_entry termmap[] = {
	/* Boolean variables */
#if 0 /* Not needed for my current purposes */
	{ "bw", TY_BOOL, 0, NULL },
	{ "am", TY_BOOL, 0, NULL },
	{ "xb", TY_BOOL, 0, NULL },
	{ "xs", TY_BOOL, 0, NULL },
	{ "xn", TY_BOOL, 0, NULL },
	{ "eo", TY_BOOL, 0, NULL },
	{ "gn", TY_BOOL, 0, NULL },
	{ "hc", TY_BOOL, 0, NULL },
	{ "km", TY_BOOL, 0, NULL },
	{ "hs", TY_BOOL, 0, NULL },
	{ "in", TY_BOOL, 0, NULL },
	{ "da", TY_BOOL, 0, NULL },
	{ "db", TY_BOOL, 0, NULL },
	{ "mi", TY_BOOL, 0, NULL },
	{ "ms", TY_BOOL, 0, NULL },
	{ "os", TY_BOOL, 0, NULL },
	{ "es", TY_BOOL, 0, NULL },
	{ "xt", TY_BOOL, 0, NULL },
	{ "hz", TY_BOOL, 0, NULL },
	{ "ul", TY_BOOL, 0, NULL },
	{ "xo", TY_BOOL, 0, NULL },
	{ "nx", TY_BOOL, 0, NULL },
	{ "5i", TY_BOOL, 0, NULL },
	{ "HC", TY_BOOL, 0, NULL },
	{ "NR", TY_BOOL, 0, NULL },
	{ "NP", TY_BOOL, 0, NULL },
	{ "ND", TY_BOOL, 0, NULL },
	{ "cc", TY_BOOL, 0, NULL },
	{ "ut", TY_BOOL, 0, NULL },
	{ "hl", TY_BOOL, 0, NULL },
	{ "YA", TY_BOOL, 0, NULL },
	{ "YB", TY_BOOL, 0, NULL },
	{ "YC", TY_BOOL, 0, NULL },
	{ "YD", TY_BOOL, 0, NULL },
	{ "YE", TY_BOOL, 0, NULL },
	{ "YF", TY_BOOL, 0, NULL },
	{ "YG", TY_BOOL, 0, NULL },
	{ "NL", TY_BOOL, 0, NULL },
	{ "EP", TY_BOOL, 0, NULL },
	{ "OP", TY_BOOL, 0, NULL },
	{ "HD", TY_BOOL, 0, NULL },
	{ "LC", TY_BOOL, 0, NULL },
	{ "UC", TY_BOOL, 0, NULL },
	{ "pt", TY_BOOL, 0, NULL },
	{ "xr", TY_BOOL, 0, NULL },
	{ "xx", TY_BOOL, 0, NULL },
	{ "bs", TY_BOOL, 0, NULL },
	{ "ns", TY_BOOL, 0, NULL },
	{ "nc", TY_BOOL, 0, NULL },

	/* Numeric variables */
	{ "co", TY_NUM, 0, NULL },
	{ "it", TY_NUM, 0, NULL },
	{ "li", TY_NUM, 0, NULL },
	{ "lm", TY_NUM, 0, NULL },
	{ "sg", TY_NUM, 0, NULL },
	{ "pb", TY_NUM, 0, NULL },
	{ "vt", TY_NUM, 0, NULL },
	{ "ws", TY_NUM, 0, NULL },
	{ "Nl", TY_NUM, 0, NULL },
	{ "lh", TY_NUM, 0, NULL },
	{ "lw", TY_NUM, 0, NULL },
	{ "ma", TY_NUM, 0, NULL },
	{ "MW", TY_NUM, 0, NULL },
	{ "ug", TY_NUM, 0, NULL },
	{ "Co", TY_NUM, 0, NULL },
	{ "pa", TY_NUM, 0, NULL },
	{ "NC", TY_NUM, 0, NULL },
	{ "Ya", TY_NUM, 0, NULL },
	{ "Yb", TY_NUM, 0, NULL },
	{ "Yc", TY_NUM, 0, NULL },
	{ "Yd", TY_NUM, 0, NULL },
	{ "Ye", TY_NUM, 0, NULL },
	{ "Yf", TY_NUM, 0, NULL },
	{ "Yg", TY_NUM, 0, NULL },
	{ "Yh", TY_NUM, 0, NULL },
	{ "Yi", TY_NUM, 0, NULL },
	{ "Yj", TY_NUM, 0, NULL },
	{ "Yk", TY_NUM, 0, NULL },
	{ "Yl", TY_NUM, 0, NULL },
	{ "Ym", TY_NUM, 0, NULL },
	{ "Yn", TY_NUM, 0, NULL },
	{ "BT", TY_NUM, 0, NULL },
	{ "Yo", TY_NUM, 0, NULL },
	{ "Yp", TY_NUM, 0, NULL },
	{ "dB", TY_NUM, 0, NULL },
	{ "dF", TY_NUM, 0, NULL },
	{ "dT", TY_NUM, 0, NULL },
	{ "dV", TY_NUM, 0, NULL },
	{ "kn", TY_NUM, 0, NULL },
	{ "dC", TY_NUM, 0, NULL },
	{ "dN", TY_NUM, 0, NULL },
#endif

	/* String variables */
	{ "bt", TY_STRING, 0, NULL, 0 },
	{ "bl", TY_STRING, 0, NULL, 0 },
	{ "cr", TY_STRING, 0, NULL, 0 },
	{ "cs", TY_STRING, 0, NULL, 0 },
	{ "ct", TY_STRING, 0, NULL, 0 },
	{ "cl", TY_STRING, 0, NULL, 0 },
	{ "ce", TY_STRING, 0, NULL, 0 },
	{ "cd", TY_STRING, 0, NULL, 0 },
	{ "ch", TY_STRING, 0, NULL, 0 },
	{ "CC", TY_STRING, 0, NULL, 0 },
	{ "cm", TY_STRING, 0, NULL, 0 },
	{ "do", TY_STRING, 0, NULL, 0 },
	{ "ho", TY_STRING, 0, NULL, 0 },
	{ "vi", TY_STRING, 0, NULL, 0 },
	{ "le", TY_STRING, 0, NULL, 0 },
	{ "CM", TY_STRING, 0, NULL, 0 },
	{ "ve", TY_STRING, 0, NULL, 0 },
	{ "nd", TY_STRING, 0, NULL, 0 },
	{ "ll", TY_STRING, 0, NULL, 0 },
	{ "up", TY_STRING, 0, NULL, 0 },
	{ "vs", TY_STRING, 0, NULL, 0 },
	{ "dc", TY_STRING, 0, NULL, 0 },
	{ "dl", TY_STRING, 0, NULL, 0 },
	{ "ds", TY_STRING, 0, NULL, 0 },
	{ "hd", TY_STRING, 0, NULL, 0 },
	{ "as", TY_STRING, 0, NULL, 0 },
	{ "mb", TY_STRING, 0, NULL, 0 },
	{ "md", TY_STRING, 0, NULL, 0 },
	{ "ti", TY_STRING, 0, NULL, 0 },
	{ "dm", TY_STRING, 0, NULL, 0 },
	{ "mh", TY_STRING, 0, NULL, 0 },
	{ "im", TY_STRING, 0, NULL, 0 },
	{ "mk", TY_STRING, 0, NULL, 0 },
	{ "mp", TY_STRING, 0, NULL, 0 },
	{ "mr", TY_STRING, 0, NULL, 0 },
	{ "so", TY_STRING, 0, NULL, 0 },
	{ "us", TY_STRING, 0, NULL, 0 },
	{ "ec", TY_STRING, 0, NULL, 0 },
	{ "ae", TY_STRING, 0, NULL, 0 },
	{ "me", TY_STRING, 0, NULL, 0 },
	{ "te", TY_STRING, 0, NULL, 0 },
	{ "ed", TY_STRING, 0, NULL, 0 },
	{ "ei", TY_STRING, 0, NULL, 0 },
	{ "se", TY_STRING, 0, NULL, 0 },
	{ "ue", TY_STRING, 0, NULL, 0 },
	{ "vb", TY_STRING, 0, NULL, 0 },
	{ "ff", TY_STRING, 0, NULL, 0 },
	{ "fs", TY_STRING, 0, NULL, 0 },
	{ "i1", TY_STRING, 0, NULL, 0 },
	{ "is", TY_STRING, 0, NULL, 0 },
	{ "i3", TY_STRING, 0, NULL, 0 },
	{ "if", TY_STRING, 0, NULL, 0 },
	{ "ic", TY_STRING, 0, NULL, 0 },
	{ "al", TY_STRING, 0, NULL, 0 },
	{ "ip", TY_STRING, 0, NULL, 0 },
	{ "kb", TY_STRING, 0, NULL, 0 },
	{ "ka", TY_STRING, 0, NULL, 0 },
	{ "kC", TY_STRING, 0, NULL, 0 },
	{ "kt", TY_STRING, 0, NULL, 0 },
	{ "kD", TY_STRING, 0, NULL, 0 },
	{ "kL", TY_STRING, 0, NULL, 0 },
	{ "kd", TY_STRING, 0, NULL, KEY_DOWN },
	{ "kM", TY_STRING, 0, NULL, 0 },
	{ "kE", TY_STRING, 0, NULL, 0 },
	{ "kS", TY_STRING, 0, NULL, 0 },
	{ "k0", TY_STRING, 0, NULL, 0 },
	{ "k1", TY_STRING, 0, NULL, KEY_F1 },
	{ "k;", TY_STRING, 0, NULL, KEY_F10 },
	{ "k2", TY_STRING, 0, NULL, KEY_F2 },
	{ "k3", TY_STRING, 0, NULL, KEY_F3 },
	{ "k4", TY_STRING, 0, NULL, KEY_F4 },
	{ "k5", TY_STRING, 0, NULL, KEY_F5 },
	{ "k6", TY_STRING, 0, NULL, KEY_F6 },
	{ "k7", TY_STRING, 0, NULL, KEY_F7 },
	{ "k8", TY_STRING, 0, NULL, KEY_F8 },
	{ "k9", TY_STRING, 0, NULL, KEY_F9 },
	{ "kh", TY_STRING, 0, NULL, KEY_HOME },
	{ "kI", TY_STRING, 0, NULL, 0 },
	{ "kA", TY_STRING, 0, NULL, 0 },
	{ "kl", TY_STRING, 0, NULL, KEY_LEFT },
	{ "kH", TY_STRING, 0, NULL, 0 },
	{ "kN", TY_STRING, 0, NULL, 0 },
	{ "kP", TY_STRING, 0, NULL, 0 },
	{ "kr", TY_STRING, 0, NULL, KEY_RIGHT },
	{ "kF", TY_STRING, 0, NULL, 0 },
	{ "kR", TY_STRING, 0, NULL, 0 },
	{ "kT", TY_STRING, 0, NULL, 0 },
	{ "ku", TY_STRING, 0, NULL, KEY_UP },
	{ "ke", TY_STRING, 0, NULL, 0 },
	{ "ks", TY_STRING, 0, NULL, 0 },
	{ "l0", TY_STRING, 0, NULL, 0 },
	{ "l1", TY_STRING, 0, NULL, 0 },
	{ "la", TY_STRING, 0, NULL, 0 },
	{ "l2", TY_STRING, 0, NULL, 0 },
	{ "l3", TY_STRING, 0, NULL, 0 },
	{ "l4", TY_STRING, 0, NULL, 0 },
	{ "l5", TY_STRING, 0, NULL, 0 },
	{ "l6", TY_STRING, 0, NULL, 0 },
	{ "l7", TY_STRING, 0, NULL, 0 },
	{ "l8", TY_STRING, 0, NULL, 0 },
	{ "l9", TY_STRING, 0, NULL, 0 },
	{ "mo", TY_STRING, 0, NULL, 0 },
	{ "mm", TY_STRING, 0, NULL, 0 },
	{ "nw", TY_STRING, 0, NULL, 0 },
	{ "pc", TY_STRING, 0, NULL, 0 },
	{ "DC", TY_STRING, 0, NULL, 0 },
	{ "DL", TY_STRING, 0, NULL, 0 },
	{ "DO", TY_STRING, 0, NULL, 0 },
	{ "IC", TY_STRING, 0, NULL, 0 },
	{ "SF", TY_STRING, 0, NULL, 0 },
	{ "AL", TY_STRING, 0, NULL, 0 },
	{ "LE", TY_STRING, 0, NULL, 0 },
	{ "RI", TY_STRING, 0, NULL, 0 },
	{ "SR", TY_STRING, 0, NULL, 0 },
	{ "UP", TY_STRING, 0, NULL, 0 },
	{ "pk", TY_STRING, 0, NULL, 0 },
	{ "pl", TY_STRING, 0, NULL, 0 },
	{ "px", TY_STRING, 0, NULL, 0 },
	{ "ps", TY_STRING, 0, NULL, 0 },
	{ "pf", TY_STRING, 0, NULL, 0 },
	{ "po", TY_STRING, 0, NULL, 0 },
	{ "rp", TY_STRING, 0, NULL, 0 },
	{ "r1", TY_STRING, 0, NULL, 0 },
	{ "r2", TY_STRING, 0, NULL, 0 },
	{ "r3", TY_STRING, 0, NULL, 0 },
	{ "rf", TY_STRING, 0, NULL, 0 },
	{ "rc", TY_STRING, 0, NULL, 0 },
	{ "cv", TY_STRING, 0, NULL, 0 },
	{ "sc", TY_STRING, 0, NULL, 0 },
	{ "sf", TY_STRING, 0, NULL, 0 },
	{ "sr", TY_STRING, 0, NULL, 0 },
	{ "sa", TY_STRING, 0, NULL, 0 },
	{ "st", TY_STRING, 0, NULL, 0 },
	{ "wi", TY_STRING, 0, NULL, 0 },
	{ "ta", TY_STRING, 0, NULL, 0 },
	{ "ts", TY_STRING, 0, NULL, 0 },
	{ "uc", TY_STRING, 0, NULL, 0 },
	{ "hu", TY_STRING, 0, NULL, 0 },
	{ "iP", TY_STRING, 0, NULL, 0 },
	{ "K1", TY_STRING, 0, NULL, 0 },
	{ "K2", TY_STRING, 0, NULL, 0 },
	{ "K3", TY_STRING, 0, NULL, 0 },
	{ "K4", TY_STRING, 0, NULL, 0 },
	{ "K5", TY_STRING, 0, NULL, 0 },
	{ "pO", TY_STRING, 0, NULL, 0 },
	{ "i2", TY_STRING, 0, NULL, 0 },
	{ "rs", TY_STRING, 0, NULL, 0 },
	{ "rP", TY_STRING, 0, NULL, 0 },
	{ "ac", TY_STRING, 0, NULL, 0 },
	{ "pn", TY_STRING, 0, NULL, 0 },
	{ "kB", TY_STRING, 0, NULL, 0 },
	{ "SX", TY_STRING, 0, NULL, 0 },
	{ "RX", TY_STRING, 0, NULL, 0 },
	{ "SA", TY_STRING, 0, NULL, 0 },
	{ "RA", TY_STRING, 0, NULL, 0 },
	{ "XN", TY_STRING, 0, NULL, 0 },
	{ "XF", TY_STRING, 0, NULL, 0 },
	{ "eA", TY_STRING, 0, NULL, 0 },
	{ "LO", TY_STRING, 0, NULL, 0 },
	{ "LF", TY_STRING, 0, NULL, 0 },
	{ "@1", TY_STRING, 0, NULL, 0 },
	{ "@2", TY_STRING, 0, NULL, 0 },
	{ "@3", TY_STRING, 0, NULL, 0 },
	{ "@4", TY_STRING, 0, NULL, 0 },
	{ "@5", TY_STRING, 0, NULL, 0 },
	{ "@6", TY_STRING, 0, NULL, 0 },
	{ "@7", TY_STRING, 0, NULL, KEY_EOL },
	{ "@8", TY_STRING, 0, NULL, 0 },
	{ "@9", TY_STRING, 0, NULL, 0 },
	{ "@0", TY_STRING, 0, NULL, 0 },
	{ "%1", TY_STRING, 0, NULL, 0 },
	{ "%2", TY_STRING, 0, NULL, 0 },
	{ "%3", TY_STRING, 0, NULL, 0 },
	{ "%4", TY_STRING, 0, NULL, 0 },
	{ "%5", TY_STRING, 0, NULL, 0 },
	{ "%6", TY_STRING, 0, NULL, 0 },
	{ "%7", TY_STRING, 0, NULL, 0 },
	{ "%8", TY_STRING, 0, NULL, 0 },
	{ "%9", TY_STRING, 0, NULL, 0 },
	{ "%0", TY_STRING, 0, NULL, 0 },
	{ "&1", TY_STRING, 0, NULL, 0 },
	{ "&2", TY_STRING, 0, NULL, 0 },
	{ "&3", TY_STRING, 0, NULL, 0 },
	{ "&4", TY_STRING, 0, NULL, 0 },
	{ "&5", TY_STRING, 0, NULL, 0 },
	{ "&6", TY_STRING, 0, NULL, 0 },
	{ "&7", TY_STRING, 0, NULL, 0 },
	{ "&8", TY_STRING, 0, NULL, 0 },
	{ "&9", TY_STRING, 0, NULL, 0 },
	{ "&0", TY_STRING, 0, NULL, 0 },
	{ "*1", TY_STRING, 0, NULL, 0 },
	{ "*2", TY_STRING, 0, NULL, 0 },
	{ "*3", TY_STRING, 0, NULL, 0 },
	{ "*4", TY_STRING, 0, NULL, 0 },
	{ "*5", TY_STRING, 0, NULL, 0 },
	{ "*6", TY_STRING, 0, NULL, 0 },
	{ "*7", TY_STRING, 0, NULL, 0 },
	{ "*8", TY_STRING, 0, NULL, 0 },
	{ "*9", TY_STRING, 0, NULL, 0 },
	{ "*0", TY_STRING, 0, NULL, 0 },
	{ "#1", TY_STRING, 0, NULL, 0 },
	{ "#2", TY_STRING, 0, NULL, 0 },
	{ "#3", TY_STRING, 0, NULL, 0 },
	{ "#4", TY_STRING, 0, NULL, 0 },
	{ "%a", TY_STRING, 0, NULL, 0 },
	{ "%b", TY_STRING, 0, NULL, 0 },
	{ "%c", TY_STRING, 0, NULL, 0 },
	{ "%d", TY_STRING, 0, NULL, 0 },
	{ "%e", TY_STRING, 0, NULL, 0 },
	{ "%f", TY_STRING, 0, NULL, 0 },
	{ "%g", TY_STRING, 0, NULL, 0 },
	{ "%h", TY_STRING, 0, NULL, 0 },
	{ "%i", TY_STRING, 0, NULL, 0 },
	{ "%j", TY_STRING, 0, NULL, 0 },
	{ "!1", TY_STRING, 0, NULL, 0 },
	{ "!2", TY_STRING, 0, NULL, 0 },
	{ "!3", TY_STRING, 0, NULL, 0 },
	{ "RF", TY_STRING, 0, NULL, 0 },
	{ "F1", TY_STRING, 0, NULL, KEY_F11 },
	{ "F2", TY_STRING, 0, NULL, KEY_F12 },
	{ "F3", TY_STRING, 0, NULL, 0 },
	{ "F4", TY_STRING, 0, NULL, 0 },
	{ "F5", TY_STRING, 0, NULL, 0 },
	{ "F6", TY_STRING, 0, NULL, 0 },
	{ "F7", TY_STRING, 0, NULL, 0 },
	{ "F8", TY_STRING, 0, NULL, 0 },
	{ "F9", TY_STRING, 0, NULL, 0 },
	{ "FA", TY_STRING, 0, NULL, 0 },
	{ "FB", TY_STRING, 0, NULL, 0 },
	{ "FC", TY_STRING, 0, NULL, 0 },
	{ "FD", TY_STRING, 0, NULL, 0 },
	{ "FE", TY_STRING, 0, NULL, 0 },
	{ "FG", TY_STRING, 0, NULL, 0 },
	{ "FH", TY_STRING, 0, NULL, 0 },
	{ "FI", TY_STRING, 0, NULL, 0 },
	{ "FJ", TY_STRING, 0, NULL, 0 },
	{ "FK", TY_STRING, 0, NULL, 0 },
	{ "FL", TY_STRING, 0, NULL, 0 },
	{ "FM", TY_STRING, 0, NULL, 0 },
	{ "FN", TY_STRING, 0, NULL, 0 },
	{ "FO", TY_STRING, 0, NULL, 0 },
	{ "FP", TY_STRING, 0, NULL, 0 },
	{ "FQ", TY_STRING, 0, NULL, 0 },
	{ "FR", TY_STRING, 0, NULL, 0 },
	{ "FS", TY_STRING, 0, NULL, 0 },
	{ "FT", TY_STRING, 0, NULL, 0 },
	{ "FU", TY_STRING, 0, NULL, 0 },
	{ "FV", TY_STRING, 0, NULL, 0 },
	{ "FW", TY_STRING, 0, NULL, 0 },
	{ "FX", TY_STRING, 0, NULL, 0 },
	{ "FY", TY_STRING, 0, NULL, 0 },
	{ "FZ", TY_STRING, 0, NULL, 0 },
	{ "Fa", TY_STRING, 0, NULL, 0 },
	{ "Fb", TY_STRING, 0, NULL, 0 },
	{ "Fc", TY_STRING, 0, NULL, 0 },
	{ "Fd", TY_STRING, 0, NULL, 0 },
	{ "Fe", TY_STRING, 0, NULL, 0 },
	{ "Ff", TY_STRING, 0, NULL, 0 },
	{ "Fg", TY_STRING, 0, NULL, 0 },
	{ "Fh", TY_STRING, 0, NULL, 0 },
	{ "Fi", TY_STRING, 0, NULL, 0 },
	{ "Fj", TY_STRING, 0, NULL, 0 },
	{ "Fk", TY_STRING, 0, NULL, 0 },
	{ "Fl", TY_STRING, 0, NULL, 0 },
	{ "Fm", TY_STRING, 0, NULL, 0 },
	{ "Fn", TY_STRING, 0, NULL, 0 },
	{ "Fo", TY_STRING, 0, NULL, 0 },
	{ "Fp", TY_STRING, 0, NULL, 0 },
	{ "Fq", TY_STRING, 0, NULL, 0 },
	{ "Fr", TY_STRING, 0, NULL, 0 },
	{ "cb", TY_STRING, 0, NULL, 0 },
	{ "MC", TY_STRING, 0, NULL, 0 },
	{ "ML", TY_STRING, 0, NULL, 0 },
	{ "MR", TY_STRING, 0, NULL, 0 },
	{ "Lf", TY_STRING, 0, NULL, 0 },
	{ "SC", TY_STRING, 0, NULL, 0 },
	{ "DK", TY_STRING, 0, NULL, 0 },
	{ "RC", TY_STRING, 0, NULL, 0 },
	{ "CW", TY_STRING, 0, NULL, 0 },
	{ "WG", TY_STRING, 0, NULL, 0 },
	{ "HU", TY_STRING, 0, NULL, 0 },
	{ "DI", TY_STRING, 0, NULL, 0 },
	{ "QD", TY_STRING, 0, NULL, 0 },
	{ "TO", TY_STRING, 0, NULL, 0 },
	{ "PU", TY_STRING, 0, NULL, 0 },
	{ "fh", TY_STRING, 0, NULL, 0 },
	{ "PA", TY_STRING, 0, NULL, 0 },
	{ "WA", TY_STRING, 0, NULL, 0 },
	{ "u0", TY_STRING, 0, NULL, 0 },
	{ "u1", TY_STRING, 0, NULL, 0 },
	{ "u2", TY_STRING, 0, NULL, 0 },
	{ "u3", TY_STRING, 0, NULL, 0 },
	{ "u4", TY_STRING, 0, NULL, 0 },
	{ "u5", TY_STRING, 0, NULL, 0 },
	{ "u6", TY_STRING, 0, NULL, 0 },
	{ "u7", TY_STRING, 0, NULL, 0 },
	{ "u8", TY_STRING, 0, NULL, 0 },
	{ "u9", TY_STRING, 0, NULL, 0 },
	{ "op", TY_STRING, 0, NULL, 0 },
	{ "oc", TY_STRING, 0, NULL, 0 },
	{ "Ic", TY_STRING, 0, NULL, 0 },
	{ "Ip", TY_STRING, 0, NULL, 0 },
	{ "sp", TY_STRING, 0, NULL, 0 },
	{ "Sf", TY_STRING, 0, NULL, 0 },
	{ "Sb", TY_STRING, 0, NULL, 0 },
	{ "ZA", TY_STRING, 0, NULL, 0 },
	{ "ZB", TY_STRING, 0, NULL, 0 },
	{ "ZC", TY_STRING, 0, NULL, 0 },
	{ "ZD", TY_STRING, 0, NULL, 0 },
	{ "ZE", TY_STRING, 0, NULL, 0 },
	{ "ZF", TY_STRING, 0, NULL, 0 },
	{ "ZG", TY_STRING, 0, NULL, 0 },
	{ "ZH", TY_STRING, 0, NULL, 0 },
	{ "ZI", TY_STRING, 0, NULL, 0 },
	{ "ZJ", TY_STRING, 0, NULL, 0 },
	{ "ZK", TY_STRING, 0, NULL, 0 },
	{ "ZL", TY_STRING, 0, NULL, 0 },
	{ "ZM", TY_STRING, 0, NULL, 0 },
	{ "ZN", TY_STRING, 0, NULL, 0 },
	{ "ZO", TY_STRING, 0, NULL, 0 },
	{ "ZP", TY_STRING, 0, NULL, 0 },
	{ "ZQ", TY_STRING, 0, NULL, 0 },
	{ "ZR", TY_STRING, 0, NULL, 0 },
	{ "ZS", TY_STRING, 0, NULL, 0 },
	{ "ZT", TY_STRING, 0, NULL, 0 },
	{ "ZU", TY_STRING, 0, NULL, 0 },
	{ "ZV", TY_STRING, 0, NULL, 0 },
	{ "ZW", TY_STRING, 0, NULL, 0 },
	{ "ZX", TY_STRING, 0, NULL, 0 },
	{ "ZY", TY_STRING, 0, NULL, 0 },
	{ "ZZ", TY_STRING, 0, NULL, 0 },
	{ "Za", TY_STRING, 0, NULL, 0 },
	{ "Zb", TY_STRING, 0, NULL, 0 },
	{ "Zc", TY_STRING, 0, NULL, 0 },
	{ "Zd", TY_STRING, 0, NULL, 0 },
	{ "Ze", TY_STRING, 0, NULL, 0 },
	{ "Zf", TY_STRING, 0, NULL, 0 },
	{ "Zg", TY_STRING, 0, NULL, 0 },
	{ "Zh", TY_STRING, 0, NULL, 0 },
	{ "Zj", TY_STRING, 0, NULL, 0 },
	{ "Zk", TY_STRING, 0, NULL, 0 },
	{ "Zl", TY_STRING, 0, NULL, 0 },
	{ "Zm", TY_STRING, 0, NULL, 0 },
	{ "Zn", TY_STRING, 0, NULL, 0 },
	{ "Zo", TY_STRING, 0, NULL, 0 },
	{ "Zp", TY_STRING, 0, NULL, 0 },
	{ "Zq", TY_STRING, 0, NULL, 0 },
	{ "Zr", TY_STRING, 0, NULL, 0 },
	{ "Zs", TY_STRING, 0, NULL, 0 },
	{ "Zt", TY_STRING, 0, NULL, 0 },
	{ "Zu", TY_STRING, 0, NULL, 0 },
	{ "Zv", TY_STRING, 0, NULL, 0 },
	{ "Zw", TY_STRING, 0, NULL, 0 },
	{ "Zx", TY_STRING, 0, NULL, 0 },
	{ "Zy", TY_STRING, 0, NULL, 0 },
	{ "Km", TY_STRING, 0, NULL, 0 },
	{ "Mi", TY_STRING, 0, NULL, 0 },
	{ "RQ", TY_STRING, 0, NULL, 0 },
	{ "Gm", TY_STRING, 0, NULL, 0 },
	{ "AF", TY_STRING, 0, NULL, 0 },
	{ "AB", TY_STRING, 0, NULL, 0 },
	{ "xl", TY_STRING, 0, NULL, 0 },
	{ "dv", TY_STRING, 0, NULL, 0 },
	{ "ci", TY_STRING, 0, NULL, 0 },
	{ "s0", TY_STRING, 0, NULL, 0 },
	{ "s1", TY_STRING, 0, NULL, 0 },
	{ "s2", TY_STRING, 0, NULL, 0 },
	{ "s3", TY_STRING, 0, NULL, 0 },
	{ "ML", TY_STRING, 0, NULL, 0 },
	{ "MT", TY_STRING, 0, NULL, 0 },
	{ "Xy", TY_STRING, 0, NULL, 0 },
	{ "Zz", TY_STRING, 0, NULL, 0 },
	{ "Yw", TY_STRING, 0, NULL, 0 },
	{ "Yx", TY_STRING, 0, NULL, 0 },
	{ "Yy", TY_STRING, 0, NULL, 0 },
	{ "Yz", TY_STRING, 0, NULL, 0 },
	{ "YZ", TY_STRING, 0, NULL, 0 },
	{ "S1", TY_STRING, 0, NULL, 0 },
	{ "S2", TY_STRING, 0, NULL, 0 },
	{ "S3", TY_STRING, 0, NULL, 0 },
	{ "S4", TY_STRING, 0, NULL, 0 },
	{ "S5", TY_STRING, 0, NULL, 0 },
	{ "S6", TY_STRING, 0, NULL, 0 },
	{ "S7", TY_STRING, 0, NULL, 0 },
	{ "S8", TY_STRING, 0, NULL, 0 },
	{ "Xh", TY_STRING, 0, NULL, 0 },
	{ "Xl", TY_STRING, 0, NULL, 0 },
	{ "Xo", TY_STRING, 0, NULL, 0 },
	{ "Xr", TY_STRING, 0, NULL, 0 },
	{ "Xt", TY_STRING, 0, NULL, 0 },
	{ "Xv", TY_STRING, 0, NULL, 0 },
	{ "ko", TY_STRING, 0, NULL, 0 },
	{ "ma", TY_STRING, 0, NULL, 0 },
	{ "ml", TY_STRING, 0, NULL, 0 },
	{ "mu", TY_STRING, 0, NULL, 0 },
	{ "nl", TY_STRING, 0, NULL, 0 },
	{ "bc", TY_STRING, 0, NULL, 0 },
	{ NULL, 0, 0, NULL, 0 } 
};

/*
 * Returns the terminal map entry corresponding to ``name'' on success,
 * else a null pointer
 */
static struct termcap_entry *
lookup_entry(const char *name) {
	int		i;

	for (i = 0; termmap[i].name != NULL; ++i) {
		if (strcmp(termmap[i].name, name) == 0) {
			return &termmap[i];
		}
	}
	return NULL;
}

#define DOGET_EXIT(fd, file, name) do { \
	(void) fprintf(stderr, "Couldn't find %s in %s\n", name, file); \
	fclose(fd); \
	return NULL; \
} while (0)


/*
 * Reads the numeric option argument given to *p on success and stores
 * it in the terminal map entry pointed to by ``entry''. ``word'' is
 * the option name passed for printing in error messages. *p must
 * initially point to the ``#'' in ``opt#<num>'' and and is updated to
 * point after that on success
 */
static int
read_numeric(	char **p,
				const char *word,
				struct termcap_entry *entry) {
	char	numbuf[64];
	char	*err;

	if ((*p)[3] != '#') {
		fprintf(stderr,
			"Illegal character after numeric option %s: %c\n",
			word, (*p)[3]);
		return 1;
	}
	if (sscanf(&(*p)[4], "%63[^:]", numbuf) != 1) {
		fprintf(stderr,
			"Bad argument to numeric option %s\n", word);
		return 1;
	}
	if (entry->data == NULL) {
		entry->data = n_xmalloc(sizeof(int));
		*(int *)entry->data = (int)strtol(numbuf, &err, 10);
		if (*err) {
			fprintf(stderr,
				"Non-numeric option argument to %s\n",
				word);
			free(entry->data);
			return 1;
		}
	}
	*p += 4 + strlen(numbuf);
	return 0;
}

/*
 * Reads the \-escape-sequence pointed to by *p (AFTER the beginning /)
 * and stores the value in *dest. ``word'' is passed for printing in
 * error messages. Returns 0 on success, 1 on failure. On success, *p
 * is updated to point to the LAST character of the escape sequence
 */
static int
get_escape(char *dest, char **p, const char *word) {
	int		err = 0;
	char	input = **p;

	(void)word;

	switch (input) {
	case 0: /* Premature end of entry */
		++err;
		break;
	case 'E': /* Escape */
		*dest = 033;
		break;
	case 'n': /* Newline */
		*dest = '\n';
		break;
	case 'r': /* Return */
		*dest = '\r';
		break;
	case 't': /* Tabulator */
		*dest = '\t';
		break;
	case 'b': /* Backspace */
		*dest = '\t';
	case 'f': /* Formfeed */
		*dest = '\f';
		break;
	case '\\':
		*dest = '\\';
		break;
	case '^':
		*dest = '^';
		break;
	case ':':
		*dest = ':';
		break;
	default:
		/* Must be octal code! */
		if (!isdigit((unsigned char)input)) {
			++err;
			break;
		} else {
			unsigned	num;
			/*++(*p);*/
			if (sscanf(*p, "%o", &num) != 1) {
				++err;
				break;
			}
			*dest = (char)num;
			while (num /= 10) {
				++(*p);
			}
		}
	}
	return err;
}

/*
 * Reads the \-escape-sequence pointed to by *p (AFTER the beginning /)
 * and stores the value in *dest. ``word'' is passed for printing in
 * error messages.
 */
static int
get_escape2(char *dest, char input, const char *word) {
	(void)dest; (void)input; (void)word;
	/* XXX implement if needed :) */
	if (input == '?') {
		*dest = 0177;
	} else {
		*dest &= 037;
	}
	return 0;
}

/*
 * Reads the string option argument given to *p on success and stores
 * it in the terminal map entry pointed to by ``entry''. ``word'' is
 * the option name passed for printing in error messages. *p must
 * initially point to the ``='' in ``opt#<num>'' and and is updated to
 * point after that on success
 */
static int
read_string(	char **p,
				const char *word,
				struct termcap_entry *entry) {
	size_t	i;
	char	buf[128];

	if ((*p)[3] != '=') {
		return 1;
	}
	*p += 4;

	for (i = 0; i < sizeof buf - 1; ++i) {
		if (**p == 0) {
			; /* incomplete */
		} else if (**p == ':') {
			break;
		} else {
			if (**p == '\\') {
				/* Escape sequence */
				++(*p);
				if (get_escape(&buf[i], p, word) != 0) {
					return 1;
				}
			} else if (**p == '^') {
				/* Escape sequence */
				++(*p);
				if (get_escape2(&buf[i], **p, word) != 0) {
					return 1;
				}
			} else {
				/* Ordinary character */
				buf[i] = **p;
			}
		}
		++(*p);
	}
	buf[i] = 0;
	if (entry->data == NULL) {
		entry->data = n_xstrdup(buf);
	}
	return 0;
}

static int
extract_option_name(char *p, char *word, int *disable_opt) {
	word[2] = 0;
	*disable_opt = 0;
	if ((word[0] = p[1]) == 0
		|| (word[1] = p[2]) == 0) {
		/* Not even complete option name - must be 2 chars! */
		return 1;
	} else if (isalnum((unsigned char)p[3])) {
		/* Option too long - more than two chars */
		word[2] = 0;
		return 1;
	} else if (p[3] == 0) {
		/* All lines must end in ``\'' or ``:''! */
		word[2] = 0;
		return 1;
	} else if (p[3] == '@') {
		/*
		 * This option is disabled and excluded from further
		 * processing. Must be format op@:
		 */
		word[2] = 0;
		if (p[4] != ':') {
			return 1;
		}
		*disable_opt = 1;
	}
	return 0;
}


static int
read_continued(const char *p) {
	/* Continued by other terminal entry! */
	char	cont[256];
	char	*end;
	size_t	n;

	if (p[3] != '='
		|| (end = strchr(&p[3], ':')) == NULL
		|| end[1] != 0) {
			return 1;
	}
	if ((n = end - &p[4]) > sizeof cont) {
		return 1;
	}
	cont[0] = 0;
	strncat(cont, &p[4], n);
	if (do_get_termcap_entry(cont) == NULL) {
		return 1;
	}
	return 0;
}


#define NEXT_OPTION(p) \
	if ((p = strchr(++p, ':')) != NULL) {	\
		/* Continue processing line */	\
		continue;			\
	} else {				\
		/* Read new line */		\
		break;				\
	}	
		

static struct termcap_entry *
parse_entry(FILE *fd) {
	struct termcap_entry	*entry;
	int						disable_opt;
	char					buf[1024];
	char					word[3];
	char					*p;
	static char				dummy; /* For data field in booleans */

	while (fgets(buf, sizeof buf, fd) != NULL) {
		if (buf[0] == '#') {
			/* Comment - skip */
		}
		(void) strtok(buf, "\n");
		if ((p = strchr(buf, ':')) == NULL) {
			fprintf(stderr, "Malformed termcap entry\n");
			return NULL;
		}

		/*
		 * Process all options in line. The format is roughly:
		 *    :opt:opt:opt:\
		 *    :opt:opt:opt:
		 * I.e., ``:'' is used as separator and ``\'' indicates that
		 * the entry is continued on the next line
		 */
		while (p[1] != '\\') {
			if (p[1] == 0) {
				/* No continuing ``\'' - Finished! */
				return termmap;
			}
			if (extract_option_name(p, word, &disable_opt) != 0) {
				/* Ignore */
				NEXT_OPTION(p);
			}

			if ((entry = lookup_entry(word)) == NULL) {
				if (strcmp(word, "tc") == 0) {
					/* Continued by other terminal entry! */
					if (read_continued(p) != 0) {
						return NULL;
					} else {
						return termmap;
					}
				} else {
					NEXT_OPTION(p);
				}
			}

			if (disable_opt) {
				entry->unused = 1;
			} else if (entry->unused) {
				; /* Option is disabled */
			} else if (entry->type == TY_BOOL) {
				/* Sole occurance suffices to assume option = true! */
				p += 3;
				entry->data = &dummy;
			} else if (entry->type == TY_NUM) {
				/* Format is na#<number> */
				if (read_numeric(&p, word, entry) != 0) {
					return NULL;
				}
			} else { /* TY_STRING */
				/* Format is st=<text> */
				if (read_string(&p, word, entry) != 0) {
					return NULL;
				}
			}
			if (*p != ':') {
				return NULL;
			}
		}
	}
	return	termmap;
}

static struct termcap_entry *
do_get_entry(const char *file, const char *name) {
	struct termcap_entry	*ret;
	FILE					*fd;
	char					*p;
	char					*p2;
	char					buf[1024];

	if ((fd = fopen(file, "r")) == NULL) {
		if (errno != ENOENT) {
			/* File exists but couldn't be opened */
			perror(file);
		}
		return NULL;
	}

	while (fgets(buf, sizeof buf, fd) != NULL) {
		/*
		 * The first line of an entry begins with its name. All
		 * subsequent lines begin with a series of whitespaces followed
		 * by``:''
		 */
		while (!isalnum((unsigned char)buf[0])) {
			/* Skip comments */
			if (fgets(buf, sizeof buf, fd) == NULL) {
				DOGET_EXIT(fd, file, name);
			}
		}

		/* Scan all terminal names - format is name|name|name| */ 
		p = buf;
		while ((p2 = strchr(p, '|')) != NULL) {
			*p2 = 0;
			if (strcmp(p, name) == 0) {
				/* Entry found! */
				struct termcap_entry	*entry;
				printf("Found %s!\n", p);
				ret = parse_entry(fd);
				fclose(fd);
#ifdef KEY_HOME_VAL
				if ((entry = lookup_entry("kh")) != NULL) {
					if (entry->data != NULL) free(entry->data);
					entry->data = n_xstrdup(KEY_HOME_VAL);
				}
#endif
#ifdef KEY_EOL_VAL
				if ((entry = lookup_entry("@7")) != NULL) {
					if (entry->data != NULL) free(entry->data);
					entry->data = n_xstrdup(KEY_EOL_VAL);
				}
#endif
				return ret;
			}
			p = p2 + 1;
		}
	}
	DOGET_EXIT(fd, file, name);
}

static struct termcap_entry *
do_get_termcap_entry(const char *name) {
	int							i;
	static struct termcap_entry	*p = NULL;

	if (p != NULL) {
		/* Already called to probe - stuff is initialized */
		return p;
	}

	for (i = 0; termcap_paths[i] != NULL; ++i) {
		if ((p = do_get_entry(termcap_paths[i], name)) != NULL) {
			break;
		}
	}
	if (p == NULL) {
#if 0
		fprintf(stderr, "Error: Couldn't find termcap database!\n");
#endif
		return NULL;
	}
	return p;
}

static void
do_store_term_esc(void *te,
					struct termcap_entry	***dest) {
	static int		alloc = 0;
	static int		index = 0;

	if (te == NULL && dest == NULL) {
		alloc = 0;
		index = 0;
		return;
	}
	if (index >= alloc) {
		if (alloc == 0) alloc = 64;
		else alloc *= 2;
		*dest = n_xrealloc(*dest, alloc * sizeof **dest);
	}
	(*dest)[index] = te;
	++index;
}


static int
do_sort(const void *dest, const void *src) {
	struct termcap_entry	**t1 = (struct termcap_entry **)dest;
	struct termcap_entry	**t2 = (struct termcap_entry **)src;

	return strlen((char *)(*t1)->data) - strlen((char *)(*t2)->data);
}

#define ELEMENTS(x) \
	(sizeof x / sizeof x[0])

void
populate_term_esc(void) {
	int		i;
	int		cur;
	int		nmembers = 0;
	size_t	tind = 0;
	char	*p;

	/* Set pointers to all escape sequence strings */
	for (i = 0; termmap[i].name != NULL; ++i) {
		if (termmap[i].type == TY_STRING) {
			if ((p = termmap[i].data) != NULL
				&& p[0] == 033) {
				/* Is escape sequence */
				++nmembers;
				do_store_term_esc(&termmap[i], &term_esc);
			}
		}
	}

	/* Sort pointers by number of characters - in ascending order */
	qsort(term_esc, nmembers, sizeof *term_esc, do_sort);

	do_store_term_esc(NULL, &term_esc);

	cur = 0;
	for (i = 0; term_esc[i] != NULL; ++i) {
		p = term_esc[i]->data;
		if (p[cur] != 0) {
			/* New length encountered */
			if (tind >= ELEMENTS(term_esc_len) - 1) {
				fprintf(stderr, "Terminal entry map overflow - too long\n");
				exit(EXIT_FAILURE);
			}
			++tind;
			term_esc_len[tind] = i;
			/* do_store_term_esc(term_esc[i], &term_esc_len); */
			do {
				++cur;
			} while (p[cur] != 0);
		}
	}
	term_esc_len[tind] = -1;
}


static int
tc_assign_entry(const char *name, const char *data) {
	struct termcap_entry	*te;

	if ((te = lookup_entry(name)) == NULL) {
		/* Shouldn't happen */
		(void) fprintf(stderr, "Cannot find termcap entry for ``%s''\n",
			name);
		return -1;
	}
	if (te->data == NULL) {
		te->data = n_xstrdup(data);
	}
	return 0;
}

#ifndef NO_CURSES
/* 
 * Various libraries I've encountered use different prototyeps for this ...
 */
char	*tgetstr();
#endif

struct termcap_entry *
get_termcap_from_curses(void) {
#ifndef NO_CURSES
	int			i;
	char		*name;
	char		*p;
	char		*data;
	char		buf[2048];
	const char	*caps[] = {
		"kd", /* KEY_DOWN */
		"k1", /* KEY_F1 */
		"k;", /* KEY_F10 */
		"k2", /* KEY_F2 */
		"k3", /* KEY_F3 */
		"k4", /* KEY_F4 */
		"k5", /* KEY_F5 */
		"k6", /* KEY_F6 */
		"k7", /* KEY_F7 */
		"k8", /* KEY_F8 */
		"k9", /* KEY_F9 */
		"kl", /* KEY_LEFT */
		"kr", /* KEY_RIGHT */
		"ku", /* KEY_UP */
		"F1", /* KEY_F11 */
		"F2", /* KEY_F12 */
#ifndef KEY_HOME_VAL
		"kh", /* KEY_HOME */
#endif
#ifndef KEY_EOL_VAL
		"@7", /* KEY_EOL */
#endif
		NULL
	};

#ifdef KEY_HOME_VAL
	(void) tc_assign_entry("kh", KEY_HOME_VAL); 
#endif
#ifdef KEY_EOL_VAL
	(void) tc_assign_entry("@7", KEY_EOL_VAL);
#endif

	if ((name = get_term_name()) == NULL) {
		return NULL;
	}

	if (tgetent(buf, name) != 1) {
		(void) fprintf(stderr, "Couldn't load termcap entry for %s\n",
			name);
		return NULL;
	}

	for (i = 0; caps[i] != NULL; ++i) {
		p = buf;
		if ((data = tgetstr(caps[i], &p)) == NULL) {
			(void) fprintf(stderr, "Couldn't read termcap capability %s\n",
				caps[i]);
			continue;
		}
		if (tc_assign_entry(caps[i], data) != 0) { 
			/* May be tiny memory leak, but we don't care */
			return NULL;
		}
	}
	return termmap;
#else
	return NULL;
#endif /* #ifndef NO_CURSES */
}


struct termcap_entry *
get_termcap_entry(void) {
	char	*name;

	if ((name = get_term_name()) == NULL) {
		return NULL;
	}
	return do_get_termcap_entry(name);
}


#ifdef TEST_TERMCAP

int
main(void) {
	struct termcap_entry	*p;
	if ((p = get_termcap_entry()) != NULL) {
		puts("success");
		populate_term_esc();
	}
	return 0;
}

#endif

