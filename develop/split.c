/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "split.h"

static char *
dupstr(char *str) {
	char *ret;
	ret = malloc(strlen(str) + 1);
	if (ret) {
		strcpy(ret, str);
	}
	return ret;
}

int is_blank(int ch) {
	return ch == ' ' || ch == '\t';
}

static int 
do_copy(
					struct split *s, 
					char *buf, 
					char *input,
					int   options,
					int   max ) {
	const int	basesize	= 8;
	int			alloctmp	= s->alloc, 
				indextmp	= s->nstr,
				rc			= 0;
	char		**datatmp = s->data,
				**tmp,
				*p;

	if (max && indextmp >= max) {
		p  = input;
		s->rc = rc = 2;
	} else {
		p = buf;
	} 

	if (indextmp >= (alloctmp - 1)) { /* -1 to ensure that there's /always/ one
										* free ``slot'' for caller to NULL-
										* terminate */
		alloctmp += basesize;
		tmp = realloc(datatmp, alloctmp * sizeof(char*));
		if (tmp == NULL) {
			int i;
			for (i = 0; i < indextmp; ++i) {
				free(datatmp[i]);
			}
			free(datatmp);
			return (s->rc = 1);
		}
		s->alloc = alloctmp;
		datatmp = tmp;
	}

  /*
   * Don't copy, just assign pointer
   */
	if (options & SPLIT_NOCOPY)
		datatmp[indextmp] = input;
	else
		datatmp[indextmp] = dupstr(p);
	if (max && indextmp >= max) {
		datatmp[indextmp + 1] = NULL;
		s->rc = 0;
	}
	s->data = datatmp;
	++s->nstr;
	return rc;
}
  

struct split
split(char *input, char *tokens, int options, int optparam) {
	struct split	result = { NULL, 0, 0, 0, 0 };
	char			buf[8192],
					*tmp;
	const int		basesize = 8;
	int				i;


	if (options & SPLIT_NOCOPY)
		result.type = SPLIT_NOCOPY;
	else
		result.type = 0;

	result.data = malloc(basesize * sizeof(char*));

	if (result.data == NULL) {
		result.rc = 1;
		return result;
	}

	if (options & SPLIT_UPTO) {
		--optparam;
	} else {
		optparam = 0;
	}
  
	for (i = 0; i < 8191 && *input; ++i, ++input) {
		for (tmp = tokens; *tmp && *input; ++tmp) {
			if (*input != *tmp) {
				continue;
			}
			if (i == 0) { /* avoid copying nul-string */
				if (options & SPLIT_INCTOK) {
					if (((options & SPLIT_NOWHITE) == 0) ||
						(is_blank(*input) == 0 && *input != '\n')) {
						buf[0] = *input;
						buf[1] = 0;
						if (options & SPLIT_NOCOPY)
							*input = 0;
						if (do_copy(&result,buf,input-i,options,optparam) != 0){
							return result;
						}
					}
				} 
				tmp = tokens - 1;
				++input;
				continue;
			}
			buf[i] = 0;
			if (options & SPLIT_NOCOPY)
				*input = 0;
			if (do_copy(&result,buf,input - i,options,optparam) != 0) { 
				return result;
			}
			if (options & SPLIT_INCTOK) {
				if (((options & SPLIT_NOWHITE) == 0) || 
					(is_blank(*input) == 0 && *input != '\n')) { 
					buf[0] = *input;
					buf[1] = 0;
					if (options & SPLIT_NOCOPY)
						*input = 0;
					if (do_copy(&result, buf, input, options, optparam) != 0) {
						return result;
					}
				}
			}
			++input;
			i = 0; 
			if (options & SPLIT_EATREP) {
				tmp = tokens - 1; /* undefined behaviour :( */
				continue;
			}
			break;
		}
		if (*input == 0 ) {
			break;
		}
		buf[i] = *input;
	}
	if (i > 0) {
		buf[i] = 0;
		if (options & SPLIT_NOCOPY)
			*input = 0;
		if (do_copy(&result, buf, input-i, options,optparam) != 0) {
				return result;
		}
	}
	result.data[result.nstr]  = NULL;
	result.rc   = 0;
	return result;
}

void
split_destroy(struct split *s) {
	int	i;
	if (s->rc == 0 && s->data != NULL && s->type != SPLIT_NOCOPY) {
		s->data[0] = NULL;
		for (i = 0; i < s->nstr; ++i) {
			free(s->data[i]);
		}
		free(s->data);
		s->data = NULL;
		s->rc   = 1;
	} else if (s->type == SPLIT_NOCOPY) {
		if (s->rc == 0 && s->data != NULL) {
			free(s->data);
			s->data = NULL;
		}
	}
}

#ifdef TEST_SPLIT

int
main(void) {
	char			buf[128];
	int				loop,
					flags;
	struct split	s;
	FILE			*fd = fopen( "/etc/services", "r" );

	s = split("oh m   gee, lol!!! ", " ,!", SPLIT_EATREP | SPLIT_NOCOPY, 0);
	if (s.rc == 0) {
		int	i;
		for (i = 0; i < s.nstr; ++i) {
			if (!SPLIT_ISWHITE(s.data[i])) {
				printf("%s\n", s.data[i]);
			}
		}
		split_destroy(&s);
		exit(0);
	}
	if (fd == NULL) {
		perror("fopen");
		return 1;
	}
	for (loop = 3; loop < 4; ++loop) {
		switch (loop) {
		case 0: flags = SPLIT_EATREP;              break; 
		case 1: flags = SPLIT_UPTO;                break;
		case 2: flags = SPLIT_UPTO | SPLIT_EATREP; break;
		case 3: flags = SPLIT_INCTOK;
		}
		while (fgets(buf, 128, fd)) {
			s = split(buf, " \t\n", flags, 2);
			if (s.rc == 0) {
				int i;
				for (i = 0; i < s.nstr; ++i) {
					printf("%s\n", s.data[i]);
				}
				split_destroy(&s);
			} else {
				perror("split");
			}
		}
		rewind(fd);
	}
	return 0;
}

#endif /* #ifdef TEST_SPLIT */

