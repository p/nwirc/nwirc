/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef USERS_H
#define USERS_H

#ifndef TIME_H
#define TIME_H
#include <time.h>
#endif

#ifndef DEFS_H
#include "defs.h"
#endif

extern int current;

struct chan_user {
	char             *name;
	time_t            signon;
	int               is_operator;
	struct chan_user *next;
	/* struct chan_user *prior; */
};

void initusers      (char *data                                            );
void clearusers     (void *ptr, char *name                                 );
void adduser        (char *name, char *ch                                  );
void remuser        (char *name, char *ch, int is_quit, char *quitmsg      );
void change_userdata(char *name, char *newname, int operator, char *channel);
void printusers     (char *c                                               );

#endif
