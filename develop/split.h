/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef SPLIT__
#define SPLIT__


struct split {
	char **data;
	int    nstr,
	       alloc,
	       rc,
	       type;
};

#define SPLIT_EATREP  1
#define SPLIT_UPTO    (1 << 1)
#define SPLIT_INCTOK  (1 << 2)
#define SPLIT_NOWHITE (1 << 3)
#define SPLIT_NOCOPY  (1 << 4)

#define SPLIT_ISWHITE(str) ( \
    (((str)[0] == ' ') || ((str)[0] == '\t') || ((str)[0] == '\n')) && \
    ((str)[1] == 0) \
  )

struct split split(char *input, char *tokens, int options, int optparam);
void split_destroy(struct split *s                                     );
  
#endif  
  
