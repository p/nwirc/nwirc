/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef COMMANDS_H
#define COMMANDS_H

#define QUIET 1
#include "module.h"
 
extern char filename[1024];

extern int size,
       dcc_waiting;

struct sockaddr_in;
extern struct sockaddr_in dcc_addr;

/* applied commands */

struct server;

typedef int (*f_privmsg_send_t)  (struct server *, char *, char *, int);
typedef int (*f_me_send_t)       (char *, char *);
typedef int (*f_ctcp_send_t)     (char *, char *);
typedef int (*f_quit_send_t)     (char *);
typedef int (*f_part_send_t)     (char *);
typedef int (*f_join_send_t)     (char *);
typedef int (*f_nick_send_t)     (char *);
typedef int (*f_topic_send_t)    (char *);
typedef int (*f_mode_send_t)     (char *, char *);
typedef int (*f_kick_send_t)     (char *, char *);
typedef int (*f_sys_send_t)      (char *);
typedef int (*f_ip_send_t)       (char *);
typedef int (*f_notice_send_t)   (char *, char *);

int privmsg_send  (struct server *s, char *other, char *message,
					int display);
int me_send       (char *other, char *message              );
int ctcp_send     (char *other, char *message              );
int quit_send     (char *command                           );
int part_send     (char *command                           );
int join_send     (char *command                           );
int nick_send     (char *command                           );
int topic_send    (char *topic                             );
int mode_send     (char *receiver, char *mode              );
int kick_send     (char *ch, char *receiver                );
int sys_send      (char *command                           );
int ip_send       (char *chan                              );
int notice_send   (char *user, char *message               );
int exit_client   (void                                    );
int deluxe        (char *command                           );

/* received packets */

void mode_recv    (char *sender, char *channel, char *mode                   );
void kick_recv    (char *sender, char *channel, char *receiver, char *message);
void ping_recv    (char *sender                                              );
void topic_recv   (char *sender, char *channel, char *topic                  );
void privmsg_recv (char *address, char *sender, char *receiver,char *message );
void part_recv    (char *name, char *address, char *sender, char *channel, char *message    );
void join_recv    (char *name, char *address, char *sender, char *channel    );
void quit_recv    (char *name, char *address, char *sender, char *message, char *message2   );
void notice_recv  (char *sender, char *address, char *message                );
void nick_recv    (char *name, char *address, char *sender, char *nick       );

#endif
