/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#ifndef FSERV_H
#define FSERV_H

#define FORCED     1 
#define NOT_FORCED 0 

#ifndef TIME_H
#define TIME_H
#include <time.h>
#endif

struct user_list {
	char name[100],
	     path[1024],
	    *address;
	time_t last_action;
	struct user_list *next;
};

void fserv   (char *address, char *entire, char *command);

#endif
