/*
 * Copyright (C) 2002, 2003, 2004  Nils R. Weller 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "mode.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "split.h"
#include "color.h"
#include "users.h"

static struct split s;
static int          argindex,
                    dowhat;

/*
 * Reaction to fnmatch( servers[x].nickname, BAN ): send -o+i+b user user
 */

static int
mode_operator(char *chan) { 
	/*
	 * Change operator status. Verfiy that argument (username) is given..
	 */
	if (argindex >= s.nstr) {
		printcol( "Cannot parse mode string: Bad format\n" );
		return 1;
	}
	if (dowhat == 1) { /* operator now */
		change_userdata(s.data[argindex], NULL, 1, chan);
	} else { /* not operator anymore */
		change_userdata(s.data[argindex], NULL, 0, chan); 
	}
	++argindex;
	return 0;
}

void
parse_mode(char *chan, char *modestr) {
	char        *p;
	int          rc;
	s = split(modestr, " ", SPLIT_EATREP, 0);
	if (s.rc != 0) {
		printcol("split: %s\n", strerror(errno));
	}
	argindex = 1;
	/*
	 * Parse first field. Format:
	 * +x-yz+foo arg1 arg2 arg3 ...
	 */
	for (p = s.data[0]; *p; ++p) {
		switch (*p) {
		case '+': dowhat = 1; break; /* add subsequent modes */
		case '-': dowhat = 0; break; /* remove subsequent modes */
		default:
			rc = 1;
			if (*p == 'o') {
				rc = mode_operator(chan);
			}
			if (rc == 1) {
				return;
			}
		}
	}
}

